package container.constant;

/**
 * 容器常量
 *
 * @author moqi
 * @date 2022/8/5 14:22
 */
public class ContainerConstant {

    public static final String REDIS_IMAGE = "redis:7.0.4";

    public static final String REDIS_ALPINE_IMAGE = REDIS_IMAGE + "-alpine";

    public static final int REDIS_TEST_PORT = 6379;

}
