package com.moqi.in20210919;

import java.io.PrintWriter;

/**
 * @author moqi
 * On 2021/9/19 13:48
 */
public class PrintWriteTest {

    private static final String EMPLOYEE_TXT = "employee.txt";

    public static void main(String[] args) throws Exception {

        PrintWriter printWriter = new PrintWriter(EMPLOYEE_TXT, "UTF-8");
        String name = "Harry Hacker";
        double salary = 85000;

        printWriter.print(name);
        printWriter.print(' ');
        printWriter.print(salary);
        printWriter.flush();

        System.out.println("main over.");
    }

}
