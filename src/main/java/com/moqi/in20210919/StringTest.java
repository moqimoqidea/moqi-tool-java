package com.moqi.in20210919;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Stream;

import static com.moqi.in20210919.ResourcesUtil.getPathByName;

/**
 * 读取文本文件内容
 *
 * @author moqi
 * On 2021/9/19 16:43
 */
public class StringTest {

    private static final String TEXT_TXT = "text.txt";

    public static void main(String[] args) throws IOException, URISyntaxException {
        oneline();

        readLineByLine();

        streamingRead();
    }

    /**
     * 将短小的文本文件像下面这样读入到一个字符串
     */
    private static void oneline() throws IOException, URISyntaxException {
        String content = new String(Files.readAllBytes(getPathByName(TEXT_TXT)), StandardCharsets.UTF_8);
        System.out.println("content = " + content);
    }

    /**
     * 单行读取
     */
    private static void readLineByLine() throws IOException, URISyntaxException {
        List<String> strings = Files.readAllLines(getPathByName(TEXT_TXT));
        System.out.println("strings = " + strings);
        System.out.println();
    }

    /**
     * 惰性读取为 Stream
     */
    private static void streamingRead() {
        try (Stream<String> lines = Files.lines(getPathByName(TEXT_TXT))) {
            lines.forEach(System.out::println);
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

}
