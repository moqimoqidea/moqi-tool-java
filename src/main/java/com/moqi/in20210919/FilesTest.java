package com.moqi.in20210919;

import com.google.common.collect.Maps;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Comparator;
import java.util.stream.Stream;

/**
 * @author moqi
 * On 2021/9/19 19:09
 */
public class FilesTest {

    public static void main(String[] args) throws IOException {

        read();

        write();

        appendWrite();

        createDirectory();

        createDirectories();

        createEmptyFile();

        createTempFile();
        createTempFile();
        createTempFile();

        createTempDir();
        createTempDir();
        createTempDir();

        copyAnyway();
        copyAnyway();

        moveAtomic();

        walk();

        newDirectoryStream();

        zipSystem();
    }

    private static void read() throws IOException {
        String content = new String(Files.readAllBytes(Paths.get("employee.txt")), StandardCharsets.UTF_8);
        System.out.println("content = " + content);
    }

    private static void write() throws IOException {
        Files.write(Paths.get("employee.files.txt"), "Something write\n".getBytes(StandardCharsets.UTF_8));
        System.out.println("write over");
    }

    private static void appendWrite() throws IOException {
        Files.write(
                Paths.get("employee.files.txt"),
                "Something append write\n".getBytes(StandardCharsets.UTF_8),
                StandardOpenOption.APPEND);
        System.out.println("append write over");
    }

    private static void createDirectory() throws IOException {
        Path path = Paths.get("createDirectory");
        deletePath(path);

        Files.createDirectory(path);
        System.out.println("create directory over!");
    }

    private static void createDirectories() throws IOException {
        Path path = Paths.get("create/directories/a/b/c/d/e");
        deletePath(path);

        Files.createDirectories(path);
        System.out.println("create directories over!");
    }

    private static void createEmptyFile() throws IOException {
        Path path = Paths.get("create/emptyFile.txt");
        deletePath(path);

        Files.createFile(path);
        System.out.println("create empty file over!");
    }

    private static void createTempFile() throws IOException {
        Path tempFile = Files.createTempFile("abc", ".txt");
        System.out.println("create temp file " + tempFile + " over!");
    }

    private static void createTempDir() throws IOException {
        Path tempDirectory = Files.createTempDirectory("tempDirPrefix");
        System.out.println("create temp directory " + tempDirectory + " over!");
    }

    private static void copyAnyway() throws IOException {
        Path from = Paths.get("create/emptyFile.txt");
        Path to = Paths.get("create/directories/emptyFile.txt");
        Path copy = Files.copy(from, to, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
        System.out.println("copy = " + copy);
    }

    private static void moveAtomic() throws IOException {
        Path from = Paths.get("create/emptyFile.txt");
        Path to = Paths.get("create/directories/a/b/c/d/e/emptyFile.txt");

        Files.deleteIfExists(to);

        Path move = Files.move(from, to, StandardCopyOption.ATOMIC_MOVE);
        System.out.println("move = " + move);
    }

    private static void walk() throws IOException {
        System.out.println();
        try (Stream<Path> stream = Files.walk(Paths.get("moqi-tool-java/gradle"))) {
            stream.forEach(System.out::println);
        }
    }

    /**
     * find all gradle suffix file
     */
    private static void newDirectoryStream() throws IOException {
        System.out.println();
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get("moqi-tool-java"), "**gradle**")) {
            stream.forEach(System.out::println);
        }
    }

    private static void zipSystem() throws IOException {
        System.out.println();
        String zipFile = "moqi-tool-java/src/main/resources/txt.zip";
        FileSystem fileSystem = FileSystems.newFileSystem(Paths.get(zipFile), Maps.newHashMap());
        Files.walkFileTree(fileSystem.getPath("/"), new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
                System.out.println("file = " + file);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    private static void deletePath(Path path) throws IOException {
        boolean exists = Files.exists(path);

        if (exists) {
            Files.walk(path)
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .peek(x -> System.out.println("delete file: " + x + " success!"))
                    .forEach(File::delete);
            System.out.println("delete old path: " + path + " success!");
        } else {
            System.out.println("old path: " + path + " is not exist!");
        }
    }

}
