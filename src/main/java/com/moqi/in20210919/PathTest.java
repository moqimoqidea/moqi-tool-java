package com.moqi.in20210919;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author moqi
 * On 2021/9/19 18:57
 */
public class PathTest {

    public static void main(String[] args) {

        resolve();

        resolveWithString();

        resolveSibling();

    }

    /**
     * p.resolve(q)
     *
     * 如果q是绝对路径，则结果就是q。
     * 否则，根据文件系统的规则，将“p后面跟着q”作为结果。
     */
    private static void resolve() {
        Path basePath = Paths.get("/home");

        Path work = Paths.get("/tom/work");
        Path resolve = basePath.resolve(work);
        System.out.println("resolve = " + resolve);

        Path work2 = Paths.get("work");
        Path resolve2 = basePath.resolve(work2);
        System.out.println("resolve2 = " + resolve2);
    }

    /**
     * resolve方法有一种快捷方式，它接受一个字符串而不是路径
     */
    private static void resolveWithString() {
        Path resolveWithString = Paths.get("/home").resolve("a/b/c/d/e");
        System.out.println("resolveWithString = " + resolveWithString);
    }

    /**
     * 还有一个很方便的方法resolveSibling，它通过解析指定路径的父路径产生其兄弟路径。
     */
    private static void resolveSibling() {
        Path resolveSibling = Paths.get("/home/home1/home2").resolveSibling("a/b/c/d/e");
        System.out.println("resolveSibling = " + resolveSibling);
    }

}
