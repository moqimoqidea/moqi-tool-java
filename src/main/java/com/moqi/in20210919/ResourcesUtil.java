package com.moqi.in20210919;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

/**
 * Resources Util handler
 *
 * @author moqi
 * On 2021/9/20 16:10
 */
public class ResourcesUtil {

    /**
     * trans resource file name to path
     */
    public static Path getPathByName(String resource) throws URISyntaxException {
        return Paths.get(getUriByName(resource));
    }

    /**
     * trans resource file name to uri
     */
    public static URI getUriByName(String resource) throws URISyntaxException {
        return Objects.requireNonNull(ResourcesUtil.class.getClassLoader().getResource(resource)).toURI();
    }

}
