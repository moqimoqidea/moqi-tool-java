package com.moqi.in20210919;

import java.nio.charset.Charset;

/**
 * @author moqi
 * On 2021/9/19 17:11
 */
public class CharsetTest {

    public static void main(String[] args) {
        Charset charset = Charset.defaultCharset();
        System.out.println("charset = " + charset);
    }

}
