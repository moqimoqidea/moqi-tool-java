package com.moqi.in20220120;

import lombok.*;

/**
 * @author moqi
 * @date 2022/1/20 16:48
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Person {

    @ToString.Exclude
    private String name;

    private Integer age;

}
