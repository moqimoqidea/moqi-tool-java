package com.moqi.in20220223;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author moqi
 * @date 2022/2/23 12:31
 */
public class ExecutorShutdownNowTest {

    public static void main(String[] args) throws InterruptedException {

        ExecutorService executor = Executors.newFixedThreadPool(2);


        Callable<Void> callable = () -> {
            try {
                for (int i = 0; i < 1000; i++) {
                    System.out.println("Hello from thread " + Thread.currentThread().getName() + ", i = " + i);
                    Thread.sleep(20);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return null;
        };

        Future<Void> future = executor.submit(callable);

        Thread.sleep(3333L);

        // 取消任务并不能杀死子进程
        future.cancel(true);
        executor.shutdownNow();
    }

}
