package com.moqi.in20220223;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/**
 * @author moqi
 * @date 2022/2/23 12:33
 */
public class CallableInterruptTest {

    public static void main(String[] args) throws Exception {
        Callable<Void> callable = () -> {
            try {
                for (int i = 0; i < 1000; i++) {
                    System.out.println("Hello from thread " + Thread.currentThread().getName() + ", i = " + i);
                    Thread.sleep(20);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return null;
        };

        Thread thread = new Thread(new FutureTask<>(callable));

        thread.start();

        try {
            Thread.sleep(3333L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        thread.interrupt();

    }

}
