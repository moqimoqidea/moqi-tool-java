package com.moqi.in20220223;

/**
 * @author moqi
 * @date 2022/2/23 12:30
 */
public class ThreadNotInterruptTest {

    public static void main(String[] args) {
        Thread thread = new Thread(() -> {
            while (true) {
                for (int i = 0; i < 100000000; i++) {
                    if (System.currentTimeMillis() % 1000 == 0) {
                        System.out.println("Hello from thread " + Thread.currentThread().getName() + ", i = " + i);
                    }
                }
            }
        });

        thread.start();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        thread.interrupt();
    }

}
