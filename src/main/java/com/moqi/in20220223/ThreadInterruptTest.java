package com.moqi.in20220223;

/**
 * @author moqi
 * @date 2022/2/23 12:30
 */
public class ThreadInterruptTest {

    public static void main(String[] args) {
        Thread thread = new Thread(() -> {
            try {
                for (int i = 0; i < 1000; i++) {
                    System.out.println("Hello from thread " + Thread.currentThread().getName() + ", i = " + i);
                    Thread.sleep(20);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        thread.start();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        thread.interrupt();
    }

}
