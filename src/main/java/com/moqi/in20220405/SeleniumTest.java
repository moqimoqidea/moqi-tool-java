package com.moqi.in20220405;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

/**
 * https://www.baeldung.com/java-selenium-screenshots
 *
 * @author moqi
 * @date 4/5/22 19:02
 */
public class SeleniumTest {

    private static final String DRIVER_NAME = "webdriver.chrome.driver";
    private static final String DRIVER_PATH = "/Users/moqi/Code/moqi/moqi-tool-java/src/main/resources/driver/chromedriver_mac64_100.0.4896.60";

    private static final String GOOGLE_URL = "https://www.google.com/";

    private static final ChromeDriver CHROME_DRIVER;
    private static final String GOOGLE_HOME_PNG_PATH = "/Users/moqi/Code/moqi/moqi-tool-java/src/main/resources/image/google-home.png";


    static {
        System.setProperty(DRIVER_NAME, DRIVER_PATH);

        CHROME_DRIVER = new ChromeDriver(new ChromeOptions());
        CHROME_DRIVER.manage()
                .timeouts()
                .implicitlyWait(Duration.ofSeconds(5L));

        CHROME_DRIVER.get(GOOGLE_URL);
    }

    public static void main(String[] args) throws IOException {
        takeScreenshot(GOOGLE_HOME_PNG_PATH);
    }

    public static void takeScreenshot(String pathname) throws IOException {
        File src = ((TakesScreenshot) CHROME_DRIVER).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(src, new File(pathname));

        CHROME_DRIVER.close();
    }

}
