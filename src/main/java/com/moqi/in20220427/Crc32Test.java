package com.moqi.in20220427;

import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.zip.CRC32;

/**
 * @author moqi
 * @date 4/27/22 17:25
 */
@Slf4j
public class Crc32Test {

    private static final List<String> LIST = List.of("aaa", "bbb");
    private static final ThreadLocal<CRC32> THREAD_LOCAL = ThreadLocal.withInitial(CRC32::new);

    public static void main(String[] args) {
        String hash123 = hash("123");
        log.info("hash123:{}", hash123);

        String hash999 = hash("999");
        log.info("hash999:{}", hash999);
    }

    private static String hash(String id) {
        CRC32 crc32 = THREAD_LOCAL.get();
        crc32.reset();

        crc32.update(id.getBytes());
        long value = crc32.getValue();
        crc32.reset();

        int index = (int) (value % LIST.size());

        THREAD_LOCAL.remove();
        return LIST.get(index);
    }

}
