package com.moqi.in20220109;

import org.apache.logging.log4j.core.config.Property;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Base on https://github.com/prestodb/presto/blob/master/presto-hive-common/src/main/java/org/apache/hadoop/fs/HadoopExtendedFileSystemCache.java
 *
 * @author moqi
 * @date 1/9/22 14:38
 */
public class Java17ReflectTool {

    private Java17ReflectTool() {
    }

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        Property property = Property.createProperty("aaa", "bbb");
        // setFinalStatic(Field.class, new Properties(), "map", new ConcurrentHashMap<>(10));
        Field f = get(property, "name");
        System.out.println(f.get(property));
        f.set(property, "ccc");
        System.out.println(f.get(property));

    }

    public static Field get(Object obj, String name) throws NoSuchFieldException {
        try {
            Field field = obj.getClass().getDeclaredField(name);
            field.setAccessible(true);
            return field;
        } catch (NoSuchFieldException e) {
            try {
                Method getDeclaredFields0 = Class.class.getDeclaredMethod("getDeclaredFields0", boolean.class);
                getDeclaredFields0.setAccessible(true);
                Field[] fields = (Field[]) getDeclaredFields0.invoke(Field.class, false);
                for (Field field : fields) {
                    if (name.equals(field.getName())) {
                        field.setAccessible(true);
                        return field;
                    }
                }
            } catch (ReflectiveOperationException ex) {
                e.addSuppressed(ex);
            }
            throw e;
        }
    }

}
