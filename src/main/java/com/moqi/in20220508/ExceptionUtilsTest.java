package com.moqi.in20220508;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.util.Arrays;

/**
 * @author moqi
 * @date 5/8/22 15:53
 */
@Slf4j
public class ExceptionUtilsTest {

    public static void main(String[] args) {
        RuntimeException runtimeException = new RuntimeException("AAAAA");
        log.info("runtimeException:", runtimeException);

        String message = ExceptionUtils.getMessage(runtimeException);
        log.info("message:{}", message);

        Throwable rootCause = ExceptionUtils.getRootCause(runtimeException);
        log.info("rootCause:", rootCause);

        String[] stackFrames = ExceptionUtils.getStackFrames(runtimeException);
        log.info("stackFrames:{}", Arrays.deepToString(stackFrames));

        ExceptionUtils.printRootCauseStackTrace(runtimeException);

        try {
            ExceptionUtils.wrapAndThrow(runtimeException);
        } catch (Exception e) {
            log.info("e:", e);
        }
    }

}
