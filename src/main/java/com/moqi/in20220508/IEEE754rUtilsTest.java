package com.moqi.in20220508;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.IEEE754rUtils;

/**
 * @author moqi
 * @date 5/8/22 16:02
 */
@Slf4j
@SuppressWarnings("AlibabaClassNamingShouldBeCamel")
public class IEEE754rUtilsTest {

    public static void main(String[] args) {
        double min = IEEE754rUtils.min(1.1111111, 1.1111112);
        log.info("min:{}", min);

        double max = IEEE754rUtils.max(1.111111234, 1.111111233);
        log.info("max:{}", max);
    }

}
