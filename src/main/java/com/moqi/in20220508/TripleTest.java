package com.moqi.in20220508;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Triple;

/**
 * @author moqi
 * @date 5/8/22 16:36
 */
@Slf4j
public class TripleTest {

    public static void main(String[] args) {
        Triple<String, String, String> triple = Triple.of("a", "b", "c");
        log.info("triple:{}", triple);

        String left = triple.getLeft();
        log.info("left:{}", left);

        String middle = triple.getMiddle();
        log.info("middle:{}", middle);

        String right = triple.getRight();
        log.info("right:{}", right);
    }

}
