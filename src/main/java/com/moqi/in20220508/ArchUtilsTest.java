package com.moqi.in20220508;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArchUtils;
import org.apache.commons.lang3.arch.Processor;

/**
 * @author moqi
 * @date 5/8/22 13:02
 */
@Slf4j
public class ArchUtilsTest {

    /**
     * 2022-05-08 13:04:05:378 INFO  - arch:BIT_64, type:X86
     */
    public static void main(String[] args) {
        Processor processor = ArchUtils.getProcessor();
        Processor.Arch arch = processor.getArch();
        Processor.Type type = processor.getType();
        log.info("arch:{}, type:{}", arch, type);
    }

}
