package com.moqi.in20220508;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RegExUtils;

/**
 * @author moqi
 * @date 5/8/22 15:13
 */
@Slf4j
public class RegExUtilsTest {

    public static void main(String[] args) {
        String s1 = RegExUtils.removeAll("123456789AAA", "\\d");
        log.info("s1:{}", s1);

        String s2 = RegExUtils.removeFirst("123456789AAA", "\\d");
        log.info("s2:{}", s2);

        String s3 = RegExUtils.removePattern("123456789AAA", "\\d");
        log.info("s3:{}", s3);

        String s4 = RegExUtils.replaceAll("123456789AAA", "\\d", "X");
        log.info("s4:{}", s4);

        String s5 = RegExUtils.replaceFirst("123456789AAA", "\\d", "X");
        log.info("s5:{}", s5);

        String s6 = RegExUtils.replacePattern("123456789AAA", "\\d", "X");
        log.info("s6:{}", s6);

        // s7:1,2345,6789AAA
        // 匹配成功 两组
        String s7 = RegExUtils.replacePattern("123456789AAA", "(\\d)(\\d{3})", "$1,$2");
        log.info("s7:{}", s7);

        // s8:1,23456789AAA
        String s8 = RegExUtils.replacePattern("123456789AAA", "(\\d)(\\d{7})", "$1,$2");
        log.info("s8:{}", s8);
    }

}
