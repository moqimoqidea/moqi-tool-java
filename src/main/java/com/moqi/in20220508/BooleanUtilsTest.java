package com.moqi.in20220508;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.BooleanUtils;

/**
 * @author moqi
 * @date 5/8/22 14:32
 */
@SuppressWarnings("ConstantConditions")
@Slf4j
public class BooleanUtilsTest {

    public static void main(String[] args) {
        boolean b1 = BooleanUtils.isFalse(null);
        log.info("b1:{}", b1);

        boolean b2 = BooleanUtils.isNotFalse(null);
        log.info("b2:{}", b2);

        boolean b3 = BooleanUtils.isNotTrue(null);
        log.info("b3:{}", b3);

        boolean b4 = BooleanUtils.isTrue(null);
        log.info("b4:{}", b4);

        /*
         * BooleanUtils.toBoolean(null)    = false
         * BooleanUtils.toBoolean("true")  = true
         * BooleanUtils.toBoolean("TRUE")  = true
         * BooleanUtils.toBoolean("tRUe")  = true
         * BooleanUtils.toBoolean("on")    = true
         * BooleanUtils.toBoolean("yes")   = true
         * BooleanUtils.toBoolean("false") = false
         * BooleanUtils.toBoolean("x gti") = false
         * BooleanUtils.toBoolean("y") = true
         * BooleanUtils.toBoolean("n") = false
         * BooleanUtils.toBoolean("t") = true
         * BooleanUtils.toBoolean("f") = false
         */
        boolean b5 = BooleanUtils.toBoolean("on");
        log.info("b5:{}", b5);

        /*
         * BooleanUtils.toStringOnOff(Boolean.TRUE)  = "on"
         * BooleanUtils.toStringOnOff(Boolean.FALSE) = "off"
         * BooleanUtils.toStringOnOff(null)          = null;
         */
        String s = BooleanUtils.toStringOnOff(Boolean.TRUE);
        log.info("s:{}", s);
    }

}
