package com.moqi.in20220508;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomUtils;

/**
 * @author moqi
 * @date 5/8/22 15:09
 */
@Slf4j
public class RandomUtilsTest {

    private static final int FIVE = 5;

    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            int nextInt = RandomUtils.nextInt();
            log.info("nextInt:{}", nextInt);
        }
        System.out.println("==============================");

        for (int i = 0; i < FIVE; i++) {
            double nextDouble = RandomUtils.nextDouble();
            log.info("nextDouble:{}", nextDouble);
        }
        System.out.println("==============================");

        for (int i = 0; i < FIVE; i++) {
            long nextLong = RandomUtils.nextLong();
            log.info("nextLong:{}", nextLong);
        }
        System.out.println("==============================");
    }

}
