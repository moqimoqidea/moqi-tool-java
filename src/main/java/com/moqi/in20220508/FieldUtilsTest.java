package com.moqi.in20220508;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.reflect.FieldUtils;

/**
 *
 * @see <a href="https://github.com/apache/commons-lang/blob/master/src/test/java/org/apache/commons/lang3/reflect/FieldUtilsTest.java">FieldUtilsTest.java</a>
 *
 * @author moqi
 * @date 5/8/22 16:11
 */
@Slf4j
public class FieldUtilsTest {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {

        FieldUtils.writeStaticField(StaticContainer.class, "mutablePublic", "new1", true);
        log.info("StaticContainer.mutablePublic:{}", StaticContainer.mutablePublic);

        FieldUtils.writeStaticField(StaticContainer.class, "mutableProtected", "new2", true);
        log.info("StaticContainer.getMutableProtected():{}", StaticContainer.getMutableProtected());

        FieldUtils.writeStaticField(StaticContainer.class, "mutablePackage", "new3", true);
        log.info("StaticContainer.getMutablePackage():{}", StaticContainer.getMutablePackage());

        FieldUtils.writeStaticField(StaticContainer.class, "mutablePrivate", "new4", true);
        log.info("StaticContainer.getMutablePrivate():{}", StaticContainer.getMutablePrivate());

        FieldUtils.writeStaticField(StaticContainer.class, "mutablePublic", "new5");
        log.info("StaticContainer.mutablePublic:{}", StaticContainer.mutablePublic);
    }
}

@SuppressWarnings("All")
class StaticContainer {

    public static final Object IMMUTABLE_PUBLIC = "public";

    protected static final Object IMMUTABLE_PROTECTED = "protected";

    static final Object IMMUTABLE_PACKAGE = "";

    private static final Object IMMUTABLE_PRIVATE = "private";

    private static final Object IMMUTABLE_PRIVATE_2 = "private";

    public static Object mutablePublic;
    protected static Object mutableProtected;
    static Object mutablePackage;
    private static Object mutablePrivate;

    public static void reset() {
        mutablePublic = null;
        mutableProtected = null;
        mutablePackage = null;
        mutablePrivate = null;
    }

    public static Object getMutableProtected() {
        return mutableProtected;
    }

    public static Object getMutablePackage() {
        return mutablePackage;
    }

    public static Object getMutablePrivate() {
        return mutablePrivate;
    }

}