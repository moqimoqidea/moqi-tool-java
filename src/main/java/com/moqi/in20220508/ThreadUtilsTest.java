package com.moqi.in20220508;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ThreadUtils;

import java.util.Collection;

/**
 * @author moqi
 * @date 5/8/22 15:43
 */
@Slf4j
public class ThreadUtilsTest {

    public static void main(String[] args) {
        //noinspection AlibabaAvoidManuallyCreateThread
        Thread thread = new Thread(() -> {
            long longValue = 0L;
            for (int i = 0; i < Integer.MAX_VALUE; i++) {
                longValue += i;
            }
            log.info("longValue:{}", longValue);
        });
        thread.setName("LongValueThread");
        thread.start();

        // moqiFIXME: 5/8/22 15:52: 为何这里无法获取到 LongValueThread 线程？
        Collection<ThreadGroup> allThreadGroups = ThreadUtils.getAllThreadGroups();
        log.info("allThreadGroups:{}", allThreadGroups);
    }

}
