package com.moqi.in20220508;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;

import java.util.Arrays;
import java.util.BitSet;
import java.util.Map;

/**
 * @author moqi
 * @date 5/8/22 11:38
 */
@Slf4j
public class ArrayUtilTest {

    public static void main(String[] args) {
        int[] arr = {1, 2, 3};
        log.info("arr:{}", arr);

        int[] arr1 = ArrayUtils.add(arr, 100);
        log.info("arr1:{}", arr1);

        int[] arr2 = ArrayUtils.addAll(arr1, arr);
        log.info("arr2:{}", arr2);

        int[] arr3 = ArrayUtils.addFirst(arr2, 0);
        log.info("arr3:{}", arr3);

        Integer[] arr4 = ArrayUtils.toObject(arr3);
        log.info("arr4:{}", Arrays.deepToString(arr4));

        Integer getValue1 = ArrayUtils.get(arr4, 0, 999);
        log.info("getValue1:{}", getValue1);

        Integer getValue2 = ArrayUtils.get(null, 0, 999);
        log.info("getValue2:{}", getValue2);

        Integer getValue3 = ArrayUtils.get(arr4, 100, 999);
        log.info("getValue3:{}", getValue3);

        int[] arr5 = ArrayUtils.toPrimitive(arr4);
        log.info("arr5:{}", arr5);

        ArrayUtils.swap(arr5, 0, 3);
        log.info("arr5:{}", arr5);

        int[] arr6 = ArrayUtils.subarray(arr5, 0, 5);
        log.info("subarray arr6:{}", arr6);

        ArrayUtils.shuffle(arr6);
        log.info("shuffle arr6:{}", arr6);

        ArrayUtils.shift(arr6, 2);
        log.info("shift arr6:{}", arr6);

        ArrayUtils.reverse(arr6);
        log.info("reverse arr6:{}", arr6);

        int[] arr7 = ArrayUtils.remove(arr6, 0);
        log.info("arr7:{}", arr7);

        int[] arr8 = ArrayUtils.removeElement(arr7, 3);
        log.info("arr8:{}", arr8);

        int[] arr9 = ArrayUtils.removeElements(arr8, 999, 888, 777);
        log.info("arr9:{}", arr9);

        int[] arr10 = ArrayUtils.removeAll(arr9, 0, 1);
        log.info("arr10:{}", arr10);

        int[] arr11 = ArrayUtils.removeAllOccurrences(arr10, 999);
        log.info("arr11:{}", arr11);

        int[] arr12 = ArrayUtils.nullToEmpty(arr11);
        log.info("arr12:{}", arr12);

        int[] arr13 = ArrayUtils.nullToEmpty(new int[]{});
        log.info("arr13:{}", arr13);

        boolean arrayIndexValid = ArrayUtils.isArrayIndexValid(ArrayUtils.toObject(arr12), 0);
        log.info("arrayIndexValid:{}", arrayIndexValid);

        boolean arr12Empty = ArrayUtils.isEmpty(arr12);
        log.info("arr12Empty:{}", arr12Empty);

        boolean arr13Empty = ArrayUtils.isEmpty(arr13);
        log.info("arr32Empty:{}", arr13Empty);

        int[] arr14 = ArrayUtils.insert(0, arr12, 999, 888);
        log.info("arr14:{}", arr14);

        int[] arr15 = ArrayUtils.insert(2, arr14, 0, 1, 2, 3);
        log.info("arr15:{}", arr15);

        BitSet bitSet = ArrayUtils.indexesOf(arr15, 999);
        log.info("bitSet:{}", bitSet);

        int index = ArrayUtils.indexOf(arr15, 999);
        log.info("index:{}", index);

        int indexAfter4 = ArrayUtils.indexOf(arr15, 999, 3);
        log.info("indexAfter4:{}", indexAfter4);

        Map<Object, Object> map1 = ArrayUtils.toMap(new String[][]{
                {"RED", "#FF0000"},
                {"GREEN", "#00FF00"},
                {"BLUE", "#0000FF"}
        });
        log.info("map1:{}", map1);
    }

}
