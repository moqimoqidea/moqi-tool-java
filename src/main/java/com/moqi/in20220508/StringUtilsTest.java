package com.moqi.in20220508;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

/**
 * @author moqi
 * @date 5/8/22 15:21
 */
@Slf4j
public class StringUtilsTest {

    public static void main(String[] args) {
        int i1 = StringUtils.countMatches("abba", 'b');
        log.info("i1:{}", i1);

        String s1 = StringUtils.deleteWhitespace("   ab  c  ");
        log.info("s1:{}", s1);

        String s2 = StringUtils.firstNonEmpty(null, "xyz", "abc");
        log.info("s2:{}", s2);

        String commonPrefix = StringUtils.getCommonPrefix("abcde", "abxyz");
        log.info("commonPrefix:{}", commonPrefix);

        /*
         * Note that the method does not allow for a leading sign, either positive or negative.
         * Also, if a String passes the numeric test,
         * it may still generate a NumberFormatException when parsed by Integer.parseInt or Long.parseLong,
         * e.g. if the value is outside the range for int or long respectively.
         */
        boolean numeric1 = StringUtils.isNumeric("-123");
        log.info("numeric1:{}", numeric1);

        boolean numeric2 = StringUtils.isNumeric("123");
        log.info("numeric2:{}", numeric2);

        String s3 = StringUtils.joinWith(",", "a", null, "b");
        log.info("s3:{}", s3);

        String s4 = StringUtils.left("abc", 2);
        log.info("s4:{}", s4);

        int i2 = StringUtils.ordinalIndexOf("aabaabaa", "ab", 2);
        log.info("i2:{}", i2);

        String overlay = StringUtils.overlay("abcdef", "zzzz", 2, 4);
        log.info("overlay:{}", overlay);

        String right = StringUtils.right("abc", 2);
        log.info("right:{}", right);

        String[] strings = StringUtils.splitByWholeSeparator("ab-!-cd-!-ef", "-!-");
        log.info("strings:{}", Arrays.deepToString(strings));

        String strip = StringUtils.strip(" ab c ");
        log.info("strip:{}", strip);

        String swapCase = StringUtils.swapCase("The dog has a BONE");
        log.info("swapCase:{}", swapCase);

        String wrapIfMissing = StringUtils.wrapIfMissing("ab", 'x');
        log.info("wrapIfMissing:{}", wrapIfMissing);
    }

}
