package com.moqi.in20220508;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.JavaVersion;
import org.apache.commons.lang3.SystemUtils;

import java.io.File;

/**
 * @author moqi
 * @date 5/8/22 15:35
 */
@Slf4j
public class SystemUtilsTest {

    public static void main(String[] args) {
        String hostName = SystemUtils.getHostName();
        log.info("hostName:{}", hostName);

        File javaHome = SystemUtils.getJavaHome();
        log.info("javaHome:{}", javaHome);

        File javaIoTmpDir = SystemUtils.getJavaIoTmpDir();
        log.info("javaIoTmpDir:{}", javaIoTmpDir);

        File userDir = SystemUtils.getUserDir();
        log.info("userDir:{}", userDir);

        File userHome = SystemUtils.getUserHome();
        log.info("userHome:{}", userHome);

        String userName = SystemUtils.getUserName();
        log.info("userName:{}", userName);

        boolean javaAwtHeadless = SystemUtils.isJavaAwtHeadless();
        log.info("javaAwtHeadless:{}", javaAwtHeadless);

        boolean javaVersionAtLeast = SystemUtils.isJavaVersionAtLeast(JavaVersion.JAVA_17);
        log.info("javaVersionAtLeast:{}", javaVersionAtLeast);

        boolean javaVersionAtMost = SystemUtils.isJavaVersionAtMost(JavaVersion.JAVA_17);
        log.info("javaVersionAtMost:{}", javaVersionAtMost);
    }

}
