package com.moqi.in20220508;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;

/**
 * @author moqi
 * @date 5/8/22 16:04
 */
@Slf4j
public class NumberUtilsTest {

    public static void main(String[] args) {
        long l1 = NumberUtils.toLong(null);
        log.info("l1:{}", l1);

        long l2 = NumberUtils.toLong("-2222333334");
        log.info("l2:{}", l2);

        //noinspection ConstantConditions
        boolean creatable1 = NumberUtils.isCreatable(null);
        log.info("creatable1:{}", creatable1);

        boolean creatable2 = NumberUtils.isCreatable("9999");
        log.info("creatable2:{}", creatable2);

        boolean parsable1 = NumberUtils.isParsable("   9999   ");
        log.info("parsable1:{}", parsable1);

        boolean parsable2 = NumberUtils.isParsable("3.1415926");
        log.info("parsable2:{}", parsable2);
    }

}
