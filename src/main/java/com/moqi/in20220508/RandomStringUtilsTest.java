package com.moqi.in20220508;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;

/**
 * @author moqi
 * @date 5/8/22 15:03
 */
@Slf4j
public class RandomStringUtilsTest {

    private static final int FIVE = 5;

    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            String random = RandomStringUtils.random(10);
            log.info("random:{}", random);
        }
        System.out.println("==============================");

        for (int i = 0; i < 5; i++) {
            String randomAlphabetic = RandomStringUtils.randomAlphabetic(10);
            log.info("randomAlphabetic:{}", randomAlphabetic);
        }
        System.out.println("==============================");

        for (int i = 0; i < 5; i++) {
            String randomAlphanumeric = RandomStringUtils.randomAlphanumeric(10);
            log.info("randomAlphanumeric:{}", randomAlphanumeric);
        }
        System.out.println("==============================");

        for (int i = 0; i < FIVE; i++) {
            String randomAscii = RandomStringUtils.randomAscii(10);
            log.info("randomAscii:{}", randomAscii);
        }
        System.out.println("==============================");

        for (int i = 0; i < FIVE; i++) {
            String randomGraph = RandomStringUtils.randomGraph(10);
            log.info("randomGraph:{}", randomGraph);
        }
        System.out.println("==============================");

        for (int i = 0; i < FIVE; i++) {
            String randomNumeric = RandomStringUtils.randomNumeric(10);
            log.info("randomNumeric:{}", randomNumeric);
        }
        System.out.println("==============================");

        for (int i = 0; i < FIVE; i++) {
            String randomPrint = RandomStringUtils.randomPrint(10);
            log.info("randomPrint:{}", randomPrint);
        }
        System.out.println("==============================");

    }

}
