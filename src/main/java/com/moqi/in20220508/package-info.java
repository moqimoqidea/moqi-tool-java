/**
 * 练习 apache commons-lang3 下工具
 *
 * 3.13+ 发布后可练习 FluentBitSet.
 *
 * @author moqi
 * @date 5/8/22 16:41
 * @see <a href="https://github.com/apache/commons-lang/blob/master/src/main/java/org/apache/commons/lang3/util/FluentBitSet.java">FluentBitSet.java</a>
 * @see <a href="https://github.com/apache/commons-lang/blob/master/src/test/java/org/apache/commons/lang3/util/FluentBitSetTest.java">FluentBitSetTest.java</a>
 */
package com.moqi.in20220508;