package com.moqi.archive.in20210827;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * @author moqi
 * On 8/27/21 16:47
 */
@Slf4j
public class ListTest {

    public static void main(String[] args) {

        List<Person> list = new ArrayList<>();
        list.add(Person.builder().name("tom").age(18).build());
        log.info("list = " + list);

    }

}

@Data
@Builder
class Person {
    private String name;
    private int age;
}
