package com.moqi.archive.in20210729;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

/**
 * 测试 Stream 排序包含空
 *
 * @author moqi
 * @date 7/29/21 11:10
 */
@Slf4j
public class StreamSortWithNull {

    public static void main(String[] args) {
        List<String> collect = Stream.of(null, null, "a", "b", "c").sorted(
                Comparator.nullsFirst(Comparator.naturalOrder())
        ).collect(Collectors.toList());
        log.info("collect = " + collect);

        List<String> collect1 = LongStream.range(0, 10).mapToObj(x -> "asdf").collect(Collectors.toList());
        log.info("collect1 = " + collect1);


        List<Person> personList = new ArrayList<>();
        Person p1 = new Person();
        p1.setName("a1");
        personList.add(p1);
        Person p2 = new Person();
        p2.setName("b1");
        personList.add(p2);

        personList.add(null);
        personList.add(null);
        personList.add(null);
        personList.add(null);
        Person empty = new Person();
        empty.setName("");
        personList.add(empty);

        List<Person> finalPersonList = personList.stream().sorted(
                Comparator.nullsFirst(Comparator.comparing(Person::getName))
        ).collect(Collectors.toList());
        log.info("finalPersonList = " + finalPersonList);
    }

}

class Person {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                '}';
    }
}
