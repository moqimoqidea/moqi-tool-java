package com.moqi.archive.in20210729;

import java.util.Random;
import java.util.stream.IntStream;

/**
 * @author moqi
 * @date 7/29/21 12:38
 */
public class RandomIpGenerate {

    public static void main(String[] args) {
        Random r = new Random();

        IntStream.range(0, 10)
                .mapToObj(x -> r.nextInt(256) + "." + r.nextInt(256) + "." + r.nextInt(256) + "." + r.nextInt(256))
                .forEach(System.out::println);
    }
}
