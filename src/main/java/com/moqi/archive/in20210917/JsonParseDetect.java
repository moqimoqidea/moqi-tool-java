package com.moqi.archive.in20210917;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * @author moqi
 * @date 9/17/21 10:49
 */
public class JsonParseDetect {

    public static void main(String[] args) {
        String jsonArray = getJsonArray();
        List<WeiboFunctionDetectPO> list = JSON.parseArray(jsonArray, WeiboFunctionDetectPO.class);
        System.out.println("list = " + list);
    }

    private static String getJsonArray() {
        return "[\n" +
                "        {\n" +
                "          \"functionName\": \"nimaTest\",\n" +
                "          \"functionData\": {\n" +
                "            \"connect_timeout\": 8000,\n" +
                "            \"read_timeout\": 8000,\n" +
                "            \"execute_timeout\": 8000,\n" +
                "            \"request\": {\n" +
                "              \"body\": {\n" +
                "                \"priority\": 1,\n" +
                "                \"weight\": 1,\n" +
                "                \"data\": \"{\\\"imageUrl\\\":\\\"https://public.video.weibocdn.com/5Umozx8zlx07NEEqbdYA010f12002StO0E019.tar\\\",\\\"fileInfo\\\":\\\"[{\\\\\\\"frame_offset\\\\\\\":36.0,\\\\\\\"file_name\\\\\\\":\\\\\\\"2.jpg\\\\\\\"},{\\\\\\\"frame_offset\\\\\\\":132.0,\\\\\\\"file_name\\\\\\\":\\\\\\\"6.jpg\\\\\\\"},{\\\\\\\"frame_offset\\\\\\\":156.0,\\\\\\\"file_name\\\\\\\":\\\\\\\"7.jpg\\\\\\\"},{\\\\\\\"frame_offset\\\\\\\":12.0,\\\\\\\"file_name\\\\\\\":\\\\\\\"1.jpg\\\\\\\"},{\\\\\\\"frame_offset\\\\\\\":228.0,\\\\\\\"file_name\\\\\\\":\\\\\\\"10.jpg\\\\\\\"},{\\\\\\\"frame_offset\\\\\\\":180.0,\\\\\\\"file_name\\\\\\\":\\\\\\\"8.jpg\\\\\\\"},{\\\\\\\"frame_offset\\\\\\\":84.0,\\\\\\\"file_name\\\\\\\":\\\\\\\"4.jpg\\\\\\\"},{\\\\\\\"frame_offset\\\\\\\":108.0,\\\\\\\"file_name\\\\\\\":\\\\\\\"5.jpg\\\\\\\"},{\\\\\\\"frame_offset\\\\\\\":60.0,\\\\\\\"file_name\\\\\\\":\\\\\\\"3.jpg\\\\\\\"},{\\\\\\\"frame_offset\\\\\\\":204.0,\\\\\\\"file_name\\\\\\\":\\\\\\\"9.jpg\\\\\\\"}]\\\",\\\"mediaId\\\":\\\"4676979937706030\\\"}\",\n" +
                "                \"callback_info\": {\n" +
                "                  \"url\": \"\",\n" +
                "                  \"passthrough_params\": {\n" +
                "                    \"a\": \"b\",\n" +
                "                    \"vv\": \"444444\"\n" +
                "                  }\n" +
                "                }\n" +
                "              }\n" +
                "            },\n" +
                "            \"callbackRules\": [\n" +
                "              {\n" +
                "                \"rule\": \"$.context.status == 'abc'\",\n" +
                "                \"error_message\": \"context status is not abc!\"\n" +
                "              },\n" +
                "              {\n" +
                "                \"rule\": \"$.response.data.result.avg < 3\",\n" +
                "                \"error_message\": \"response.data.result.avg litter than 3!\"\n" +
                "              },\n" +
                "              {\n" +
                "                \"rule\": \"it's just not a expression\",\n" +
                "                \"error_message\": \"it's just not a expression\"\n" +
                "              },\n" +
                "              {\n" +
                "                \"rule\": \"$.response.data.result.avg > 8\",\n" +
                "                \"error_message\": \"response.data.result.avg great than 8!\",\n" +
                "                \"use\": false\n" +
                "              }\n" +
                "            ]\n" +
                "          }\n" +
                "        }\n" +
                "      ]";
    }

}

@NoArgsConstructor
@Data
class WeiboFunctionDetectPO {
    private String functionName;
    private FunctionDataBean functionData;

    @NoArgsConstructor
    @Data
    public static class FunctionDataBean {

        @JSONField(name = "connect_timeout")
        private Long connectTimeout;
        @JSONField(name = "read_timeout")
        private Long readTimeout;
        @JSONField(name = "execute_timeout")
        private Long executeTimeout;
        private RequestBean request;
        private List<WeiboFunctionCallbackRulePO> callbackRules;

        @NoArgsConstructor
        @Data
        public static class RequestBean {

            private FunctionInvokeBody body;

            @NoArgsConstructor
            @Data
            public static class FunctionInvokeBody {

                private Integer priority;
                private Integer weight;
                private Object data;

                @JSONField(name = "callback_info")
                private CallbackInfoBean callbackInfo;

                @NoArgsConstructor
                @Data
                public static class CallbackInfoBean {

                    private String url;
                    @JSONField(name = "passthrough_params")
                    private Map<String, String> passthroughParams;
                }
            }
        }
    }
}

@NoArgsConstructor
@Data
class WeiboFunctionCallbackRulePO {

    private String rule;

    @JSONField(name = "error_message")
    private String errorMessage;

    /**
     * use 字段为 false 时忽略此条规则
     */
    private Boolean use;

}
