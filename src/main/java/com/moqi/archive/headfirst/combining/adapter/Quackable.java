package com.moqi.archive.headfirst.combining.adapter;

public interface Quackable {
	void quack();
}
