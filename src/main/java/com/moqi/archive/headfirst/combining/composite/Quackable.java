package com.moqi.archive.headfirst.combining.composite;

public interface Quackable {
    void quack();
}
