package com.moqi.archive.headfirst.combining.ducks;

public interface Quackable {
    void quack();
}
