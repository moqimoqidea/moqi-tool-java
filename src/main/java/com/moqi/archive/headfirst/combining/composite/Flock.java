package com.moqi.archive.headfirst.combining.composite;

import java.util.ArrayList;

public class Flock implements Quackable {
    ArrayList<Quackable> quackerList = new ArrayList<>();

    public void add(Quackable quacker) {
        quackerList.add(quacker);
    }

    public void quack() {
        for (Quackable quacker : quackerList) {
            quacker.quack();
        }
    }

    public String toString() {
        return "Flock of Quackers";
    }
}
