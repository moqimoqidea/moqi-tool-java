package com.moqi.archive.headfirst.combining.ducks;

public class DuckSimulator {

    /**
     * Duck Simulator
     * Quack
     * Quack
     * Kwak
     * Squeak
     */
    public static void main(String[] args) {
        new DuckSimulator().simulate();
    }

    void simulate() {
        Quackable mallardDuck = new MallardDuck();
        Quackable redheadDuck = new RedheadDuck();
        Quackable duckCall = new DuckCall();
        Quackable rubberDuck = new RubberDuck();

        System.out.println("\nDuck Simulator");

        simulate(mallardDuck);
        simulate(redheadDuck);
        simulate(duckCall);
        simulate(rubberDuck);
    }

    void simulate(Quackable duck) {
        duck.quack();
    }

}
