package com.moqi.archive.headfirst.combining.factory;

public interface Quackable {
	void quack();
}
