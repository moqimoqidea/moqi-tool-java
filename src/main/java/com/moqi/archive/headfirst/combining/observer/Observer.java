package com.moqi.archive.headfirst.combining.observer;

public interface Observer {
    void update(QuackObservable duck);
}
