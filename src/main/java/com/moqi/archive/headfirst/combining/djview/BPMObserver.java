package com.moqi.archive.headfirst.combining.djview;

public interface BPMObserver {
    void updateBPM();
}
