package com.moqi.archive.headfirst.combining.decorator;

public interface Quackable {
	void quack();
}
