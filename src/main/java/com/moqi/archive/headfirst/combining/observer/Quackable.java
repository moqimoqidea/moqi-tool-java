package com.moqi.archive.headfirst.combining.observer;

public interface Quackable extends QuackObservable {
    void quack();
}
