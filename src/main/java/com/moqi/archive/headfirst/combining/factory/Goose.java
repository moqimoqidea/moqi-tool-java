package com.moqi.archive.headfirst.combining.factory;

public class Goose {
	public void honk() {
		System.out.println("Honk");
	}

	public String toString() {
		return "Goose";
	}
}
