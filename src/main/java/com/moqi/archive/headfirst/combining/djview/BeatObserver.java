package com.moqi.archive.headfirst.combining.djview;

public interface BeatObserver {
    void updateBeat();
}
