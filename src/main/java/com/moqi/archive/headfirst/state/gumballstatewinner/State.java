package com.moqi.archive.headfirst.state.gumballstatewinner;

interface State {

    void insertQuarter();

    void ejectQuarter();

    void turnCrank();

    void dispense();

    void refill();

}
