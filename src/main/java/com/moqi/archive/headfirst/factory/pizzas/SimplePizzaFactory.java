package com.moqi.archive.headfirst.factory.pizzas;

/**
 * Created by Gavin on 2017/3/9.
 */
public class SimplePizzaFactory {
    public Pizza createPizza(String type) {
        Pizza pizza;

        switch (type) {
            case "cheese":
                pizza = new CheesePizza();
                break;
            case "pepperoni":
                pizza = new PepperoniPizza();
                break;
            case "clam":
                pizza = new ClamPizza();
                break;
            case "veggie":
                pizza = new VeggiePizza();
                break;
            default:
                pizza = null;
                break;
        }

        return pizza;
    }
}
