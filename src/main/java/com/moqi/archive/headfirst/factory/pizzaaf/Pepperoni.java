package com.moqi.archive.headfirst.factory.pizzaaf;

/**
 * Created by Gavin on 2017/3/9.
 */
public interface Pepperoni {
    String toString();
}
