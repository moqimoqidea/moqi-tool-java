package com.moqi.archive.headfirst.factory.pizzafm;

/**
 * Created by Gavin on 2017/3/9.
 */
public class PizzaTestDrive {

    /**
     * Ethan ordered a NY Style Sauce and Cheese Pizza
     *
     * --- Making a Chicago Style Deep Dish Cheese Pizza ---
     * Prepare Chicago Style Deep Dish Cheese Pizza
     * Tossing dough...
     * Adding sauce...
     * Adding toppings:
     *    Shredded Mozzarella Cheese
     * Bake for 25 minutes at 350
     * Cutting the pizza into square slices
     * Place pizza in official PizzaStore box
     * Joel ordered a Chicago Style Deep Dish Cheese Pizza
     *
     * --- Making a NY Style Clam Pizza ---
     * Prepare NY Style Clam Pizza
     * Tossing dough...
     * Adding sauce...
     * Adding toppings:
     *    Grated Reggiano Cheese
     *    Fresh Clams from Long Island Sound
     * Bake for 25 minutes at 350
     * Cut the pizza into diagonal slices
     * Place pizza in official PizzaStore box
     * Ethan ordered a NY Style Clam Pizza
     *
     * --- Making a Chicago Style Clam Pizza ---
     * Prepare Chicago Style Clam Pizza
     * Tossing dough...
     * Adding sauce...
     * Adding toppings:
     *    Shredded Mozzarella Cheese
     *    Frozen Clams from Chesapeake Bay
     * Bake for 25 minutes at 350
     * Cutting the pizza into square slices
     * Place pizza in official PizzaStore box
     * Joel ordered a Chicago Style Clam Pizza
     *
     * --- Making a NY Style Pepperoni Pizza ---
     * Prepare NY Style Pepperoni Pizza
     * Tossing dough...
     * Adding sauce...
     * Adding toppings:
     *    Grated Reggiano Cheese
     *    Sliced Pepperoni
     *    Garlic
     *    Onion
     *    Mushrooms
     *    Red Pepper
     * Bake for 25 minutes at 350
     * Cut the pizza into diagonal slices
     * Place pizza in official PizzaStore box
     * Ethan ordered a NY Style Pepperoni Pizza
     *
     * --- Making a Chicago Style Pepperoni Pizza ---
     * Prepare Chicago Style Pepperoni Pizza
     * Tossing dough...
     * Adding sauce...
     * Adding toppings:
     *    Shredded Mozzarella Cheese
     *    Black Olives
     *    Spinach
     *    Eggplant
     *    Sliced Pepperoni
     * Bake for 25 minutes at 350
     * Cutting the pizza into square slices
     * Place pizza in official PizzaStore box
     * Joel ordered a Chicago Style Pepperoni Pizza
     *
     * --- Making a NY Style Veggie Pizza ---
     * Prepare NY Style Veggie Pizza
     * Tossing dough...
     * Adding sauce...
     * Adding toppings:
     *    Grated Reggiano Cheese
     *    Garlic
     *    Onion
     *    Mushrooms
     *    Red Pepper
     * Bake for 25 minutes at 350
     * Cut the pizza into diagonal slices
     * Place pizza in official PizzaStore box
     * Ethan ordered a NY Style Veggie Pizza
     *
     * --- Making a Chicago Deep Dish Veggie Pizza ---
     * Prepare Chicago Deep Dish Veggie Pizza
     * Tossing dough...
     * Adding sauce...
     * Adding toppings:
     *    Shredded Mozzarella Cheese
     *    Black Olives
     *    Spinach
     *    Eggplant
     * Bake for 25 minutes at 350
     * Cutting the pizza into square slices
     * Place pizza in official PizzaStore box
     * Joel ordered a Chicago Deep Dish Veggie Pizza
     */
    public static void main(String[] args) {
        PizzaStore nyStore = new NYPizzaStore();
        PizzaStore chicagoStore = new ChicagoPizzaStore();

        Pizza pizza = nyStore.createPizza("cheese");
        System.out.println("Ethan ordered a " + pizza.getName() + "\n");

        pizza = chicagoStore.orderPizza("cheese");
        System.out.println("Joel ordered a " + pizza.getName() + "\n");

        pizza = nyStore.orderPizza("clam");
        System.out.println("Ethan ordered a " + pizza.getName() + "\n");

        pizza = chicagoStore.orderPizza("clam");
        System.out.println("Joel ordered a " + pizza.getName() + "\n");

        pizza = nyStore.orderPizza("pepperoni");
        System.out.println("Ethan ordered a " + pizza.getName() + "\n");

        pizza = chicagoStore.orderPizza("pepperoni");
        System.out.println("Joel ordered a " + pizza.getName() + "\n");

        pizza = nyStore.orderPizza("veggie");
        System.out.println("Ethan ordered a " + pizza.getName() + "\n");

        pizza = chicagoStore.orderPizza("veggie");
        System.out.println("Joel ordered a " + pizza.getName() + "\n");

    }

}
