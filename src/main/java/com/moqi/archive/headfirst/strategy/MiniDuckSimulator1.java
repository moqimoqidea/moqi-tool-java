package com.moqi.archive.headfirst.strategy;

/**
 * Created by Gavin on 2017/2/10.
 */
public class MiniDuckSimulator1 {

    /**
     * Quack
     * I'm flying!!I can't fly
     * I'm flying with a rocket
     */
    public static void main(String[] args) {
        Duck mallard = new MallardDuck();
        mallard.performQuack();
        mallard.performFly();

        Duck model = new ModelDuck();
        model.performFly();
        model.setFlyBehavior(new FlyRocketPowered());
        model.performFly();
    }

}
