package com.moqi.archive.headfirst.strategy;

/**
 * Created by Gavin on 2017/2/10.
 */
public interface QuackBehavior {
    void quack();
}
