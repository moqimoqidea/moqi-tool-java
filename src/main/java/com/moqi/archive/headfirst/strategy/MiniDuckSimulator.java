package com.moqi.archive.headfirst.strategy;

/**
 * Created by Gavin on 2017/2/10.
 */
public class MiniDuckSimulator {

    /**
     * Quack
     * Squeak
     * << Silence >>
     * Quack
     * I'm flying with a rocket
     */
    public static void main(String[] args) {
        MallardDuck mallard = new MallardDuck();
        RubberDuck rubberDuck = new RubberDuck();
        DecoyDuck decoy = new DecoyDuck();

        Duck model = new ModelDuck();

        mallard.performQuack();
        rubberDuck.performQuack();
        decoy.performQuack();

        model.performQuack();
        model.setFlyBehavior(new FlyRocketPowered());
        model.performFly();
    }

}
