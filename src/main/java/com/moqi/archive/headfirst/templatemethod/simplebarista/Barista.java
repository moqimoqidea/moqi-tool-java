package com.moqi.archive.headfirst.templatemethod.simplebarista;

/**
 * Created by Gavin on 2017/3/17.
 */
public class Barista {

    /**
     * Making tea...
     * Boiling water
     * Steeping the tea
     * Pouring into cup
     * Adding Lemon
     *
     * Making coffee...
     * Boiling water
     * Dripping Coffee through filter
     * Pouring into cup
     * Adding Sugar and Milk
     */
    public static void main(String[] args) {
        System.out.println("Making tea...");
        Tea tea = new Tea();
        tea.prepareRecipe();

        System.out.println();

        System.out.println("Making coffee...");
        Coffee coffee = new Coffee();
        coffee.prepareRecipe();
    }

}
