package com.moqi.archive.headfirst.templatemethod.sort;

/**
 * Created by Gavin on 2017/3/20.
 */
public class Duck implements Comparable<Duck> {

    String name;
    int weight;

    public Duck(String name, int weight) {
        this.name = name;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return name + " weighs " + weight;
    }

    @Override
    public int compareTo(Duck object) {
        return Integer.compare(this.weight, object.weight);
    }

}
