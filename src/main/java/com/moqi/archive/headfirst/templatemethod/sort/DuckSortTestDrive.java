package com.moqi.archive.headfirst.templatemethod.sort;

import java.util.Arrays;

/**
 * Created by Gavin on 2017/3/20.
 */
public class DuckSortTestDrive {

    /**
     * Before sorting
     * Daffy weighs 8
     * Dewey weighs 2
     * Howard weighs 7
     * Louie weighs 2
     * Donald weighs 10
     * Huey weighs 2
     *
     * After sorting:
     * Dewey weighs 2
     * Louie weighs 2
     * Huey weighs 2
     * Howard weighs 7
     * Daffy weighs 8
     * Donald weighs 10
     */
    public static void main(String[] args) {
        Duck[] ducks = {
                new Duck("Daffy", 8),
                new Duck("Dewey", 2),
                new Duck("Howard", 7),
                new Duck("Louie", 2),
                new Duck("Donald", 10),
                new Duck("Huey", 2)
        };

        System.out.println("Before sorting");
        display(ducks);

        Arrays.sort(ducks);

        System.out.println("\nAfter sorting:");
        display(ducks);

    }

    public static void display(Duck[] ducks) {
        for (Duck d : ducks) {
            System.out.println(d);
        }
    }

}
