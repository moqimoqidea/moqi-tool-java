package com.moqi.archive.headfirst.templatemethod.barista;

public class BeverageTestDrive {

    /**
     * Making tea...
     * Boiling water
     * Steeping the tea
     * Pouring into cup
     * Adding Lemon
     *
     * Making coffee...
     * Boiling water
     * Dripping Coffee through filter
     * Pouring into cup
     * Adding Sugar and Milk
     *
     * Making tea...
     * Boiling water
     * Steeping the tea
     * Pouring into cup
     * Would you like lemon with your tea (y/n)? y
     * Adding Lemon
     *
     * Making coffee...
     * Boiling water
     * Dripping Coffee through filter
     * Pouring into cup
     * Would you like milk and sugar with your coffee (y/n)? n
     */
    public static void main(String[] args) {

        Tea tea = new Tea();
        Coffee coffee = new Coffee();

        System.out.println("\nMaking tea...");
        tea.prepareRecipe();

        System.out.println("\nMaking coffee...");
        coffee.prepareRecipe();


        TeaWithHook teaHook = new TeaWithHook();
        CoffeeWithHook coffeeHook = new CoffeeWithHook();

        System.out.println("\nMaking tea...");
        teaHook.prepareRecipe();

        System.out.println("\nMaking coffee...");
        coffeeHook.prepareRecipe();
    }

}
