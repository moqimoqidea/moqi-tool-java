package com.moqi.archive.headfirst.command.party;

public class RemoteLoader {

    /**
     * ------ Remote Control -------
     * [slot 0] com.moqi.archive.headfirst.command.party.MacroCommand    com.moqi.archive.headfirst.command.party.MacroCommand
     * [slot 1] com.moqi.archive.headfirst.command.party.NoCommand    com.moqi.archive.headfirst.command.party.NoCommand
     * [slot 2] com.moqi.archive.headfirst.command.party.NoCommand    com.moqi.archive.headfirst.command.party.NoCommand
     * [slot 3] com.moqi.archive.headfirst.command.party.NoCommand    com.moqi.archive.headfirst.command.party.NoCommand
     * [slot 4] com.moqi.archive.headfirst.command.party.NoCommand    com.moqi.archive.headfirst.command.party.NoCommand
     * [slot 5] com.moqi.archive.headfirst.command.party.NoCommand    com.moqi.archive.headfirst.command.party.NoCommand
     * [slot 6] com.moqi.archive.headfirst.command.party.NoCommand    com.moqi.archive.headfirst.command.party.NoCommand
     * [undo] com.moqi.archive.headfirst.command.party.NoCommand
     *
     * --- Pushing Macro On---
     * Light is on
     * Living Room stereo is on
     * Living Room TV is on
     * Living Room TV channel is set for DVD
     * Hottub is heating to a steaming 104 degrees
     * Hottub is bubbling!
     * --- Pushing Macro Off---
     * Light is off
     * Living Room stereo is off
     * Living Room TV is off
     * Hottub is cooling to 98 degrees
     */
    public static void main(String[] args) {
        RemoteControl remoteControl = new RemoteControl();

        Light light = new Light("Living Room");
        TV tv = new TV("Living Room");
        Stereo stereo = new Stereo("Living Room");
        Hottub hottub = new Hottub();

        LightOnCommand lightOn = new LightOnCommand(light);
        StereoOnCommand stereoOn = new StereoOnCommand(stereo);
        TVOnCommand tvOn = new TVOnCommand(tv);
        HottubOnCommand hottubOn = new HottubOnCommand(hottub);

        LightOffCommand lightOff = new LightOffCommand(light);
        StereoOffCommand stereoOff = new StereoOffCommand(stereo);
        TVOffCommand tvOff = new TVOffCommand(tv);
        HottubOffCommand hottubOff = new HottubOffCommand(hottub);

        Command[] partyOn = {lightOn, stereoOn, tvOn, hottubOn};
        Command[] partyOff = {lightOff, stereoOff, tvOff, hottubOff};

        MacroCommand partyOnMacro = new MacroCommand(partyOn);
        MacroCommand partyOffMacro = new MacroCommand(partyOff);

        remoteControl.setCommand(0, partyOnMacro, partyOffMacro);

        System.out.println(remoteControl);
        System.out.println("--- Pushing Macro On---");
        remoteControl.onButtonWasPushed(0);
        System.out.println("--- Pushing Macro Off---");
        remoteControl.offButtonWasPushed(0);
    }

}
