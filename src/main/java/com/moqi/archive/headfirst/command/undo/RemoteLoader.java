package com.moqi.archive.headfirst.command.undo;

/**
 * Created by Gavin on 2017/3/14.
 */
public class RemoteLoader {

    /**
     * Light is on
     * Light is off
     *
     * ------ Remote Control -------
     * [slot 0] com.moqi.archive.headfirst.command.undo.LightOnCommand    com.moqi.archive.headfirst.command.undo.LightOffCommand
     * [slot 1] com.moqi.archive.headfirst.command.undo.NoCommand    com.moqi.archive.headfirst.command.undo.NoCommand
     * [slot 2] com.moqi.archive.headfirst.command.undo.NoCommand    com.moqi.archive.headfirst.command.undo.NoCommand
     * [slot 3] com.moqi.archive.headfirst.command.undo.NoCommand    com.moqi.archive.headfirst.command.undo.NoCommand
     * [slot 4] com.moqi.archive.headfirst.command.undo.NoCommand    com.moqi.archive.headfirst.command.undo.NoCommand
     * [slot 5] com.moqi.archive.headfirst.command.undo.NoCommand    com.moqi.archive.headfirst.command.undo.NoCommand
     * [slot 6] com.moqi.archive.headfirst.command.undo.NoCommand    com.moqi.archive.headfirst.command.undo.NoCommand
     * [undo] com.moqi.archive.headfirst.command.undo.LightOffCommand
     *
     * Light is dimmed to 100%
     * Light is off
     * Light is on
     *
     * ------ Remote Control -------
     * [slot 0] com.moqi.archive.headfirst.command.undo.LightOnCommand    com.moqi.archive.headfirst.command.undo.LightOffCommand
     * [slot 1] com.moqi.archive.headfirst.command.undo.NoCommand    com.moqi.archive.headfirst.command.undo.NoCommand
     * [slot 2] com.moqi.archive.headfirst.command.undo.NoCommand    com.moqi.archive.headfirst.command.undo.NoCommand
     * [slot 3] com.moqi.archive.headfirst.command.undo.NoCommand    com.moqi.archive.headfirst.command.undo.NoCommand
     * [slot 4] com.moqi.archive.headfirst.command.undo.NoCommand    com.moqi.archive.headfirst.command.undo.NoCommand
     * [slot 5] com.moqi.archive.headfirst.command.undo.NoCommand    com.moqi.archive.headfirst.command.undo.NoCommand
     * [slot 6] com.moqi.archive.headfirst.command.undo.NoCommand    com.moqi.archive.headfirst.command.undo.NoCommand
     * [undo] com.moqi.archive.headfirst.command.undo.LightOnCommand
     *
     * Light is off
     * Living Room ceiling fan is on medium
     * Living Room ceiling fan is off
     *
     * ------ Remote Control -------
     * [slot 0] com.moqi.archive.headfirst.command.undo.CeilingFanMediumCommand    com.moqi.archive.headfirst.command.undo.CeilingFanOffCommand
     * [slot 1] com.moqi.archive.headfirst.command.undo.CeilingFanHighCommand    com.moqi.archive.headfirst.command.undo.CeilingFanOffCommand
     * [slot 2] com.moqi.archive.headfirst.command.undo.NoCommand    com.moqi.archive.headfirst.command.undo.NoCommand
     * [slot 3] com.moqi.archive.headfirst.command.undo.NoCommand    com.moqi.archive.headfirst.command.undo.NoCommand
     * [slot 4] com.moqi.archive.headfirst.command.undo.NoCommand    com.moqi.archive.headfirst.command.undo.NoCommand
     * [slot 5] com.moqi.archive.headfirst.command.undo.NoCommand    com.moqi.archive.headfirst.command.undo.NoCommand
     * [slot 6] com.moqi.archive.headfirst.command.undo.NoCommand    com.moqi.archive.headfirst.command.undo.NoCommand
     * [undo] com.moqi.archive.headfirst.command.undo.CeilingFanOffCommand
     *
     * Living Room ceiling fan is on medium
     * Living Room ceiling fan is on high
     *
     * ------ Remote Control -------
     * [slot 0] com.moqi.archive.headfirst.command.undo.CeilingFanMediumCommand    com.moqi.archive.headfirst.command.undo.CeilingFanOffCommand
     * [slot 1] com.moqi.archive.headfirst.command.undo.CeilingFanHighCommand    com.moqi.archive.headfirst.command.undo.CeilingFanOffCommand
     * [slot 2] com.moqi.archive.headfirst.command.undo.NoCommand    com.moqi.archive.headfirst.command.undo.NoCommand
     * [slot 3] com.moqi.archive.headfirst.command.undo.NoCommand    com.moqi.archive.headfirst.command.undo.NoCommand
     * [slot 4] com.moqi.archive.headfirst.command.undo.NoCommand    com.moqi.archive.headfirst.command.undo.NoCommand
     * [slot 5] com.moqi.archive.headfirst.command.undo.NoCommand    com.moqi.archive.headfirst.command.undo.NoCommand
     * [slot 6] com.moqi.archive.headfirst.command.undo.NoCommand    com.moqi.archive.headfirst.command.undo.NoCommand
     * [undo] com.moqi.archive.headfirst.command.undo.CeilingFanHighCommand
     *
     * Living Room ceiling fan is on medium
     */
    public static void main(String[] args) {
        RemoteControlWithUndo remoteControl = new RemoteControlWithUndo();

        Light livingRoomLight = new Light("Living Room");

        LightOnCommand livingRoomLightOn = new LightOnCommand(livingRoomLight);
        LightOffCommand livingRoomLightOff = new LightOffCommand(livingRoomLight);

        remoteControl.setCommand(0, livingRoomLightOn, livingRoomLightOff);

        remoteControl.onButtonWasPushed(0);
        remoteControl.offButtonWasPushed(0);
        System.out.println(remoteControl);
        remoteControl.undoButtonWasPushed();
        remoteControl.offButtonWasPushed(0);
        remoteControl.onButtonWasPushed(0);
        System.out.println(remoteControl);
        remoteControl.undoButtonWasPushed();

        CeilingFan ceilingFan = new CeilingFan("Living Room");

        CeilingFanMediumCommand ceilingFanMedium = new CeilingFanMediumCommand(ceilingFan);
        CeilingFanHighCommand ceilingFanHigh = new CeilingFanHighCommand(ceilingFan);
        CeilingFanOffCommand ceilingFanOff = new CeilingFanOffCommand(ceilingFan);

        remoteControl.setCommand(0, ceilingFanMedium, ceilingFanOff);
        remoteControl.setCommand(1, ceilingFanHigh, ceilingFanOff);

        remoteControl.onButtonWasPushed(0);
        remoteControl.offButtonWasPushed(0);
        System.out.println(remoteControl);
        remoteControl.undoButtonWasPushed();

        remoteControl.onButtonWasPushed(1);
        System.out.println(remoteControl);
        remoteControl.undoButtonWasPushed();
    }

}
