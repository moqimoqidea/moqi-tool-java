package com.moqi.archive.headfirst.command.remote;

/**
 * Created by Gavin on 2017/3/14.
 */
public class RemoteLoader {

    /**
     * ------ Remote Control -------
     * [slot 0] com.moqi.archive.headfirst.command.remote.LightOnCommand    com.moqi.archive.headfirst.command.remote.LightOffCommand
     * [slot 1] com.moqi.archive.headfirst.command.remote.LightOnCommand    com.moqi.archive.headfirst.command.remote.LightOffCommand
     * [slot 2] com.moqi.archive.headfirst.command.remote.CeilingFanOnCommand    com.moqi.archive.headfirst.command.remote.CeilingFanOffCommand
     * [slot 3] com.moqi.archive.headfirst.command.remote.StereoOnWithCDCommand    com.moqi.archive.headfirst.command.remote.StereoOffCommand
     * [slot 4] com.moqi.archive.headfirst.command.remote.NoCommand    com.moqi.archive.headfirst.command.remote.NoCommand
     * [slot 5] com.moqi.archive.headfirst.command.remote.NoCommand    com.moqi.archive.headfirst.command.remote.NoCommand
     * [slot 6] com.moqi.archive.headfirst.command.remote.NoCommand    com.moqi.archive.headfirst.command.remote.NoCommand
     *
     * Living Room light is on
     * Living Room light is off
     * Kitchen light is on
     * Kitchen light is off
     * Living Room ceiling fan is on high
     * Living Room ceiling fan is off
     * Living Room stereo is on
     * Living Room stereo is set for CD input
     * Living Room Stereo volume set to 11
     * Living Room stereo is off
     */
    public static void main(String[] args) {
        RemoteControl remoteControl = new RemoteControl();

        Light livingRoomLight = new Light("Living Room");
        Light kitchenLight = new Light("Kitchen");
        CeilingFan ceilingFan = new CeilingFan("Living Room");
        GarageDoor garageDoor = new GarageDoor("");
        Stereo stereo = new Stereo("Living Room");

        LightOnCommand livingRoomLightOn = new LightOnCommand(livingRoomLight);
        LightOffCommand livingRoomLightOff = new LightOffCommand(livingRoomLight);
        LightOnCommand kitchenLightOn = new LightOnCommand(kitchenLight);
        LightOffCommand kitchenLightOff = new LightOffCommand(kitchenLight);

        CeilingFanOnCommand ceilingFanOn = new CeilingFanOnCommand(ceilingFan);
        CeilingFanOffCommand ceilingFanOff = new CeilingFanOffCommand(ceilingFan);

        GarageDoorUpCommand garageDoorUp = new GarageDoorUpCommand(garageDoor);
        GarageDoorDownCommand garageDoorDown = new GarageDoorDownCommand(garageDoor);

        StereoOnWithCDCommand stereoOnWithCD = new StereoOnWithCDCommand(stereo);
        StereoOffCommand stereoOff = new StereoOffCommand(stereo);

        remoteControl.setCommand(0, livingRoomLightOn, livingRoomLightOff);
        remoteControl.setCommand(1, kitchenLightOn, kitchenLightOff);
        remoteControl.setCommand(2, ceilingFanOn, ceilingFanOff);
        remoteControl.setCommand(3, stereoOnWithCD, stereoOff);

        System.out.println(remoteControl);

        remoteControl.onButtonWasPushed(0);
        remoteControl.offButtonWasPushed(0);
        remoteControl.onButtonWasPushed(1);
        remoteControl.offButtonWasPushed(1);
        remoteControl.onButtonWasPushed(2);
        remoteControl.offButtonWasPushed(2);
        remoteControl.onButtonWasPushed(3);
        remoteControl.offButtonWasPushed(3);
    }

}
