package com.moqi.archive.headfirst.command.simpleremoteWL;

/**
 * Created by Gavin on 2017/3/14.
 */
public class RemoteControlTest {

    /**
     * Light is on
     * Garage Door is Open
     * Garage light is on
     * Garage light is off
     */
    public static void main(String[] args) {
        SimpleRemoteControl remote = new SimpleRemoteControl();
        Light light = new Light();
        remote.setCommand(light::on);
        remote.buttonWasPressed();

        GarageDoor garageDoor = new GarageDoor();
        remote.setCommand(garageDoor::up);
        remote.buttonWasPressed();
        remote.setCommand(garageDoor::lightOn);
        remote.buttonWasPressed();
        remote.setCommand(garageDoor::lightOff);
        remote.buttonWasPressed();

        // 初始形式
        // remote.setCommand(new Command() {
        //     @Override
        //     public void execute() {
        //         light.on();
        //     }
        // });

        // 中间形式
        // remote.setCommand(() -> light.on());

        // 最终形式
        // remote.setCommand(light::on);
    }

}
