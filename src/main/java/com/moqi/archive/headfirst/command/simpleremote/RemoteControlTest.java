package com.moqi.archive.headfirst.command.simpleremote;

/**
 * Created by Gavin on 2017/3/14.
 */
public class RemoteControlTest {

    /**
     * Light is on
     * Garage Door is Open
     */
    public static void main(String[] args) {
        SimpleRemoteControl remote = new SimpleRemoteControl();

        Light light = new Light();
        LightOnCommand lightOn = new LightOnCommand(light);
        remote.setCommand(lightOn);
        remote.buttonWasPressed();

        GarageDoor garageDoor = new GarageDoor();
        GarageDoorOpenCommand garageDoorOpen = new GarageDoorOpenCommand(garageDoor);
        remote.setCommand(garageDoorOpen);
        remote.buttonWasPressed();
    }

}
