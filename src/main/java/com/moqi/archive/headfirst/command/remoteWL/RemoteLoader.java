package com.moqi.archive.headfirst.command.remoteWL;

public class RemoteLoader {

    /**
     * [slot 0] com.moqi.headfirst.command.remoteWL.RemoteLoader$$Lambda$3/1639622804    com.moqi.headfirst.command.remoteWL.RemoteLoader$$Lambda$4/124313277
     * [slot 1] com.moqi.headfirst.command.remoteWL.RemoteLoader$$Lambda$5/1225616405    com.moqi.headfirst.command.remoteWL.RemoteLoader$$Lambda$6/2101842856
     * [slot 2] com.moqi.headfirst.command.remoteWL.RemoteLoader$$Lambda$7/1151020327    com.moqi.headfirst.command.remoteWL.RemoteLoader$$Lambda$8/88579647
     * [slot 3] com.moqi.headfirst.command.remoteWL.RemoteLoader$$Lambda$9/654845766    com.moqi.headfirst.command.remoteWL.RemoteLoader$$Lambda$10/1712536284
     * [slot 4] com.moqi.headfirst.command.remoteWL.RemoteLoader$$Lambda$11/2080166188    com.moqi.headfirst.command.remoteWL.RemoteLoader$$Lambda$12/1123225098
     * [slot 5] com.moqi.headfirst.command.remoteWL.RemoteControl$$Lambda$1/1599771323    com.moqi.headfirst.command.remoteWL.RemoteControl$$Lambda$2/353842779
     * [slot 6] com.moqi.headfirst.command.remoteWL.RemoteControl$$Lambda$1/1599771323    com.moqi.headfirst.command.remoteWL.RemoteControl$$Lambda$2/353842779
     *
     * Living Room light is on
     * Living Room light is off
     * Kitchen light is on
     * Kitchen light is off
     * Living Room ceiling fan is on high
     * Living Room ceiling fan is off
     * Living Room stereo is on
     * Living Room stereo is set for CD input
     * Living Room Stereo volume set to 11
     * Living Room stereo is off
     * Main house garage Door is Up
     * Main house garage Door is Down
     */
    public static void main(String[] args) {
        RemoteControl remoteControl = new RemoteControl();

        Light livingRoomLight = new Light("Living Room");
        remoteControl.setCommand(0, livingRoomLight::on, livingRoomLight::off);

        Light kitchenLight = new Light("Kitchen");
		remoteControl.setCommand(1, kitchenLight::on, kitchenLight::off);

        CeilingFan ceilingFan = new CeilingFan("Living Room");
		remoteControl.setCommand(2, ceilingFan::high, ceilingFan::off);

        Stereo stereo = new Stereo("Living Room");
		Command stereoOnWithCD = () -> {
			stereo.on(); stereo.setCD(); stereo.setVolume(11);
		};
		remoteControl.setCommand(3, stereoOnWithCD, stereo::off);

        GarageDoor garageDoor = new GarageDoor("Main house");
		remoteControl.setCommand(4, garageDoor::up, garageDoor::down);

        System.out.println(remoteControl);

        remoteControl.onButtonWasPushed(0);
		remoteControl.offButtonWasPushed(0);
		remoteControl.onButtonWasPushed(1);
		remoteControl.offButtonWasPushed(1);
		remoteControl.onButtonWasPushed(2);
		remoteControl.offButtonWasPushed(2);
		remoteControl.onButtonWasPushed(3);
        remoteControl.offButtonWasPushed(3);
        remoteControl.onButtonWasPushed(4);
        remoteControl.offButtonWasPushed(4);
		remoteControl.onButtonWasPushed(5);
	}
}
