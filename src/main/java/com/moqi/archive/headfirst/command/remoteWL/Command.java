package com.moqi.archive.headfirst.command.remoteWL;

public interface Command {
	void execute();
}
