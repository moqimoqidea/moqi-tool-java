package com.moqi.archive.headfirst.singleton.chocolate;

/**
 * Created by Gavin on 2017/3/10.
 */
public class ChocolateController {

    /**
     * Creating unique instance of Chocolate Boiler
     * Returning instance of Chocolate Boiler
     * Returning instance of Chocolate Boiler
     */
    public static void main(String[] args) {
        ChocolateBoiler boiler = ChocolateBoiler.getInstance();
        boiler.fill();
        boiler.boil();
        boiler.drain();

        ChocolateBoiler.getInstance();
    }

}
