package com.moqi.archive.headfirst.singleton.subclass;

/**
 * Created by Gavin on 2017/3/13.
 */
public class SingletonTestDrive {

    /**
     * com.moqi.headfirst.singleton.subclass.Singleton@1c655221
     * com.moqi.headfirst.singleton.subclass.Singleton@1c655221
     */
    public static void main(String[] args) {
        Singleton foo = CoolerSingleton.getInstance();
        Singleton bar = HotterSingleton.getInstance();
        System.out.println(foo);
        System.out.println(bar);
    }

}
