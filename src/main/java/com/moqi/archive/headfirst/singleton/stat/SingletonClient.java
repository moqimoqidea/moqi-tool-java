package com.moqi.archive.headfirst.singleton.stat;

/**
 * Created by Gavin on 2017/3/13.
 */
public class SingletonClient {

    /**
     * I'm a statically initialized Singleton!
     */
    public static void main(String[] args) {
        Singleton singleton = Singleton.getInstance();
        System.out.println(singleton.getDescription());
    }

}
