package com.moqi.archive.headfirst.singleton.dcl;

/**
 * Created by Gavin on 2017/3/13.
 */
public class SingletonClient {

    /**
     * double check locking success!
     */
    public static void main(String[] args) {
        Singleton.getInstance();
    }

}
