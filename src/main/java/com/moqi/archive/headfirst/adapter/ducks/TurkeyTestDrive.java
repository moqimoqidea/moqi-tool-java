package com.moqi.archive.headfirst.adapter.ducks;

/**
 * Created by Gavin on 2017/3/16.
 */
public class TurkeyTestDrive {

    /**
     * The DuckAdapter says...
     * Quack
     * The DuckAdapter says...
     * Quack
     * The DuckAdapter says...
     * Quack
     * The DuckAdapter says...
     * Quack
     * The DuckAdapter says...
     * Quack
     * I'm flying
     * The DuckAdapter says...
     * Quack
     * The DuckAdapter says...
     * Quack
     * I'm flying
     * The DuckAdapter says...
     * Quack
     * The DuckAdapter says...
     * Quack
     * The DuckAdapter says...
     * Quack
     */
    public static void main(String[] args) {
        MallardDuck duck = new MallardDuck();
        Turkey duckAdapter = new DuckAdapter(duck);

        for (int i = 0; i < 10; i++) {
            System.out.println("The DuckAdapter says...");
            duckAdapter.gobble();
            duckAdapter.fly();
        }
    }

}
