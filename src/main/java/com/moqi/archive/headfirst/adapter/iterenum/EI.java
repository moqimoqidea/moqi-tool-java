package com.moqi.archive.headfirst.adapter.iterenum;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.Vector;

public class EI {

    /**
     * one
     * two
     * three
     *
     * one
     * two
     * three
     */
    public static void main(String[] args) {
        Vector<String> v = new Vector<>(Arrays.asList("one", "two", "three"));
        Enumeration<String> enumeration = v.elements();

        while (enumeration.hasMoreElements()) {
            System.out.println(enumeration.nextElement());
        }

        System.out.println();

        for (String s : v) {
            System.out.println(s);
        }
    }

}
