package com.moqi.archive.headfirst.proxy.javaproxy;

/**
 * Created by Gavin on 2017/3/24.
 */
public interface PersonBean {
    String getName();

    void setName(String name);

    String getGender();

    void setGender(String gender);

    String getInterests();

    void setInterests(String interests);

    int getHotOrNotRating();

    void setHotOrNotRating(int rating);
}
