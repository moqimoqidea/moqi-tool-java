package com.moqi.archive.headfirst.proxy.gumball;

import java.rmi.Naming;

/**
 * Created by Gavin on 2017/3/23.
 */
public class GumballMachineTestDrive {

    public static void main(String[] args) {
        String[] innerArgs = new String[]{"localhost", "300"};
        GumballMachineRemote gumballMachine;
        int count;

        try {
            count = Integer.parseInt(innerArgs[1]);

            gumballMachine = new GumballMachine(innerArgs[0], count);
            Naming.rebind("//" + innerArgs[0] + "/gumballmachine", gumballMachine);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
