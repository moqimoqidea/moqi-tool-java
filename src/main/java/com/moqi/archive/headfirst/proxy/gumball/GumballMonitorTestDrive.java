package com.moqi.archive.headfirst.proxy.gumball;

import java.rmi.Naming;

/**
 * Created by Gavin on 2017/3/23.
 */
public class GumballMonitorTestDrive {

    public static void main(String[] args) {
        String[] innerArgs = new String[]{"localhost"};
        String[] location = {"rmi://santafe.mightygumball.com/gumballmachine",
                "rmi://boulder.mightygumball.com/gumballmachine",
                "rmi://seattle.mightygumball.com/gumballmachine"};

        location = new String[1];
        location[0] = "rmi://" + innerArgs[0] + "/gumballmachine";

        GumballMonitor[] monitor = new GumballMonitor[location.length];

        for (int i = 0; i < location.length; i++) {
            try {
                GumballMachineRemote machine = (GumballMachineRemote) Naming.lookup(location[i]);
                monitor[i] = new GumballMonitor(machine);
                System.out.println(monitor[i]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        for (GumballMonitor gumballMonitor : monitor) {
            gumballMonitor.report();
        }
    }

}
