package com.moqi.archive.headfirst.iterator.dinermergeri;

import java.util.Iterator;

public interface Menu {
	Iterator<MenuItem> createIterator();
}
