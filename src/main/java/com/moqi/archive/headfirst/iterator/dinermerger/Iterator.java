package com.moqi.archive.headfirst.iterator.dinermerger;

public interface Iterator {
	boolean hasNext();
	MenuItem next();
}
