package com.moqi.archive.headfirst.iterator.dinermerger;

public interface Menu {
	public Iterator createIterator();
}
