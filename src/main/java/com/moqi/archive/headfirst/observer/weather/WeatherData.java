package com.moqi.archive.headfirst.observer.weather;

import java.util.ArrayList;

/**
 * Created by Gavin on 2017/2/13.
 */
public class WeatherData implements Subject {
    private final ArrayList<MyObserver> myObservers;
    private float temperature;
    private float humidity;
    private float pressure;

    public WeatherData() {
        myObservers = new ArrayList<>();
    }

    @Override
    public void registerObserver(MyObserver o) {
        myObservers.add(o);
    }

    @Override
    public void removeObserver(MyObserver o) {
        int i = myObservers.indexOf(o);
        if (i >= 0) {
            myObservers.remove(i);
        }
    }

    @Override
    public void notifyObservers() {
        for (MyObserver myObserver : myObservers) {
            myObserver.update(temperature, humidity, pressure);
        }
    }

    public void measurementsChanged(){
        notifyObservers();
    }

    public void setMeasurements(float temperature, float humidity, float pressure){
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        measurementsChanged();
        System.out.println();
    }

    public float getTemperature() {
        return temperature;
    }

    public float getHumidity() {
        return humidity;
    }

    public float getPressure() {
        return pressure;
    }
}
