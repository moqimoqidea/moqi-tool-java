package com.moqi.archive.headfirst.observer.weather;

/**
 * Created by Gavin on 2017/2/13.
 */
public interface Subject {
    void registerObserver(MyObserver o);

    void removeObserver(MyObserver o);

    void notifyObservers();
}
