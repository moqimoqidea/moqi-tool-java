package com.moqi.archive.headfirst.observer.weatherobservable;

/**
 * Created by Gavin on 2017/2/13.
 */
public interface DisplayElement {

    void display();

}
