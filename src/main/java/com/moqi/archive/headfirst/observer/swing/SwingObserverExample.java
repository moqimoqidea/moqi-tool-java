package com.moqi.archive.headfirst.observer.swing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author moqi
 * On 2021/9/12 16:24
 */
public class SwingObserverExample {

    /**
     * Come on, do it!
     * Don't do it, you might regret it!
     */
    public static void main(String[] args) {
        new SwingObserverExample().go();
    }

    private void go() {
        JFrame frame = new JFrame();

        JButton button = new JButton("Should I do it?");
        button.addActionListener(new AngleListener());
        button.addActionListener(new DevilListener());
        frame.getContentPane().add(BorderLayout.CENTER, button);

        // How to Make Frames (Main Windows)
        // https://docs.oracle.com/javase/tutorial/uiswing/components/frame.html
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        // How to set JFrame to appear centered, regardless of monitor resolution?
        // https://stackoverflow.com/a/2442614
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    private static class AngleListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println("Don't do it, you might regret it!");
        }
    }

    private static class DevilListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println("Come on, do it!");
        }
    }

}
