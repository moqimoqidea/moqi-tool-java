package com.moqi.archive.headfirst.observer.weather;

/**
 * Created by Gavin on 2017/2/13.
 */
public interface MyObserver {

    void update(float temp, float humidity, float pressure);

}
