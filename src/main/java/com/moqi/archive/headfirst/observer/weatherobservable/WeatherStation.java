package com.moqi.archive.headfirst.observer.weatherobservable;

/**
 * Created by Gavin on 2017/2/13.
 */
public class WeatherStation {

    /**
     * Forecast: Improving weather on the way!
     * Avg/Max/Min temperature = 80.0/80.0/80.0
     * Current conditions: 80.0F degrees and 65.0% humidity
     *
     * Forecast: Watch out for cooler, rainy weather
     * Avg/Max/Min temperature = 81.0/82.0/80.0
     * Current conditions: 82.0F degrees and 70.0% humidity
     *
     * Forecast: More of the same
     * Avg/Max/Min temperature = 80.0/82.0/78.0
     * Current conditions: 78.0F degrees and 90.0% humidity
     */
    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();

        new CurrentConditionsDisplay(weatherData);
        new StatisticsDisplay(weatherData);
        new ForecastDisplay(weatherData);

        weatherData.setMeasurements(80, 65, 30.4f);
        weatherData.setMeasurements(82, 70, 29.2f);
        weatherData.setMeasurements(78, 90, 29.2f);
    }

}
