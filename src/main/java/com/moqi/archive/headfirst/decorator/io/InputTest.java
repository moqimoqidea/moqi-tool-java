package com.moqi.archive.headfirst.decorator.io;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by Gavin on 2017/3/8.
 */
public class InputTest {

    private static final String TEXT_TXT = "text.txt";
    private static final ClassLoader CLASS_LOADER = InputTest.class.getClassLoader();

    /**
     * before decorator:
     * text = I know the Decorator Pattern therefore I RULE!
     *
     * after decorator:
     * text = i know the decorator pattern therefore i rule!
     */
    public static void main(String[] args) throws IOException {

        String text = new BufferedReader(new InputStreamReader(
                Objects.requireNonNull(CLASS_LOADER.getResourceAsStream(TEXT_TXT)), StandardCharsets.UTF_8))
                .lines().collect(Collectors.joining("\n"));
        System.out.println("before decorator:");
        System.out.println("text = " + text);
        System.out.println("\nafter decorator:");

        InputStream in = new LowerCaseInputStream(new BufferedInputStream(
                Objects.requireNonNull(InputTest.class.getClassLoader().getResourceAsStream(TEXT_TXT))));
        System.out.print("text = ");

        int c;
        while ((c = in.read()) >= 0) {
            System.out.print((char) c);
        }

        in.close();
    }

}
