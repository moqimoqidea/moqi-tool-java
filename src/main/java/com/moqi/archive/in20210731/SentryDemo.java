package com.moqi.archive.in20210731;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import io.sentry.Sentry;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;

/**
 * 测试 Sentry
 *
 * @author moqi
 * On 2021/7/31 10:53
 */
@Slf4j
public class SentryDemo {

    private static String DSN = null;

    static {
        try {
            File file = new File("/Users/moqi/Dropbox/Mackup/sentry_dns/java_project_dsn.txt");
            DSN = Files.asCharSource(file, Charsets.UTF_8).readFirstLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        if (StringUtils.isBlank(DSN)) {
            throw new Exception("Dsn is blank!");
        }
        log.info("DSN:{}", DSN);

        // test host sentry: "http://4d0be320713a4bb8a79a0e2490656c38@localhost:9000/2"
        Sentry.init(options -> options.setDsn(DSN));

        try {
            throw new Exception("This is a test on java.");
        } catch (Exception e) {
            Sentry.captureException(e);
        }
    }

}
