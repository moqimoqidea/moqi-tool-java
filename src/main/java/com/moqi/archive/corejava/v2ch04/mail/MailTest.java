package com.moqi.archive.corejava.v2ch04.mail;

import com.moqi.in20210919.ResourcesUtil;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import java.io.Console;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import java.util.Properties;

/**
 * This program shows how to use JavaMail to send mail messages.
 *
 * @author Cay Horstmann
 * @version 1.00 2012-06-04
 */
public class MailTest {

    private static final String MAIL_PROPERTIES = "mail/mail.properties";
    private static final String MAIL_MESSAGE = "mail/message.txt";

    public static void main(String[] args) throws IOException, MessagingException, URISyntaxException {

        Properties props = new Properties();
        try (InputStream in = Files.newInputStream(ResourcesUtil.getPathByName(MAIL_PROPERTIES))) {
            props.load(in);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        List<String> lines = Files.readAllLines(ResourcesUtil.getPathByName(MAIL_MESSAGE), StandardCharsets.UTF_8);

        String from = lines.get(0);
        String to = lines.get(1);
        String subject = lines.get(2);

        StringBuilder builder = new StringBuilder();
        for (int i = 3; i < lines.size(); i++) {
            builder.append(lines.get(i));
            builder.append("\n");
        }

        Console console = System.console();
        String password = new String(console.readPassword("Password: "));

        Session mailSession = Session.getDefaultInstance(props);
        // mailSession.setDebug(true);
        MimeMessage message = new MimeMessage(mailSession);
        message.setFrom(new InternetAddress(from));
        message.addRecipient(RecipientType.TO, new InternetAddress(to));
        message.setSubject(subject);
        message.setText(builder.toString());

        try (Transport tr = mailSession.getTransport()) {
            tr.connect(null, password);
            tr.sendMessage(message, message.getAllRecipients());
        }

    }

}
