package com.moqi.archive.corejava.v2ch04.socket;

import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

/**
 * This program makes a socket connection to the atomic clock in Boulder, Colorado, and prints
 * the time that the server sends.
 *
 * @author Cay Horstmann
 * @version 1.21 2016-04-27
 */
public class SocketTest {

    /**
     * 59476 21-09-19 14:08:27 50 0 0 534.6 UTC(NIST) *
     */
    public static void main(String[] args) throws IOException {
        try (Socket s = new Socket("time-a.nist.gov", 13);
             Scanner in = new Scanner(s.getInputStream(), "UTF-8")) {
            while (in.hasNextLine()) {
                String line = in.nextLine();
                System.out.println(line);
            }
        }
    }

}
