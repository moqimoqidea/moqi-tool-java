package com.moqi.archive.in20210915;

import com.googlecode.aviator.AviatorEvaluator;
import com.googlecode.aviator.Expression;

import java.io.IOException;

/**
 * @author moqi
 * @date 9/15/21 16:35
 */
public class RunScriptExample {

    public static void main(final String[] args) throws Exception {
        m1();
        m2();
        m3();
        System.out.println("------------------------");

        nilVariableCalc();
    }

    private static void m3() {
        String expression = "a-(b-c) > 100";
        Expression compiledExp = AviatorEvaluator.compile(expression);
        // Execute with injected variables.
        Boolean result =
                (Boolean) compiledExp.execute(compiledExp.newEnv("a", 100.3, "b", 45, "c", -199.100));
        System.out.println("Execute with injected variables. result: " + result);
    }

    private static void m2() {
        // Compile a script
        Expression script = AviatorEvaluator.getInstance().compile("println('Hello, AviatorScript!');");
        script.execute();
    }

    private static void m1() throws IOException {
        // Compile the script into an Expression instance. With cache
        Expression exp = AviatorEvaluator.getInstance().compileScript("examples/hello.av", true);
        // Run the expression.
        exp.execute();
    }

    private static void nilVariableCalc() {
        String expression = "name != nil ? ('hello, ' + name):'who are u?'";
        Expression compiledExp = AviatorEvaluator.compile(expression);
        // we don't inject variable name
        String s = (String) compiledExp.execute();
        System.out.println("before s = " + s);

        // inject name
        s = (String) compiledExp.execute(compiledExp.newEnv("name", "dennis"));
        System.out.println("after s = " + s);
    }

}
