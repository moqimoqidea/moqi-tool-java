package com.moqi.archive.in20210816;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

/**
 * 创建 client
 *
 * @author moqi
 * @date 8/16/21 16:52
 */
public class InitDemo {

    public static RestHighLevelClient getClient() {
        return new RestHighLevelClient(RestClient.builder(new HttpHost(
                        "localhost", 9200, "http")));
    }

}
