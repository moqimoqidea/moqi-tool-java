package com.moqi.archive.in20210816;

import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.GetIndexRequest;

import java.io.IOException;

/**
 * 创建索引
 *
 * @author moqi
 * @date 9/7/21 16:45
 */
@Slf4j
public class GetIndexDemo {

    public static void main(String[] args) {
        try (RestHighLevelClient client = InitDemo.getClient()) {
            GetIndexRequest getIndexRequest = new GetIndexRequest("mess", "mess123");
            boolean exists = client.indices().exists(getIndexRequest, RequestOptions.DEFAULT);
            System.out.println("exists = " + exists);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
