package com.moqi.archive.before20200530.rpc;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.InetSocketAddress;

/**
 * @author moqi
 * On 3/8/21 15:55
 */
@Slf4j
public class RPCTest {

    public static void main(String[] args) throws IOException {
        new Thread(() -> {
            try {
                Server serviceServer = new ServiceCenter(8088);
                serviceServer.register(HelloService.class, HelloServiceImpl.class);
                serviceServer.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();

        HelloService service = RPCClient.getRemoteProxyObj(HelloService.class, new InetSocketAddress("localhost", 8088));
        log.info(service.sayHi("test"));
    }

}
