package com.moqi.archive.before20200530.heartbeat;

import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * @author moqi
 * On 3/8/21 15:45
 */
@Slf4j
public class HeartbeatHandlerImpl implements HeartbeatHandler {

    @Override
    public Cmder sendHeartBeat(HeartbeatEntity info) {
        HeartbeatLinstener linstener = HeartbeatLinstener.getInstance();

        // 添加节点
        if (!linstener.checkNodeValid(info.getNodeID())) {
            linstener.registerNode(info.getNodeID(), info);
        }

        // 其他操作
        Cmder cmder = new Cmder();
        cmder.setNodeID(info.getNodeID());
        // ...

        log.info("current all the nodes: ");
        Map<String, Object> nodes = linstener.getNodes();
        for (Map.Entry e : nodes.entrySet()) {
            log.info(e.getKey() + " : " + e.getValue());
        }
        log.info("handle a heartbeat");
        return cmder;
    }

}
