package com.moqi.archive.in20210826;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 测试 ScheduledExecutorService
 * https://segmentfault.com/a/1190000008038848
 */
@Slf4j
public class ScheduledExecutorServiceTest {

    private static final ScheduledExecutorService SCHEDULED_SERVICE = Executors.newSingleThreadScheduledExecutor();
    private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(
            10, new BasicThreadFactory.Builder().namingPattern("executor-pool-%d").build());

    public static void main(String[] args) throws Exception {
        log.info("开始\n");

        // 延时 1 秒后，按 3 秒的周期执行任务
        // EXECUTOR_SERVICE 接手线程运行，保证定时任务绝对触发时间
        SCHEDULED_SERVICE.scheduleAtFixedRate(() -> EXECUTOR_SERVICE.execute(new TimerTask(5000)),
                1000, 3000, TimeUnit.MILLISECONDS);
    }

    private static class TimerTask implements Runnable {

        private final int sleepTime;

        public TimerTask(int sleepTime) {
            this.sleepTime = sleepTime;
        }

        @Override
        public void run() {
            String threadName = Thread.currentThread().getName();
            log.info("threadName:{}, 任务开始.", threadName);

            try {
                log.info("threadName:{}, 模拟任务运行...", threadName);
                Thread.sleep(sleepTime);
            } catch (InterruptedException ex) {
                log.warn(ex.getMessage());
            }

            log.info("threadName:{}, 任务结束.\n", threadName);
        }

    }
}

