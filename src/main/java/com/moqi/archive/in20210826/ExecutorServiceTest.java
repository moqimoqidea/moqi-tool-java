package com.moqi.archive.in20210826;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.*;

/**
 * 测试 ExecutorService 在并发情况下的表现
 */
@Slf4j
public class ExecutorServiceTest {

    private static final ScheduledExecutorService SCHEDULED_SERVICE = Executors.newSingleThreadScheduledExecutor();
    private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(
            10, new BasicThreadFactory.Builder().namingPattern("executor-pool-%d").build());
    private static final BlockingQueue<String> QUEUE = new ArrayBlockingQueue<>(100);
    private static final Random random = new Random();
    private static final int BITCH_SIZE = 10;
    private static long turnCount = 0;

    /**
     * 测试竞争条件下 QUEUE 的取用问题
     */
    public static void main(String[] args) throws Exception {
        log.info("Start");

        new Thread(() -> {
            while (true) {
                try {
                    //noinspection BusyWait
                    Thread.sleep(random.nextInt(30));
                    QUEUE.add("data");
                    log.debug("-----------  add data, queue size:" + QUEUE.size());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        // 延时 1 秒后，按 3 秒的周期执行任务
        SCHEDULED_SERVICE.scheduleAtFixedRate(() -> {
            if (QUEUE.isEmpty()) {
                log.info("queue is empty, skip");
                return;
            }

            while (!QUEUE.isEmpty()) {
                log.info("in loop, now queue size: " + QUEUE.size());
                int readSize = QUEUE.drainTo(new ArrayList<>(BITCH_SIZE), BITCH_SIZE);
                if (readSize > 0) {
                    EXECUTOR_SERVICE.execute(() -> log.info("new thread run sub task success."));
                    // 如果批量大小太小，跳出这轮定时任务
                    if (readSize < BITCH_SIZE) {
                        log.info("read size less than bitch size, skip");
                        break;
                    }
                }
            }

            log.info("this turn count is:{}", turnCount);
            turnCount += 1;
        }, 1, 1, TimeUnit.SECONDS);
    }

}

