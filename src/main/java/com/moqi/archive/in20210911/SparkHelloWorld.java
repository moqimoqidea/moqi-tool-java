package com.moqi.archive.in20210911;

import static spark.Spark.get;

/**
 * https://blogs.oracle.com/javamagazine/java-spark-web-microservices
 *
 * https://github.com/perwendel/spark
 *
 * @author moqi
 * On 2021/9/11 14:36
 */
public class SparkHelloWorld {

    public static void main(String[] arg) {
        get("/hello", (request, response) -> "Hello World!");
    }

}
