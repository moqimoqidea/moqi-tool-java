package com.moqi.in20220412.mdc;


import com.moqi.in20220412.mdc.pool.MdcAwareThreadPoolExecutor;
import com.moqi.in20220412.mdc.slf4j.Slf4jRunnable;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor.AbortPolicy;

import static java.util.concurrent.TimeUnit.MINUTES;

public class TransferDemo {

    public static final ExecutorService EXECUTOR = new MdcAwareThreadPoolExecutor(
            3, 3, 0, MINUTES,
            new LinkedBlockingQueue<>(), Thread::new, new AbortPolicy());

    public static void main(String[] args) {
        TransactionFactory transactionFactory = new TransactionFactory();

        for (int i = 0; i < 10; i++) {
            EXECUTOR.submit(new Slf4jRunnable(transactionFactory.newInstance()));
        }

        EXECUTOR.shutdown();
    }

}
