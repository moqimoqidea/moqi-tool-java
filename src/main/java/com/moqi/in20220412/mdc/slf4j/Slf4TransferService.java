package com.moqi.in20220412.mdc.slf4j;

import com.moqi.in20220412.mdc.TransferService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
final class Slf4TransferService extends TransferService {

    @Override
    protected void beforeTransfer(long amount) {
        log.info("Preparing to transfer {}$.", amount);
    }

    @Override
    protected void afterTransfer(long amount, boolean outcome) {
        log.info("Has transfer of {}$ completed successfully ? {}.", amount, outcome);
    }

}