package com.moqi.in20220412.mdc;

import java.util.Random;

public class TransactionFactory {

    private static final String[] NAMES = {"John", "Susan", "Marc", "Samantha"};
    private static final Random RANDOM = new Random();
    private long nextId = 1;

    public Transfer newInstance() {
        String transactionId = String.valueOf(nextId++);
        String owner = NAMES[RANDOM.nextInt(NAMES.length)];
        long amount = RANDOM.nextLong(88888L);
        return new Transfer(transactionId, owner, amount);
    }

}
