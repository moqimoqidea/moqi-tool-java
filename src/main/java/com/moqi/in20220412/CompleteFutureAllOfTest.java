package com.moqi.in20220412;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.*;
import java.util.function.Function;

/**
 * @author moqi
 * @date 4/12/22 23:24
 */
@Slf4j
public class CompleteFutureAllOfTest {

    private static final ExecutorService executorService = new ThreadPoolExecutor(
            100,
            100,
            10L,
            TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(3000),
            new ThreadFactoryBuilder().setNameFormat("pool-%d").build(),
            new ThreadPoolExecutor.CallerRunsPolicy()
    );

    public static void main(String[] args) {
        Function<String, CompletableFuture<Void>> directionDoSomething = string -> CompletableFuture.supplyAsync(() -> {
            doSomething(string);
            return null;
        }, executorService);
        Function<String, CompletableFuture<Void>> byExecuteDoSomething = string -> CompletableFuture.supplyAsync(() -> {
            tryExecute(() -> doSomething(string));
            return null;
        }, executorService);

        try {
            // 并发执行
            CompletableFuture.allOf(directionDoSomething.apply("direction"), byExecuteDoSomething.apply("byExecute"))
                    .get(10, TimeUnit.SECONDS);
        } catch (ExecutionException | TimeoutException e) {
            log.warn("error.", e);
        } catch (InterruptedException e) {
            log.warn("interrupted exception.", e);
            Thread.currentThread().interrupt();
        }

        log.info("main end.");
        executorService.shutdown();
    }

    public static void doSomething(String string) {
        log.info("do something start, string: {}", string);
        try {
            Thread.sleep(3000L);
        } catch (InterruptedException ignore) {
            Thread.currentThread().interrupt();
        }
        log.info("do something end, string: {}", string);
    }

    public static void tryExecute(Runnable execute) {
        try {
            Thread.sleep(8888L);
            execute.run();
        } catch (Exception e) {
            log.error("execute update error. ");
        }
    }

}
