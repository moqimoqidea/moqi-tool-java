package com.moqi.in20220309;

import lombok.extern.slf4j.Slf4j;
import org.unix4j.Unix4j;
import org.unix4j.builder.Unix4jCommandBuilder;
import org.unix4j.line.Line;

import java.io.File;
import java.util.List;

/**
 * https://www.baeldung.com/grep-in-java
 *
 * @author moqi
 * @date 2022/3/9 12:14
 */
@Slf4j
public class GrepTest {

    public static void main(String[] args) {
        Unix4jCommandBuilder grep = Unix4j.grep("function", new File("/Users/moqi/Code/weibo-video-all/wenbo17/logs/access/temp.log"));
        log.info("grep:{}", grep);

        List<Line> lines = grep.toLineList();
        log.info("lines:{}", lines);

        List<String> stringList = Unix4j.ls("/Users/moqi/Code/weibo-video-all/wenbo17/logs/access").toStringList();
        log.info("stringList:{}", stringList);
    }

}
