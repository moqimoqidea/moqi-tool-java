package com.moqi.in20220302;

import java.util.concurrent.*;

/**
 * 线程池异常处理详解，一文搞懂！
 *
 * https://www.cnblogs.com/ncy1/articles/11629933.html
 *
 * 进行测试
 *
 * @author moqi
 * @date 3/2/22 12:14
 */
@SuppressWarnings("All")
public class ExecutorExceptionTest {

    public static void main(String[] args) {

        // submit01();

        // execute01();

        // submit02();

        // execute02();

        // uncaughtException01();

        // notUucaughtException01();

        // executeWithHandler01();

        // executeWithHandler02();

        // submitWithHandler01();

        // submitFuture01();

        // submitFuture02();

        // rewriteAfterExecute01();

        finallyRewriteAfterExecute();

    }

    /**
     * 第一个线程内部发生异常之后，没有任何异常信息出现，第二个任务正常执行。
     */
    private static void submit01() {
        ExecutorService executorService = Executors.newFixedThreadPool(1);

        executorService.submit(() -> {
            int i = 1 / 0;
        });

        executorService.submit(() -> {
            System.out.println("当线程池抛出异常后继续新的任务");
        });

        executorService.shutdown();
    }

    /**
     * 第一个任务出现了异常栈信息，第二个任务正常执行。
     */
    private static void execute01() {
        ExecutorService executorService = Executors.newFixedThreadPool(1);

        executorService.execute(() -> {
            int i = 1 / 0;
        });

        executorService.execute(() -> {
            System.out.println("当线程池抛出异常后继续新的任务");
        });

        executorService.shutdown();
    }

    /**
     * 将整个任务try-catch起来，捕获里面的异常，这种方式是最简单有效的方式。
     */
    private static void submit02() {
        ExecutorService executorService = Executors.newFixedThreadPool(1);

        executorService.submit(() -> {
            try {
                int i = 1 / 0;
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        });

        executorService.submit(() -> {
            System.out.println("当线程池抛出异常后继续新的任务");
        });

        executorService.shutdown();
    }

    /**
     * 将整个任务try-catch起来，捕获里面的异常，这种方式是最简单有效的方式。
     */
    private static void execute02() {
        ExecutorService executorService = Executors.newFixedThreadPool(1);

        executorService.execute(() -> {
            try {
                int i = 1 / 0;
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        });

        executorService.execute(() -> {
            System.out.println("当线程池抛出异常后继续新的任务");
        });

        executorService.shutdown();
    }


    /**
     * uncaughtException01
     */
    private static void uncaughtException01() {
        // 创建线程对象 内部会抛出异常
        Thread thread = new Thread(() -> {
            int i = 1 / 0;
        });

        // 设置该对象的默认异常处理器
        thread.setDefaultUncaughtExceptionHandler((Thread t, Throwable e) -> {
            System.err.println("exceptionHandler: " + e.getMessage());
        });

        // 启动线程
        thread.start();
    }

    /**
     * notUncaughtException01
     */
    private static void notUucaughtException01() {
        // 创建线程对象 内部会抛出异常
        Thread thread = new Thread(() -> {
            int i = 1 / 0;
        });

        // 启动线程
        thread.start();
    }

    /**
     * executeWithHandler01
     */
    private static void executeWithHandler01() {
        ExecutorService executorService = Executors.newFixedThreadPool(1);

        Thread thread = new Thread(() -> {
            int i = 1 / 0;
        });


        thread.setDefaultUncaughtExceptionHandler((Thread t, Throwable e) -> {
            System.err.println("exceptionHandler: " + e.getMessage());
        });

        executorService.execute(thread);
        executorService.shutdown();
    }

    /**
     * executeWithHandler02
     */
    private static void executeWithHandler02() {
        // 1.实现一个自己的线程池工厂
        ThreadFactory factory = (Runnable r) -> {
            // 创建一个线程
            Thread t = new Thread(r);

            // 给创建的线程设置UncaughtExceptionHandler对象 里面实现异常的默认逻辑
            t.setDefaultUncaughtExceptionHandler((Thread thread1, Throwable e) -> {
                System.err.println("线程工厂设置的 exceptionHandler: " + e.getMessage());
            });

            return t;
        };

        // 2.创建一个自己定义的线程池，使用自己定义的线程工厂
        ExecutorService service = new ThreadPoolExecutor(1, 1, 0, TimeUnit.MILLISECONDS, new LinkedBlockingQueue(10), factory);

        // 3.提交任务
        service.execute(() -> {
            int i = 1 / 0;
        });

        service.shutdown();
    }

    /**
     * submitWithHandler01
     *
     * 结果是什么也没有输出，异常信息消失了。
     * 说明UncaughtExceptionHandler并没有被调用。
     */
    private static void submitWithHandler01() {
        ExecutorService executorService = Executors.newFixedThreadPool(1);

        Thread thread = new Thread(() -> {
            int i = 1 / 0;
        });


        thread.setDefaultUncaughtExceptionHandler((Thread t, Throwable e) -> {
            System.err.println("exceptionHandler: " + e.getMessage());
        });

        executorService.submit(thread);
        executorService.shutdown();
    }

    /**
     * submitFuture01
     */
    private static void submitFuture01() {
        ExecutorService executorService = Executors.newFixedThreadPool(1);

        try {
            // 获取线程池里面的结果
            Integer a = (Integer) executorService.submit((Callable) () -> {
                return 1;
            }).get();
            System.out.println("future中获取结果: " + a);
        } catch (Exception e) {
            // 获取线程池里面的异常
            System.err.println("future中获取异常: " + e.getMessage());
        }

        executorService.shutdown();
    }

    /**
     * submitFuture02
     */
    private static void submitFuture02() {
        ExecutorService executorService = Executors.newFixedThreadPool(1);

        try {
            // 获取线程池里面的结果
            Integer a = (Integer) executorService.submit((Callable) () -> {
                return 1 / 0;
            }).get();
            System.out.println("future中获取结果: " + a);
        } catch (Exception e) {
            // 获取线程池里面的异常
            System.err.println("future中获取异常: " + e.getMessage());
        }

        executorService.shutdown();
    }


    /**
     * rewriteAfterExecute01
     */
    private static void rewriteAfterExecute01() {
        //1.创建一个自己定义的线程池,重写afterExecute方法
        ExecutorService service = new ThreadPoolExecutor(1, 1, 0, TimeUnit.MILLISECONDS, new LinkedBlockingQueue(10)) {
            @Override
            protected void afterExecute(Runnable r, Throwable t) {
                super.afterExecute(r, t);
                System.err.println("afterExecute里面获取到异常信息: " + t.getMessage());
            }
        };

        //2.提交任务
        service.execute(() -> {
            int i = 1 / 0;
        });

        service.shutdown();
    }

    /**
     * finallyRewriteAfterExecute
     */
    private static void finallyRewriteAfterExecute() {
        // 定义线程池
        ExecutorService service = new ThreadPoolExecutor(1, 1, 0, TimeUnit.MILLISECONDS, new LinkedBlockingQueue(10)) {

            // 重写afterExecute方法
            @Override
            protected void afterExecute(Runnable r, Throwable t) {
                super.afterExecute(r, t);

                // 这个是excute提交的时候
                if (t != null) {
                    System.err.println("afterExecute里面获取到异常信息: " + t.getMessage());
                }

                // 如果r的实际类型是FutureTask 那么是submit提交的，所以可以在里面get到异常
                if (r instanceof FutureTask) {
                    try {
                        Future<?> future = (Future<?>) r;
                        future.get();
                    } catch (Exception e) {
                        System.err.println("future里面取执行异常: " + e.getMessage());
                    }
                }
            }
        };

        // 2.提交任务
        service.submit(() -> {
            int i = 1 / 0;
        });

        service.execute(() -> {
            int i = 1 / 0;
        });

        service.shutdown();
    }


}
