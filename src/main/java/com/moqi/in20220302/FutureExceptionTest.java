package com.moqi.in20220302;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/**
 * @author moqi
 * @date 3/2/22 16:22
 */
public class FutureExceptionTest {

    public static void main(String[] args) {
        System.out.println("start");

        try {
            divide(1, 0);
        } finally {
            System.out.println("finally block");
        }

        System.out.println("end");
    }

    /**
     * 由于缺少 task get 调用，因此 callable 异常会被吞并
     */
    private static void divide(int a, int b) {
        Callable<Integer> callable = () -> a / b;

        FutureTask<Integer> task = new FutureTask<>(callable);
        task.run();

        /*try {
            task.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }*/

        /*try {
            task.get(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }*/

    }

}
