package com.moqi.in20220510;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.bag.HashBag;

/**
 * @author moqi
 * @date 5/10/22 22:13
 */
@Slf4j
public class HashBagTest {

    public static void main(String[] args) {
        HashBag<String> bag = new HashBag<>();

        bag.add("a");
        bag.add("b", 2);
        bag.add("c", 3);

        log.info("bag:{}", bag);
    }

}
