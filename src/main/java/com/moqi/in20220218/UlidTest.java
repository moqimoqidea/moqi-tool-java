package com.moqi.in20220218;

import de.huxhorn.sulky.ulid.ULID;
import lombok.extern.slf4j.Slf4j;

/**
 * Test Ulid: https://github.com/huxi/sulky/tree/master/sulky-ulid
 *
 * @author moqi
 * @date 2022/2/18 10:59
 */
@Slf4j
public class UlidTest {

    public static void main(String[] args) {

        ULID ulid = new ULID();

        // generate a ULID string
        // this is likely what you are looking for
        String ulidString = ulid.nextULID();
        log.info("ulidString:{}", ulidString);

        // generate a ULID Value instance
        ULID.Value ulidValue = ulid.nextValue();

        // generate the byte[] for a ULID.Value
        byte[] data = ulidValue.toBytes();

        // generate a ULID.Value from given bytes using the static fromBytes method
        ULID.Value ulidValueFromBytes = ULID.fromBytes(data);

        // generate a ULID.Value from given String using the static parseULID method
        ULID.Value ulidValueFromString = ULID.parseULID(ulidString);

        // generate a ULID string from ULID.Value
        String ulidStringFromValue = ulidValue.toString();
        log.info("ulidStringFromValue:{}", ulidStringFromValue);
    }

}
