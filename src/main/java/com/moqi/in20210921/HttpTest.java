package com.moqi.in20210921;

import org.apache.hc.client5.http.async.methods.AbstractCharResponseConsumer;
import org.apache.hc.client5.http.async.methods.SimpleHttpRequest;
import org.apache.hc.client5.http.async.methods.SimpleHttpResponse;
import org.apache.hc.client5.http.async.methods.SimpleRequestBuilder;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.entity.UrlEncodedFormEntity;
import org.apache.hc.client5.http.fluent.Form;
import org.apache.hc.client5.http.fluent.Request;
import org.apache.hc.client5.http.impl.async.CloseableHttpAsyncClient;
import org.apache.hc.client5.http.impl.async.HttpAsyncClients;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.concurrent.FutureCallback;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.HttpResponse;
import org.apache.hc.core5.http.NameValuePair;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.http.message.BasicNameValuePair;
import org.apache.hc.core5.http.nio.AsyncRequestProducer;
import org.apache.hc.core5.http.nio.support.AsyncRequestBuilder;

import java.io.IOException;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * @author moqi
 * On 2021/9/20 16:24
 */
public class HttpTest {

    private static final String GET_URL = "http://httpbin.org/get";
    private static final String POST_URL = "http://httpbin.org/post";

    public static void main(String[] args) throws IOException {

        System.out.println("------- native way start -------");
        nativeWay();
        System.out.println("------- native way end -------");

        System.out.println("------- fluent api start -------");
        fluentApi();
        System.out.println("------- fluent api end -------");

        System.out.println("------- async api start -------");
        asyncApi();
        System.out.println("------- async api end -------");
    }

    private static void nativeWay() throws IOException {

        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpGet httpGet = new HttpGet(GET_URL);

            try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
                System.out.println(response.getCode() + " " + response.getReasonPhrase());
                HttpEntity entity = response.getEntity();
                EntityUtils.consume(entity);
            }

            HttpPost httpPost = new HttpPost(POST_URL);
            List<NameValuePair> pairList = new ArrayList<>();
            pairList.add(new BasicNameValuePair("username", "vip"));
            pairList.add(new BasicNameValuePair("password", "secret"));
            httpPost.setEntity(new UrlEncodedFormEntity(pairList));

            try (CloseableHttpResponse response = httpclient.execute(httpPost)) {
                System.out.println(response.getCode() + " " + response.getReasonPhrase());
                HttpEntity entity = response.getEntity();
                EntityUtils.consume(entity);
            }
        }

    }

    private static void fluentApi() throws IOException {
        HttpResponse responseByGet = Request.get(GET_URL).execute().returnResponse();
        System.out.println(responseByGet.getCode() + " " + responseByGet.getReasonPhrase());

        HttpResponse responseByPost = Request.post(POST_URL).bodyForm(Form.form()
                        .add("username", "vip")
                        .add("password", "secret")
                        .build())
                .execute().returnResponse();
        System.out.println(responseByPost.getCode() + " " + responseByPost.getReasonPhrase());
    }

    private static void asyncApi() {

        try (CloseableHttpAsyncClient httpclient = HttpAsyncClients.createDefault()) {
            // Start the client
            httpclient.start();

            // Execute request
            SimpleHttpRequest request1 = SimpleRequestBuilder.get(GET_URL).build();
            Future<SimpleHttpResponse> future = httpclient.execute(request1, null);
            // and wait until response is received
            SimpleHttpResponse response1 = future.get();
            System.out.println(request1.getRequestUri() + "->" + response1.getCode());

            // One most likely would want to use a callback for operation result
            CountDownLatch latch1 = new CountDownLatch(1);
            SimpleHttpRequest request2 = SimpleRequestBuilder.get(GET_URL).build();
            httpclient.execute(request2, new FutureCallback<SimpleHttpResponse>() {

                @Override
                public void completed(SimpleHttpResponse response2) {
                    latch1.countDown();
                    System.out.println(request2.getRequestUri() + "->" + response2.getCode());
                }

                @Override
                public void failed(Exception ex) {
                    latch1.countDown();
                    System.out.println(request2.getRequestUri() + "->" + ex);
                }

                @Override
                public void cancelled() {
                    latch1.countDown();
                    System.out.println(request2.getRequestUri() + " cancelled");
                }

            });
            latch1.await();

            // In real world one most likely would want also want to stream
            // request and response body content
            CountDownLatch latch2 = new CountDownLatch(1);
            AsyncRequestProducer producer3 = AsyncRequestBuilder.get(GET_URL).build();
            AbstractCharResponseConsumer<HttpResponse> consumer3 = new AbstractCharResponseConsumer<HttpResponse>() {

                HttpResponse response;

                @Override
                protected void start(HttpResponse response, ContentType contentType) {
                    this.response = response;
                }

                @Override
                protected int capacityIncrement() {
                    return Integer.MAX_VALUE;
                }

                @Override
                protected void data(CharBuffer data, boolean endOfStream) {
                    // Do something useful
                }

                @Override
                protected HttpResponse buildResult() {
                    return response;
                }

                @Override
                public void releaseResources() {
                }

            };

            httpclient.execute(producer3, consumer3, new FutureCallback<HttpResponse>() {

                @Override
                public void completed(HttpResponse response3) {
                    latch2.countDown();
                    System.out.println(request2.getRequestUri() + "->" + response3.getCode());
                }

                @Override
                public void failed(Exception ex) {
                    latch2.countDown();
                    System.out.println(request2.getRequestUri() + "->" + ex);
                }

                @Override
                public void cancelled() {
                    latch2.countDown();
                    System.out.println(request2.getRequestUri() + " cancelled");
                }

            });
            latch2.await();
        } catch (IOException | InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }


}
