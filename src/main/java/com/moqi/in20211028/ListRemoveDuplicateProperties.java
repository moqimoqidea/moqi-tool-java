package com.moqi.in20211028;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Remove duplicate from List java8
 * https://stackoverflow.com/a/45418889
 *
 * @author moqi
 * @date 10/28/21 11:22
 */
public class ListRemoveDuplicateProperties {

    public static void main(String[] args) {
        List<Person> personList = new ArrayList<>();
        personList.add(new Person("a", 10));
        personList.add(new Person("a", 20));
        System.out.println("personList = " + personList);

        TreeSet<Person> treeSet = personList.stream()
                .collect(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(Person::getName))));
        System.out.println("treeSet = " + treeSet);
    }

    @Data
    @AllArgsConstructor
    private static class Person {
        public String name;
        public int age;
    }

}
