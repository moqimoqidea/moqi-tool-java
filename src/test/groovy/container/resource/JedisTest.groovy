package container.resource

import org.apache.commons.collections4.CollectionUtils
import org.testcontainers.containers.GenericContainer
import org.testcontainers.spock.Testcontainers
import redis.clients.jedis.Jedis
import redis.clients.jedis.JedisPool
import redis.clients.jedis.args.BitOP
import redis.clients.jedis.args.ListDirection
import redis.clients.jedis.args.ListPosition
import redis.clients.jedis.args.SortedSetOption
import redis.clients.jedis.params.GetExParams
import redis.clients.jedis.params.LCSParams
import redis.clients.jedis.params.ZParams
import redis.clients.jedis.params.ZRangeParams
import redis.clients.jedis.resps.KeyedZSetElement
import redis.clients.jedis.resps.LCSMatchResult
import redis.clients.jedis.resps.ScanResult
import redis.clients.jedis.resps.Tuple
import redis.clients.jedis.util.KeyValue
import spock.lang.Shared
import spock.lang.Specification

import static container.constant.ContainerConstant.REDIS_ALPINE_IMAGE
import static container.constant.ContainerConstant.REDIS_TEST_PORT

/**
 * JedisTest
 *
 * @author moqi
 * @date 4/5/22 20:43
 */
@Testcontainers
class JedisTest extends Specification {

    /**
     * 使用共享标识时，redis 测试容器不会单方法隔离，缺点是需要用户手动保证数据隔离性，优点是速度快。
     *
     * 不使用共享标识时，redis 测试容器会单方法隔离，优点是不需要用户手动保证数据隔离性，缺点是速度慢。
     */
    @Shared
    private GenericContainer redis = new GenericContainer<>(REDIS_ALPINE_IMAGE)
            .withExposedPorts(REDIS_TEST_PORT)
    private Jedis jedis

    void setup() {
        String address = redis.host
        Integer port = redis.firstMappedPort

        JedisPool pool = new JedisPool(address, port)
        jedis = pool.getResource()
    }

    def "string append"() {
        when:
        jedis.set("foo", "bar")
        jedis.append("foo", "baz")

        then:
        jedis.get("foo") == "barbaz"
    }

    def "string decr"() {
        when:
        jedis.set("foo", "1")
        jedis.decr("foo")

        then:
        jedis.get("foo") == "0"
    }

    def "string decrBy"() {
        when:
        jedis.set("foo", "1")
        jedis.decrBy("foo", 100)

        then:
        jedis.get("foo") == "-99"
    }

    def "string get"() {
        setup:
        jedis.set("test", "example")

        when:
        String retrieved = jedis.get("test")

        then:
        retrieved == "example"
    }

    def "string getdel"() {
        setup:
        jedis.set("test", "example")

        when:
        String retrieved = jedis.getDel("test")

        then:
        retrieved == "example"
        jedis.get("test") == null
    }

    def "string getex"() {
        setup:
        jedis.set("test1", "example1")

        when:
        String retrieved = jedis.getEx("test1", new GetExParams().ex(1))

        then:
        retrieved == "example1"

        when:
        Thread.sleep(1111L)

        then:
        jedis.get("test1") == null
    }

    def "string getrange"() {
        setup:
        jedis.set("test1", "example1")

        when:
        String retrieved = jedis.getrange("test1", 0, 3)

        then:
        retrieved == "exam"
    }

    def "string getset"() {
        setup:
        jedis.set("test", "example")

        when:
        String retrieved = jedis.getSet("test", "new example")

        then:
        retrieved == "example"
        jedis.get("test") == "new example"
    }

    def "string incr"() {
        when:
        jedis.set("foo", "1")
        jedis.incr("foo")

        then:
        jedis.get("foo") == "2"
    }

    def "string incrBy"() {
        when:
        jedis.set("foo", "1")
        jedis.incrBy("foo", 100)

        then:
        jedis.get("foo") == "101"
    }

    def "string incrByFloat"() {
        when:
        jedis.set("foo", "1")
        jedis.incrByFloat("foo", 100.1)

        then:
        jedis.get("foo") == "101.1"
    }

    /**
     * https://redis.io/commands/lcs/
     */
    def "string lcs"() {
        when:
        jedis.mset("key1", "ohmytext", "key2", "mynewtext")
        LCSMatchResult stringMatchResult = jedis.lcs("key1", "key2",
                LCSParams.LCSParams())

        then:
        "mytext" == stringMatchResult.getMatchString()

        when:
        LCSMatchResult stringMatchResult2 = jedis.lcs("key1", "key2",
                LCSParams.LCSParams().idx().withMatchLen())

        then:
        stringMatchResult2.getLen() == 6
        2 == stringMatchResult2.getMatches().size()

        when:
        LCSMatchResult stringMatchResult3 = jedis.lcs("key1", "key2",
                LCSParams.LCSParams().idx().minMatchLen(10))

        then:
        0 == stringMatchResult3.getMatches().size()
    }

    def "string mset"() {
        when:
        jedis.mset("test1", "example1", "test2", "example2")

        then:
        jedis.get("test1") == "example1"
        jedis.get("test2") == "example2"
    }

    def "string msetnx"() {
        when:
        jedis.msetnx("msetnx", "example1", "msetnx2", "example2")

        then:
        jedis.get("msetnx") == "example1"
        jedis.get("msetnx2") == "example2"

        when:
        jedis.msetnx("msetnx", "abc", "jjjjjj", "example2")

        then:
        jedis.get("msetnx") == "example1"
        jedis.get("msetnx2") == "example2"
        jedis.get("jjjjjj") == null
    }

    def "string psetex"() {
        when:
        jedis.psetex("foo", 100L, "bar")

        then:
        jedis.get("foo") == "bar"

        when:
        Thread.sleep(150L)

        then:
        jedis.get("foo") == null
    }

    def "string mget"() {
        setup:
        jedis.set("test1", "example1")
        jedis.set("test2", "example2")

        when:
        List<String> retrieved = jedis.mget("test1", "test2")

        then:
        retrieved.get(0) == "example1"
        retrieved.get(1) == "example2"
    }

    def "string set"() {
        when:
        jedis.set("foo", "bar")

        then:
        jedis.get("foo") == "bar"
    }

    def "string setex"() {
        when:
        jedis.setex("foo", 1, "bar")

        then:
        jedis.get("foo") == "bar"

        when:
        Thread.sleep(1111L)

        then:
        jedis.get("foo") == null
    }

    def "string setnx"() {
        when:
        jedis.setnx("kkkkk", "bar")

        then:
        jedis.get("kkkkk") == "bar"

        when:
        jedis.setnx("kkkkk", "abcde")

        // nothing happens
        then:
        jedis.get("kkkkk") == "bar"
    }

    def "string setrange"() {
        when:
        jedis.set("foo", "bar")
        jedis.setrange("foo", 2, "lllllll")

        then:
        jedis.get("foo") == "balllllll"
    }

    def "string strlen"() {
        when:
        jedis.set("foo", "bar")

        then:
        jedis.strlen("foo") == 3
    }

    def "string substr"() {
        when:
        jedis.set("foo", "bar")

        then:
        jedis.substr("foo", 0, 1) == "ba"
    }

    def "hash hdel"() {
        setup:
        jedis.hmset("test_key", Map.of("field1", "value1", "field2", "value2"))

        when:
        List<String> originList = jedis.hmget("test_key", "field1", "field2")

        then:
        originList.get(0) == "value1"
        originList.get(1) == "value2"

        when:
        jedis.hdel("test_key", "field1", "field2")
        List<String> retrievedList = jedis.hmget("test_key", "field1", "field2")

        then:
        retrievedList.get(0) == null
        retrievedList.get(1) == null
    }

    def "hash hexists"() {
        setup:
        jedis.hmset("test_key1", Map.of("field1", "value1", "field2", "value2"))

        when:
        boolean exists = jedis.hexists("test_key1", "field1")

        then:
        exists
    }

    def "hash hget"() {
        setup:
        jedis.hmset("test_key2", Map.of("field1", "value1", "field2", "value2"))

        when:
        String value = jedis.hget("test_key2", "field1")

        then:
        value == "value1"
    }

    def "hash hgetall"() {
        setup:
        jedis.hmset("test_key3", Map.of("field1", "value1", "field2", "value2"))

        when:
        Map<String, String> map = jedis.hgetAll("test_key3")

        then:
        map.get("field1") == "value1"
        map.get("field2") == "value2"
    }

    def "hash hincrby"() {
        setup:
        jedis.hmset("test_key4", Map.of("field1", 1, "field2", 100))

        when:
        jedis.hincrBy("test_key4", "field2", 1)
        String value = jedis.hget("test_key4", "field2")

        then:
        value == "101"
    }

    def "hash hincrbyfloat"() {
        setup:
        jedis.hmset("test_key5", Map.of("field1", 1.0, "field2", 100.0))

        when:
        jedis.hincrByFloat("test_key5", "field2", 1.8)
        String value = jedis.hget("test_key5", "field2")

        then:
        value == "101.8"
    }

    def "hash hkeys"() {
        setup:
        jedis.hmset("test_key6", Map.of("field1", "value1", "field2", "value2"))

        when:
        Set<String> keys = jedis.hkeys("test_key6")

        then:
        keys.containsAll(List.of("field1", "field2"))
    }

    def "hash hlen"() {
        setup:
        jedis.hmset("test_key7", Map.of("field1", "value1", "field2", "value2"))

        when:
        long length = jedis.hlen("test_key7")

        then:
        length == 2
    }

    def "hash hmget"() {
        setup:
        jedis.hmset("test_key8", Map.of("field1", "value1", "field2", "value2"))

        when:
        List<String> values = jedis.hmget("test_key8", "field1", "field2")

        then:
        values.get(0) == "value1"
        values.get(1) == "value2"
    }

    def "hash hmset"() {
        setup:
        jedis.hmset("test_key9", Map.of("field1", "value1", "field2", "value2"))

        when:
        Map<String, String> map = jedis.hgetAll("test_key9")

        then:
        map.get("field1") == "value1"
        map.get("field2") == "value2"
    }

    def "hash hrandfield"() {
        setup:
        jedis.hmset("test_key10", Map.of("field1", "value1", "field2", "value2"))

        when:
        String field = jedis.hrandfield("test_key10")

        then:
        field == "field1" || field == "field2"
    }

    def "hash hscan"() {
        setup:
        jedis.hmset("test_key11", Map.of("field1", "value1", "field2", "value2"))

        when:
        ScanResult<Map.Entry<String, String>> scanResult = jedis.hscan("test_key11", "0")

        then:
        scanResult.getResult().size() == 2
    }

    def "hash hset"() {
        when:
        jedis.hset("test_key12", "field3", "value3")
        String value = jedis.hget("test_key12", "field3")

        then:
        value == "value3"
    }

    def "hash hsetnx"() {
        setup:
        jedis.hmset("test_key13", Map.of("field1", "value1", "field2", "value2"))

        when:
        jedis.hsetnx("test_key13", "field3", "value3")
        String value = jedis.hget("test_key13", "field3")

        then:
        value == "value3"

        when:
        jedis.hsetnx("test_key13", "field3", "sdfsdfsdf")
        String secondValue = jedis.hget("test_key13", "field3")

        then:
        secondValue == "value3"
    }

    def "hash hstrlen"() {
        setup:
        jedis.hmset("test_key14", Map.of("field1", "asdfasdf", "field2", "value2"))

        when:
        long length = jedis.hstrlen("test_key14", "field1")

        then:
        length == 8
    }

    def "hash hvals"() {
        setup:
        jedis.hmset("test_key14", Map.of("field1", "value1", "field2", "value2"))

        when:
        List<String> values = jedis.hvals("test_key14")

        then:
        values.containsAll(List.of("value1", "value2"))
    }

    def "list blmove"() {
        when:
        jedis.lpush("list_test_key36", "asdfasdf")
        jedis.lpush("list_test_key36", "value2")
        jedis.lpush("list_test_key36", "value3")
        jedis.blmove("list_test_key36", "list_test_key37", ListDirection.RIGHT, ListDirection.LEFT, 2.0)
        List<String> values = jedis.lrange("list_test_key37", 0, -1)

        then:
        values.containsAll(List.of("asdfasdf"))
    }

    def "list blmpop"() {
        when:
        String mylist1 = "mylist1"
        String mylist2 = "mylist2"

        // add elements to list
        jedis.lpush(mylist1, "one", "two", "three", "four", "five")
        jedis.lpush(mylist2, "one", "two", "three", "four", "five")

        KeyValue<String, List<String>> elements = jedis.blmpop(1L, ListDirection.RIGHT, 2, mylist1, mylist2)

        then:
        elements.getKey() == mylist1
        elements.getValue().containsAll(List.of("one", "two"))
    }

    def "list blpop"() {
        when:
        List<String> values = jedis.blpop(1, "list_test_key27")

        then:
        CollectionUtils.isEmpty(values)

        when:
        jedis.lpush("list_test_key27", "value1")
        List<String> step2Values = jedis.blpop(1, "list_test_key27")

        then:
        step2Values.containsAll(List.of("list_test_key27", "value1"))
    }

    def "list brpop"() {
        when:
        List<String> values = jedis.brpop(1, "list_test_key28")

        then:
        CollectionUtils.isEmpty(values)

        when:
        jedis.lpush("list_test_key28", "value1", "value2", "value3")
        List<String> step2Values = jedis.brpop(1, "list_test_key28")

        then:
        step2Values.containsAll(List.of("list_test_key28", "value1"))
    }

    def "list brpoplpush"() {
        when:
        jedis.brpoplpush("list_test_key31", "list_test_key32", 1)
        List<String> values = jedis.lrange("list_test_key32", 0, -1)

        then:
        CollectionUtils.isEmpty(values)

        when:
        jedis.lpush("list_test_key31", "value1")
        jedis.lpush("list_test_key31", "value2")
        jedis.lpush("list_test_key31", "value3")
        jedis.brpoplpush("list_test_key31", "list_test_key32", 1)
        List<String> step2Values = jedis.lrange("list_test_key32", 0, -1)

        then:
        step2Values.containsAll(List.of("value1"))
    }

    def "list lindex"() {
        setup:
        jedis.rpush("list_test_key20", "value1")
        jedis.rpush("list_test_key20", "value2")
        jedis.rpush("list_test_key20", "value3")

        when:
        String value = jedis.lindex("list_test_key20", 1)

        then:
        value == "value2"
    }

    def "list linsert"() {
        setup:
        jedis.rpush("list_test_key21", "value1")
        jedis.rpush("list_test_key21", "value2")
        jedis.rpush("list_test_key21", "value3")
        jedis.linsert("list_test_key21", ListPosition.AFTER, "value2", "value4")

        when:
        List<String> values = jedis.lrange("list_test_key21", 0, -1)

        then:
        values.containsAll(List.of("value1", "value4", "value3"))
    }

    def "list llen"() {
        setup:
        jedis.rpush("list_test_key17", "value1")
        jedis.rpush("list_test_key17", "value2")
        jedis.rpush("list_test_key17", "value3")

        when:
        long length = jedis.llen("list_test_key17")

        then:
        length == 3
    }

    def "list lmove"() {
        when:
        jedis.lpush("list_test_key34", "value1")
        jedis.lpush("list_test_key34", "value2")
        jedis.lpush("list_test_key34", "value3")
        jedis.lmove("list_test_key34", "list_test_key35", ListDirection.RIGHT, ListDirection.LEFT)
        List<String> values = jedis.lrange("list_test_key35", 0, -1)

        then:
        values.containsAll(List.of("value1"))
    }

    def "list lmpop"() {
        when:
        String mylist1 = "mylist1"
        String mylist2 = "mylist2"

        // add elements to list
        jedis.lpush(mylist1, "one", "two", "three", "four", "five")
        jedis.lpush(mylist2, "one", "two", "three", "four", "five")

        KeyValue<String, List<String>> elements = jedis.lmpop(ListDirection.LEFT, 2, mylist1, mylist2)

        then:
        elements.getKey() == mylist1
        elements.getValue().containsAll(List.of("four", "five"))
    }

    def "list lpop"() {
        setup:
        jedis.rpush("list_test_key23", "value1")
        jedis.rpush("list_test_key23", "value2")
        jedis.rpush("list_test_key23", "value3")

        when:
        String value = jedis.lpop("list_test_key23")

        then:
        value == "value1"
    }

    def "list lpos"() {
        when:
        jedis.lpush("list_test_key33", "value1")
        jedis.lpush("list_test_key33", "value2")
        jedis.lpush("list_test_key33", "value3")
        def index = jedis.lpos("list_test_key33", "value2")

        then:
        index == 1L
    }

    def "list lpush"() {
        when:
        jedis.lpush("list_test_key16", "value1")
        jedis.lpush("list_test_key16", "value2")
        jedis.lpush("list_test_key16", "value3")
        List<String> values = jedis.lrange("list_test_key16", 0, -1)

        then:
        values.containsAll(List.of("value3", "value2", "value1"))
    }

    def "list lpushx"() {
        when:
        jedis.lpushx("list_test_key29", "444")
        List<String> values = jedis.lrange("list_test_key29", 0, -1)

        then:
        CollectionUtils.isEmpty(values)

        when:
        jedis.rpush("list_test_key29", "value1")
        jedis.rpush("list_test_key29", "value2")
        jedis.rpush("list_test_key29", "value3")
        jedis.lpushx("list_test_key29", "value4")
        List<String> step2Values = jedis.lrange("list_test_key29", 0, -1)

        then:
        step2Values.containsAll(List.of("value4", "value1", "value2", "value3"))
    }

    def "list lrange"() {
        setup:
        jedis.rpush("list_test_key17", "value1")
        jedis.rpush("list_test_key17", "value2")
        jedis.rpush("list_test_key17", "value3")

        when:
        List<String> values = jedis.lrange("list_test_key17", 0, -1)

        then:
        values.containsAll(List.of("value1", "value2", "value3"))
    }

    def "list lrem"() {
        setup:
        jedis.rpush("list_test_key22", "value1")
        jedis.rpush("list_test_key22", "value2")
        jedis.rpush("list_test_key22", "value3")
        jedis.lrem("list_test_key22", 1, "value2")

        when:
        List<String> values = jedis.lrange("list_test_key22", 0, -1)

        then:
        values.containsAll(List.of("value1", "value3"))
    }

    def "list lset"() {
        setup:
        jedis.rpush("list_test_key19", "value1")
        jedis.rpush("list_test_key19", "value2")
        jedis.rpush("list_test_key19", "value3")
        jedis.lset("list_test_key19", 0, "value4")

        when:
        List<String> values = jedis.lrange("list_test_key19", 0, -1)

        then:
        values.containsAll(List.of("value4", "value2", "value3"))
    }

    def "list ltrim"() {
        setup:
        jedis.rpush("list_test_key18", "value1")
        jedis.rpush("list_test_key18", "value2")
        jedis.rpush("list_test_key18", "value3")
        jedis.ltrim("list_test_key18", 1, -1)

        when:
        List<String> values = jedis.lrange("list_test_key18", 0, -1)

        then:
        values.containsAll(List.of("value2", "value3"))
    }

    def "list rpop"() {
        setup:
        jedis.rpush("list_test_key24", "value1")
        jedis.rpush("list_test_key24", "value2")
        jedis.rpush("list_test_key24", "value3")

        when:
        String value = jedis.rpop("list_test_key24")

        then:
        value == "value3"
    }

    def "list rpoplpush"() {
        setup:
        jedis.rpush("list_test_key25", "value1")
        jedis.rpush("list_test_key25", "value2")
        jedis.rpush("list_test_key25", "value3")
        jedis.rpush("list_test_key26", "foo")
        jedis.rpush("list_test_key26", "bar")

        when:
        String value = jedis.rpoplpush("list_test_key25", "list_test_key26")
        List<String> listTestKey25Values = jedis.lrange("list_test_key25", 0, -1)
        List<String> listTestKey26Values = jedis.lrange("list_test_key26", 0, -1)

        then:
        value == "value3"
        listTestKey25Values.containsAll(List.of("value1", "value2"))
        listTestKey26Values.containsAll(List.of("value3", "foo", "bar"))
    }

    def "list rpush"() {
        when:
        jedis.rpush("list_test_key15", "value1")
        jedis.rpush("list_test_key15", "value2")
        jedis.rpush("list_test_key15", "value3")
        List<String> values = jedis.lrange("list_test_key15", 0, -1)

        then:
        values.containsAll(List.of("value1", "value2", "value3"))
    }

    def "list rpushx"() {
        when:
        jedis.rpushx("list_test_key30", "444")
        List<String> values = jedis.lrange("list_test_key30", 0, -1)

        then:
        CollectionUtils.isEmpty(values)

        when:
        jedis.lpush("list_test_key30", "value1")
        jedis.lpush("list_test_key30", "value2")
        jedis.lpush("list_test_key30", "value3")
        jedis.rpushx("list_test_key30", "value4")
        List<String> step2Values = jedis.lrange("list_test_key30", 0, -1)

        then:
        step2Values.containsAll(List.of("value1", "value2", "value3", "value4"))
    }

    def "set sadd"() {
        when:
        long changeSize = jedis.sadd("set_test_key1", "value1")

        then:
        changeSize == 1L

        when:
        long secondChangeSize = jedis.sadd("set_test_key1", "value1")

        then:
        secondChangeSize == 0L
    }

    def "set scard"() {
        when:
        jedis.sadd("set_test_key4", "value1")
        jedis.sadd("set_test_key4", "value2")
        jedis.sadd("set_test_key4", "value3")
        long size = jedis.scard("set_test_key4")

        then:
        size == 3L
    }

    def "set sdiff"() {
        when:
        jedis.sadd("set_test_key5", "value1")
        jedis.sadd("set_test_key5", "value2")
        jedis.sadd("set_test_key5", "value3")
        jedis.sadd("set_test_key6", "value1")
        jedis.sadd("set_test_key6", "value2")
        jedis.sadd("set_test_key6", "value8")
        def sdiff = jedis.sdiff("set_test_key5", "set_test_key6")

        then:
        sdiff.containsAll(List.of("value3"))
    }

    def "set sdiffinstore"() {
        when:
        jedis.sadd("set_test_key7", "value1")
        jedis.sadd("set_test_key7", "value2")
        jedis.sadd("set_test_key7", "value3")
        jedis.sadd("set_test_key8", "value1")
        jedis.sadd("set_test_key8", "value2")
        jedis.sadd("set_test_key8", "value8")
        def sdiffSize = jedis.sdiffstore("set_test_key9", "set_test_key7", "set_test_key8")
        ScanResult<String> result = jedis.sscan("set_test_key9", "0")

        then:
        sdiffSize == 1L
        result.result.containsAll(List.of("value3"))
    }

    def "set sinter"() {
        when:
        jedis.sadd("set_test_key10", "value1")
        jedis.sadd("set_test_key10", "value2")
        jedis.sadd("set_test_key10", "value3")
        jedis.sadd("set_test_key11", "value1")
        jedis.sadd("set_test_key11", "value2")
        jedis.sadd("set_test_key11", "value8")
        def result = jedis.sinter("set_test_key10", "set_test_key11")

        then:
        result.containsAll(List.of("value1", "value2"))
    }

    def "set sintercard"() {
        when:
        jedis.sadd("set_test_key12", "value1")
        jedis.sadd("set_test_key12", "value2")
        jedis.sadd("set_test_key12", "value3")
        jedis.sadd("set_test_key13", "value1")
        jedis.sadd("set_test_key13", "value2")
        jedis.sadd("set_test_key13", "value8")
        long resultSize = jedis.sintercard("set_test_key12", "set_test_key13")

        then:
        resultSize == 2L
    }

    def "set sinterstore"() {
        when:
        jedis.sadd("set_test_key14", "value1")
        jedis.sadd("set_test_key14", "value2")
        jedis.sadd("set_test_key14", "value3")
        jedis.sadd("set_test_key15", "value1")
        jedis.sadd("set_test_key15", "value2")
        jedis.sadd("set_test_key15", "value8")
        long resultSize = jedis.sinterstore("set_test_key16", "set_test_key14", "set_test_key15")
        ScanResult<String> result = jedis.sscan("set_test_key16", "0")

        then:
        resultSize == 2L
        result.result.containsAll(List.of("value1", "value2"))
    }

    def "set sismember"() {
        when:
        jedis.sadd("set_test_key17", "value1")
        jedis.sadd("set_test_key17", "value2")
        jedis.sadd("set_test_key17", "value3")
        boolean value1IsMember = jedis.sismember("set_test_key17", "value1")
        boolean value4IsMember = jedis.sismember("set_test_key17", "value4")

        then:
        value1IsMember
        !value4IsMember
    }

    def "set smembers"() {
        when:
        jedis.sadd("set_test_key18", "value1")
        jedis.sadd("set_test_key18", "value2")
        jedis.sadd("set_test_key18", "value3")
        Set<String> resultSet = jedis.smembers("set_test_key18")

        then:
        resultSet.containsAll(Set.of("value1", "value2", "value3"))
    }

    def "set smismember"() {
        when:
        jedis.sadd("set_test_key19", "value1")
        jedis.sadd("set_test_key19", "value2")
        jedis.sadd("set_test_key19", "value3")
        def list = jedis.smismember("set_test_key19", "value1", "value4")

        then:
        list.get(0)
        !list.get(1)
    }

    def "set smove"() {
        when:
        jedis.sadd("set_test_key20", "value1")
        jedis.sadd("set_test_key20", "value2")
        jedis.sadd("set_test_key20", "value3")
        jedis.sadd("set_test_key21", "value4")
        jedis.sadd("set_test_key21", "value5")
        jedis.sadd("set_test_key21", "value6")
        jedis.smove("set_test_key20", "set_test_key21", "value1")
        long resultSize = jedis.scard("set_test_key21")
        ScanResult<String> result = jedis.sscan("set_test_key21", "0")

        then:
        resultSize == 4L
        result.result.containsAll(List.of("value4", "value6", "value1", "value5"))
    }

    def "set spop"() {
        when:
        jedis.sadd("set_test_key22", "value1")
        jedis.sadd("set_test_key22", "value2")
        jedis.sadd("set_test_key22", "value3")
        jedis.spop("set_test_key22")
        long resultSize = jedis.scard("set_test_key22")

        then:
        resultSize == 2L
    }

    def "set srandmember"() {
        when:
        jedis.sadd("set_test_key23", "value1")
        jedis.sadd("set_test_key23", "value2")
        jedis.sadd("set_test_key23", "value3")
        String result = jedis.srandmember("set_test_key23")

        then:
        result == "value1" || result == "value2" || result == "value3"
    }

    def "set srem"() {
        when:
        jedis.sadd("set_test_key3", "value1")
        jedis.sadd("set_test_key3", "value2")
        jedis.sadd("set_test_key3", "value3")
        jedis.srem("set_test_key3", "value2")
        Set<String> values = jedis.smembers("set_test_key3")

        then:
        values.containsAll(List.of("value1", "value3"))
    }

    def "set sscan"() {
        when:
        jedis.sadd("set_test_key24", "value1")
        jedis.sadd("set_test_key24", "value2")
        jedis.sadd("set_test_key24", "value3")
        ScanResult<String> result = jedis.sscan("set_test_key24", "0")

        then:
        result.result.containsAll(List.of("value1", "value2", "value3"))
    }

    def "set sunion"() {
        when:
        jedis.sadd("set_test_key25", "value1")
        jedis.sadd("set_test_key25", "value2")
        jedis.sadd("set_test_key25", "value3")
        jedis.sadd("set_test_key26", "value4")
        jedis.sadd("set_test_key26", "value5")
        jedis.sadd("set_test_key26", "value6")
        Set<String> resultSet = jedis.sunion("set_test_key25", "set_test_key26")

        then:
        resultSet.size() == 6L
    }

    def "set sunionstore"() {
        when:
        jedis.sadd("set_test_key27", "value1")
        jedis.sadd("set_test_key27", "value2")
        jedis.sadd("set_test_key27", "value3")
        jedis.sadd("set_test_key28", "value1")
        jedis.sadd("set_test_key28", "value2")
        jedis.sadd("set_test_key28", "value6")
        jedis.sunionstore("set_test_key29", "set_test_key27", "set_test_key28")
        Set<String> set = jedis.smembers("set_test_key29")

        then:
        set.containsAll(List.of("value1", "value2", "value3", "value6"))
    }

    def "zset bzmpop"() {
        when:
        KeyValue<String, List<Tuple>> keyValue = jedis.bzmpop(1L, SortedSetOption.MAX, "zset_test_key1")

        then:
        keyValue == null

        when:
        jedis.zadd("zset_test_key1", 1, "value1")
        KeyValue<String, List<Tuple>> secondKeyValue = jedis.bzmpop(1L, SortedSetOption.MAX, "zset_test_key1")

        then:
        secondKeyValue.getValue().size() == 1L
    }

    def "zset bzpopmax"() {
        when:
        jedis.zadd("zset_test_key2", 1, "value1")
        jedis.zadd("zset_test_key2", 2, "value2")
        jedis.zadd("zset_test_key2", 3, "value3")
        KeyedZSetElement element = jedis.bzpopmax(1, "zset_test_key2")

        then:
        element != null
        element.getKey() == "zset_test_key2"
        element.getElement() == "value3"
        element.getScore() == 3.0
    }

    def "zset bzpopmin"() {
        when:
        jedis.zadd("zset_test_key3", 1, "value1")
        jedis.zadd("zset_test_key3", 2, "value2")
        jedis.zadd("zset_test_key3", 3, "value3")
        KeyedZSetElement element = jedis.bzpopmin(1, "zset_test_key3")

        then:
        element != null
        element.getKey() == "zset_test_key3"
        element.getElement() == "value1"
        element.getScore() == 1.0
    }

    def "zset zadd"() {
        when:
        jedis.zadd("zset_test_key4", 1, "value1")
        long size = jedis.zcard("zset_test_key4")

        then:
        size == 1L
    }

    def "zset zcard"() {
        when:
        jedis.zadd("zset_test_key5", 1, "value1")
        long size = jedis.zcard("zset_test_key5")

        then:
        size == 1L
    }

    def "zset zcount"() {
        when:
        jedis.zadd("zset_test_key6", 1, "value1")
        jedis.zadd("zset_test_key6", 2, "value2")
        jedis.zadd("zset_test_key6", 3, "value3")
        jedis.zadd("zset_test_key6", 10, "value4")
        long size = jedis.zcount("zset_test_key6", 1.1, 9.9)

        then:
        size == 2L
    }

    def "zset zdiff"() {
        when:
        jedis.zadd("zset_test_key7", 1, "value1")
        jedis.zadd("zset_test_key7", 2, "value2")
        jedis.zadd("zset_test_key7", 3, "value3")
        jedis.zadd("zset_test_key7", 10, "value4")
        jedis.zadd("zset_test_key8", 1, "value1")
        jedis.zadd("zset_test_key8", 2, "value2")
        jedis.zadd("zset_test_key8", 3, "value3")
        jedis.zadd("zset_test_key8", 10, "value9")
        Set<String> set = jedis.zdiff("zset_test_key7", "zset_test_key8")

        then:
        set.containsAll(List.of("value4"))
    }

    def "zset zdiffstore"() {
        when:
        jedis.zadd("zset_test_key9", 1, "value1")
        jedis.zadd("zset_test_key9", 2, "value2")
        jedis.zadd("zset_test_key9", 3, "value3")
        jedis.zadd("zset_test_key9", 10, "value4")
        jedis.zadd("zset_test_key10", 1, "value1")
        jedis.zadd("zset_test_key10", 2, "value2")
        jedis.zadd("zset_test_key10", 3, "value3")
        jedis.zadd("zset_test_key10", 10, "value9")
        long size = jedis.zdiffStore("zset_test_key11", "zset_test_key9", "zset_test_key10")
        Set<String> set = jedis.zrange("zset_test_key11", 0, -1)

        then:
        size == 1L
        set.containsAll(List.of("value4"))
    }

    def "zset zincrby"() {
        when:
        jedis.zadd("zset_test_key12", 1, "value1")
        jedis.zincrby("zset_test_key12", 2, "value1")
        double score = jedis.zscore("zset_test_key12", "value1")

        then:
        score == 3.0
    }

    def "zset zinter"() {
        when:
        jedis.zadd("zset_test_key13", 1, "value1")
        jedis.zadd("zset_test_key13", 2, "value2")
        jedis.zadd("zset_test_key13", 3, "value3")
        jedis.zadd("zset_test_key13", 10, "value4")
        jedis.zadd("zset_test_key14", 1, "value1")
        jedis.zadd("zset_test_key14", 2, "value2")
        jedis.zadd("zset_test_key14", 3, "value3")
        jedis.zadd("zset_test_key14", 10, "value9")

        and:
        ZParams params = new ZParams()
        params.weights(2, 2.5)
        params.aggregate(ZParams.Aggregate.SUM)
        Set<String> set = jedis.zinter(params, "zset_test_key13", "zset_test_key14")

        then:
        set != null
        set.containsAll(List.of("value1", "value2", "value3"))
    }

    def "zset zintercard"() {
        when:
        jedis.zadd("zset_test_key15", 1, "value1")
        jedis.zadd("zset_test_key15", 2, "value2")
        jedis.zadd("zset_test_key15", 3, "value3")
        jedis.zadd("zset_test_key15", 10, "value4")
        jedis.zadd("zset_test_key16", 1, "value1")
        jedis.zadd("zset_test_key16", 2, "value2")
        jedis.zadd("zset_test_key16", 3, "value3")
        jedis.zadd("zset_test_key16", 10, "value9")
        long size = jedis.zintercard("zset_test_key15", "zset_test_key16")

        then:
        size == 3L
    }

    def "zset zinterstore"() {
        when:
        jedis.zadd("zset_test_key07", 1, "value1")
        jedis.zadd("zset_test_key07", 2, "value2")
        jedis.zadd("zset_test_key07", 3, "value3")
        jedis.zadd("zset_test_key07", 10, "value4")
        jedis.zadd("zset_test_key08", 1, "value1")
        jedis.zadd("zset_test_key08", 2, "value2")
        jedis.zadd("zset_test_key08", 3, "value3")
        jedis.zadd("zset_test_key08", 10, "value9")
        long size = jedis.zinterstore("zset_test_key09", "zset_test_key07", "zset_test_key08")

        then:
        size == 3L
    }

    def "zset zlexcount"() {
        when:
        jedis.zadd("zset_test_key17", 1, "value1")
        jedis.zadd("zset_test_key17", 2, "value2")
        jedis.zadd("zset_test_key17", 3, "value3")
        jedis.zadd("zset_test_key17", 10, "value4")
        long size = jedis.zlexcount("zset_test_key17", "[value2", "[value4")

        then:
        size == 3L
    }

    def "zset zmpop"() {
        when:
        jedis.zadd("zset_test_key18", 1, "value1")
        jedis.zadd("zset_test_key18", 2, "value2")
        jedis.zadd("zset_test_key18", 3, "value3")
        jedis.zadd("zset_test_key18", 10, "value4")
        KeyValue<String, List<Tuple>> keyValue = jedis.zmpop(SortedSetOption.MAX, 2, "zset_test_key18")
        long size = jedis.zcard("zset_test_key18")

        then:
        size == 2L
        keyValue.getValue().size() == 2
        keyValue.getValue().get(0).getScore() == 10.0
    }

    def "zset zmscore"() {
        when:
        jedis.zadd("zset_test_key19", 1, "value1")
        jedis.zadd("zset_test_key19", 2, "value2")
        jedis.zadd("zset_test_key19", 3, "value3")
        jedis.zadd("zset_test_key19", 10, "value4")
        List<Double> scores = jedis.zmscore("zset_test_key19", "value1", "value2", "value3", "value4")
        long size = jedis.zcard("zset_test_key19")

        then:
        size == 4L
        scores.size() == 4
        scores.get(0) == 1.0
        scores.get(1) == 2.0
        scores.get(2) == 3.0
        scores.get(3) == 10.0
    }

    def "zset zpopmax"() {
        when:
        jedis.zadd("zset_test_key20", 1, "value1")
        jedis.zadd("zset_test_key20", 2, "value2")
        jedis.zadd("zset_test_key20", 3, "value3")
        jedis.zadd("zset_test_key20", 10, "value4")
        Tuple tuple = jedis.zpopmax("zset_test_key20")

        then:
        tuple.getElement() == "value4"
        tuple.getScore() == 10.0
    }

    def "zset zpopmin"() {
        when:
        jedis.zadd("zset_test_key21", 1, "value1")
        jedis.zadd("zset_test_key21", 2, "value2")
        jedis.zadd("zset_test_key21", 3, "value3")
        jedis.zadd("zset_test_key21", 10, "value4")
        Tuple tuple = jedis.zpopmin("zset_test_key21")

        then:
        tuple.getElement() == "value1"
        tuple.getScore() == 1.0
    }

    def "zset zrandmember"() {
        when:
        jedis.zadd("zset_test_key22", 1, "value1")
        jedis.zadd("zset_test_key22", 2, "value2")
        jedis.zadd("zset_test_key22", 3, "value3")
        jedis.zadd("zset_test_key22", 10, "value4")
        String value = jedis.zrandmember("zset_test_key22")

        then:
        value == "value1" || value == "value2" || value == "value3" || value == "value4"
    }

    def "zset zrange"() {
        when:
        jedis.zadd("zset_test_key23", 1, "value1")
        jedis.zadd("zset_test_key23", 2, "value2")
        jedis.zadd("zset_test_key23", 3, "value3")
        jedis.zadd("zset_test_key23", 10, "value4")
        List<String> values = jedis.zrange("zset_test_key23", 0, -1)

        then:
        values.size() == 4
    }

    def "zset zrangebylex"() {
        when:
        jedis.zadd("zset_test_key24", 1, "value1")
        jedis.zadd("zset_test_key24", 2, "value2")
        jedis.zadd("zset_test_key24", 3, "value3")
        jedis.zadd("zset_test_key24", 10, "value4")
        List<String> values = jedis.zrangeByLex("zset_test_key24", "[value2", "[value4")

        then:
        values.size() == 3
        values.get(0) == "value2"
        values.get(1) == "value3"
        values.get(2) == "value4"
    }

    def "zset zrangebyscore"() {
        when:
        jedis.zadd("zset_test_key25", 1, "value1")
        jedis.zadd("zset_test_key25", 2, "value2")
        jedis.zadd("zset_test_key25", 3, "value3")
        jedis.zadd("zset_test_key25", 10, "value4")
        List<String> values = jedis.zrangeByScore("zset_test_key25", 2, 10)

        then:
        values.size() == 3
        values.get(0) == "value2"
        values.get(1) == "value3"
        values.get(2) == "value4"
    }

    def "zset zrangestore"() {
        when:
        jedis.zadd("zset_test_key26", 1, "value1")
        jedis.zadd("zset_test_key26", 2, "value2")
        jedis.zadd("zset_test_key26", 3, "value3")
        jedis.zadd("zset_test_key26", 10, "value4")
        long size = jedis.zrangestore("zset_test_key27", "zset_test_key26", ZRangeParams.zrangeByScoreParams(1, 2))

        then:
        size == 2L
    }

    def "zset zrank"() {
        when:
        jedis.zadd("zset_test_key28", 1, "value1")
        jedis.zadd("zset_test_key28", 2, "value2")
        jedis.zadd("zset_test_key28", 3, "value3")
        jedis.zadd("zset_test_key28", 10, "value4")
        long rank = jedis.zrank("zset_test_key28", "value4")

        then:
        rank == 3L
    }

    def "zset zrem"() {
        when:
        jedis.zadd("zset_test_key29", 1, "value1")
        jedis.zadd("zset_test_key29", 2, "value2")
        jedis.zadd("zset_test_key29", 3, "value3")
        jedis.zadd("zset_test_key29", 10, "value4")
        long size = jedis.zrem("zset_test_key29", "value1", "value1")

        then:
        size == 1L
    }

    def "zset zremrangebylex"() {
        when:
        jedis.zadd("zset_test_key30", 1, "value1")
        jedis.zadd("zset_test_key30", 2, "value2")
        jedis.zadd("zset_test_key30", 3, "value3")
        jedis.zadd("zset_test_key30", 10, "value4")
        long size = jedis.zremrangeByLex("zset_test_key30", "[value2", "[value4")
        long zsetSize = jedis.zcard("zset_test_key30")

        then:
        size == 3L
        zsetSize == 1L
    }

    def "zset zremrangebyrank"() {
        when:
        jedis.zadd("zset_test_key32", 1, "value1")
        jedis.zadd("zset_test_key32", 2, "value2")
        jedis.zadd("zset_test_key32", 3, "value3")
        jedis.zadd("zset_test_key32", 10, "value4")
        long size = jedis.zremrangeByRank("zset_test_key32", 1, 2)
        long zsetSize = jedis.zcard("zset_test_key32")

        then:
        size == 2L
        zsetSize == 2L
    }

    def "zset zremrangebyscore"() {
        when:
        jedis.zadd("zset_test_key31", 1, "value1")
        jedis.zadd("zset_test_key31", 2, "value2")
        jedis.zadd("zset_test_key31", 3, "value3")
        jedis.zadd("zset_test_key31", 10, "value4")
        long size = jedis.zremrangeByScore("zset_test_key31", 2, 10)
        long zsetSize = jedis.zcard("zset_test_key31")

        then:
        size == 3L
        zsetSize == 1L
    }

    def "zset zrevrange"() {
        when:
        jedis.zadd("zset_test_key33", 1, "value1")
        jedis.zadd("zset_test_key33", 2, "value2")
        jedis.zadd("zset_test_key33", 3, "value3")
        jedis.zadd("zset_test_key33", 10, "value4")
        List<String> values = jedis.zrevrange("zset_test_key33", 0, -1)

        then:
        values.size() == 4
    }

    def "zset zrevrangebyscore"() {
        when:
        jedis.zadd("zset_test_key34", 1, "value1")
        jedis.zadd("zset_test_key34", 2, "value2")
        jedis.zadd("zset_test_key34", 3, "value3")
        jedis.zadd("zset_test_key34", 10, "value4")
        List<String> values = jedis.zrevrangeByScore("zset_test_key34", 10, 2)

        then:
        values.size() == 3
    }

    def "zset zrevrangebylex"() {
        when:
        jedis.zadd("zset_test_key35", 1, "value1")
        jedis.zadd("zset_test_key35", 2, "value2")
        jedis.zadd("zset_test_key35", 3, "value3")
        jedis.zadd("zset_test_key35", 10, "value4")
        List<String> values = jedis.zrevrangeByLex("zset_test_key35", "[value4", "[value2")

        then:
        values.size() == 3
    }

    def "zset zrevrank"() {
        when:
        jedis.zadd("zset_test_key36", 1, "value1")
        jedis.zadd("zset_test_key36", 2, "value2")
        jedis.zadd("zset_test_key36", 3, "value3")
        jedis.zadd("zset_test_key36", 10, "value4")
        long rank = jedis.zrevrank("zset_test_key36", "value2")

        then:
        rank == 2L
    }

    def "zset zscan"() {
        when:
        jedis.zadd("zset_test_key37", 1, "value1")
        jedis.zadd("zset_test_key37", 2, "value2")
        jedis.zadd("zset_test_key37", 3, "value3")
        jedis.zadd("zset_test_key37", 10, "value4")
        ScanResult<Tuple> scanResult = jedis.zscan("zset_test_key37", "0")
        List<Tuple> values = scanResult.getResult()

        then:
        values.size() == 4
        values.get(0).score == 1.0
    }

    def "zset zscore"() {
        when:
        jedis.zadd("zset_test_key38", 1, "value1")
        jedis.zadd("zset_test_key38", 2, "value2")
        jedis.zadd("zset_test_key38", 3, "value3")
        jedis.zadd("zset_test_key38", 10, "value4")
        double score = jedis.zscore("zset_test_key38", "value2")

        then:
        score == 2.0
    }

    def "zset zunion"() {
        when:
        jedis.zadd("zset_test_key42", 1, "value1")
        jedis.zadd("zset_test_key42", 2, "value2")
        jedis.zadd("zset_test_key42", 3, "value3")
        jedis.zadd("zset_test_key42", 10, "value4")
        jedis.zadd("zset_test_key43", 1, "value1")
        jedis.zadd("zset_test_key43", 2, "value2")
        jedis.zadd("zset_test_key43", 3, "value3")
        jedis.zadd("zset_test_key43", 10, "value8")

        and:
        ZParams params = new ZParams()
        params.weights(2, 2.5)
        params.aggregate(ZParams.Aggregate.SUM)
        Set<String> set = jedis.zunion(params, "zset_test_key42", "zset_test_key43")

        then:
        set.size() == 5L
    }

    def "zset zunionstore"() {
        when:
        jedis.zadd("zset_test_key39", 1, "value1")
        jedis.zadd("zset_test_key39", 2, "value2")
        jedis.zadd("zset_test_key39", 3, "value3")
        jedis.zadd("zset_test_key39", 10, "value4")
        jedis.zadd("zset_test_key40", 1, "value1")
        jedis.zadd("zset_test_key40", 2, "value2")
        jedis.zadd("zset_test_key40", 3, "value3")
        jedis.zadd("zset_test_key40", 10, "value8")
        long size = jedis.zunionstore("zset_test_key41", "zset_test_key39", "zset_test_key40")

        then:
        size == 5L
    }

    def "bitmap bitcount"() {
        when:
        jedis.setbit("bitmap_test_key1", 0, true)
        long count = jedis.bitcount("bitmap_test_key1")

        then:
        count == 1L
    }

    /**
     * https://redis.io/commands/bitfield/
     */
    def "bitmap bitfield"() {
        when:
        jedis.setbit("bitmap_test_key2", 0, true)
        List<Long> responses = jedis.bitfield("bitmap_test_key2", "INCRBY", "i5", "100", "1", "GET", "u4", "0")

        then:
        responses.get(0) == 1L
        responses.get(1) == 8L
    }

    def "bitmap bitfield_ro"() {
        when:
        jedis.setbit("bitmap_test_key3", 0, true)
        jedis.bitfield("bitmap_test_key3", "INCRBY", "i5", "100", "1", "GET", "u4", "0")
        List<Long> responses = jedis.bitfieldReadonly("bitmap_test_key3", "GET", "i5", "100")

        then:
        responses.get(0) == 1L
    }

    def "bitmap bitop"() {
        when:
        jedis.set("bitmap_test_key4", "\u0060")
        jedis.set("bitmap_test_key5", "\u0044")

        jedis.bitop(BitOP.AND, "resultAnd", "bitmap_test_key4", "bitmap_test_key5")
        String resultAnd = jedis.get("resultAnd")

        jedis.bitop(BitOP.OR, "resultOr", "bitmap_test_key4", "bitmap_test_key5")
        String resultOr = jedis.get("resultOr")

        jedis.bitop(BitOP.XOR, "resultXor", "bitmap_test_key4", "bitmap_test_key5")
        String resultXor = jedis.get("resultXor")

        then:
        resultAnd == "\u0040"
        resultOr == "\u0064"
        resultXor == "\u0024"
    }

    def "bitmap bitpos"() {
        when:
        String bitMapTestKey6 = "bitMapTestKey6"

        jedis.set(bitMapTestKey6, String.valueOf(0))

        jedis.setbit(bitMapTestKey6, 3, true)
        jedis.setbit(bitMapTestKey6, 7, true)
        jedis.setbit(bitMapTestKey6, 13, true)
        jedis.setbit(bitMapTestKey6, 39, true)

        /*
         * byte: 0 1 2 3 4 bit: 00010001 / 00000100 / 00000000 / 00000000 / 00000001
         */
        long offset = jedis.bitpos(bitMapTestKey6, true)

        then:
        offset == 2L
    }

    def "bitmap getbit"() {
        when:
        jedis.setbit("bitmap_test_key7", 0, true)
        boolean result = jedis.getbit("bitmap_test_key7", 0)

        then:
        result
    }

    def "bitmap setbit"() {
        when:
        jedis.setbit("bitmap_test_key8", 0, true)
        boolean result = jedis.getbit("bitmap_test_key8", 0)

        then:
        result
    }

}


