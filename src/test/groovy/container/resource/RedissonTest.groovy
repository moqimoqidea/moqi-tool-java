package container.resource

import groovy.util.logging.Slf4j
import org.redisson.Redisson
import org.redisson.api.RedissonClient
import org.redisson.config.Config
import org.testcontainers.containers.GenericContainer
import org.testcontainers.spock.Testcontainers
import spock.lang.Specification

import static container.constant.ContainerConstant.REDIS_ALPINE_IMAGE
import static container.constant.ContainerConstant.REDIS_TEST_PORT

/**
 * RedissonTest
 *
 * @author moqi
 * @date 8/5/22 15:45
 */
@Slf4j
@Testcontainers
class RedissonTest extends Specification {

    private GenericContainer redis = new GenericContainer<>(REDIS_ALPINE_IMAGE).withExposedPorts(REDIS_TEST_PORT)
    private RedissonClient redisson1
    private RedissonClient redisson2

    void setup() {
        String address = redis.host
        Integer port = redis.firstMappedPort

        Config config = new Config()
        config.useSingleServer().setAddress("redis://" + address + ":" + port)

        redisson1 = Redisson.create(config)
        redisson2 = Redisson.create(config)
    }

    void testRMap() {
        setup:
        redisson1.getMap("test_map").put("key", "value")

        when:
        def value = redisson2.getMap("test_map").get("key")

        then:
        value == "value"
    }

}


