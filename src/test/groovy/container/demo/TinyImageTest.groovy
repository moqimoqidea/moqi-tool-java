package container.demo

import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.Network
import org.testcontainers.spock.Testcontainers
import spock.lang.Specification

/**
 * https://www.testcontainers.org/features/networking/
 *
 * @author moqi
 * @date 4/5/22 20:43
 */
@Testcontainers
class TinyImageTest extends Specification {


    public static final String NGINX_IMAGE = "nginx:alpine"

    void testNetwork() {
        setup:
        Network network = Network.newNetwork()

        GenericContainer foo = new GenericContainer<>(NGINX_IMAGE)
                .withNetwork(network)
                .withNetworkAliases("foo")
                .withCommand("/bin/sh", "-c", "while true ; do printf 'HTTP/1.1 200 OK\\n\\nyay' | nc -l -p 8080; done");

        GenericContainer bar = new GenericContainer<>(NGINX_IMAGE)
                .withNetwork(network)
                .withCommand("top")

        when:
        foo.start()
        bar.start()
        String response = bar.execInContainer("wget", "-O", "-", "http://foo:8080").getStdout()

        then:
        response == "yay"
    }
}


