package container.demo


import org.testcontainers.containers.GenericContainer
import org.testcontainers.spock.Testcontainers
import spock.lang.Shared
import spock.lang.Specification

/**
 * https://www.testcontainers.org/features/commands/
 *
 * @author moqi
 * @date 4/5/22 20:43
 */
@Testcontainers
class ContainerCommandTest extends Specification {

    @Shared
    GenericContainer container = new GenericContainer<>("redis:6-alpine")
                .withCommand("redis-server --port 7777")

    void testCommand() {
        setup:
        container.execInContainer("touch", "/somefile.txt")

        when:
        def lsResult = container.execInContainer("ls", "-al", "/")
        def stdout = lsResult.getStdout()
        def exitCode = lsResult.getExitCode()

        then:
        stdout.contains("somefile.txt")
        exitCode == 0
    }
}


