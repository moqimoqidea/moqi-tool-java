package com.moqi.in20220120

import spock.lang.Specification

/**
 * @author moqi
 * @date 2022/1/20 16:49
 */
class PersonTest extends Specification {

    def "test lombok toString not contain name"() {
        when:
        def person = new Person("moqi", 18)
        println(person)

        then:
        !person.toString().contains("moqi")
    }

}
