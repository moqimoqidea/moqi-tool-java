package com.moqi.in20220320;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * https://www.baeldung.com/guava-eventbus
 *
 * @author moqi
 * @date 3/21/22 01:13
 */
@Slf4j
public class EventbusTest {

    public static void main(String[] args) {
        EventBus eventBus = new EventBus();

        EventListener listener = new EventListener();
        eventBus.register(listener);
        // eventBus.unregister(listener);

        eventBus.post("String Event");
        int eventsHandledOne = listener.getEventsHandled();
        log.info("eventsHandledOne:{}", eventsHandledOne);

        CustomEvent customEvent = new CustomEvent("Custom Event");
        eventBus.post(customEvent);
        int eventsHandledTwo = listener.getEventsHandled();
        log.info("eventsHandledTwo:{}", eventsHandledTwo);
    }

}

class EventListener {
    private static int eventsHandled;

    @Subscribe
    public void stringEvent(String event) {
        eventsHandled++;
    }

    @Subscribe
    public void someCustomEvent(CustomEvent customEvent) {
        eventsHandled++;
    }

    public int getEventsHandled() {
        return eventsHandled;
    }
}

@Data
@AllArgsConstructor
class CustomEvent {
    private String action;

    // standard getters/setters and constructors
}