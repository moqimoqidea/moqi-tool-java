package com.moqi.in20220320

import com.google.common.collect.HashBasedTable
import com.google.common.collect.Table
import spock.lang.Specification

/**
 * Table
 *
 * @author moqi
 * @date 3/20/22 19:49
 */
class TableTest extends Specification {

    def "table should work well"() {
        when:
        Table<Integer, Integer, String> table = HashBasedTable.create(0, 0)
        table.put(1, 1, "1")
        table.put(1, 2, "2")
        table.put(2, 1, "3")
        table.put(2, 2, "4")

        then:
        table.size() == 4
        table.get(2,2) == "4"
        table.get(2,3) == null
    }

}
