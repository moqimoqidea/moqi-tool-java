package com.moqi.in20220320;

import com.google.common.util.concurrent.AbstractScheduledService;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * https://zhanyingf15.github.io/2017/11/03/java%E5%A4%9A%E7%BA%BF%E7%A8%8B%E7%AC%94%E8%AE%B0-Guava-%E4%BA%8C/
 *
 * @author moqi
 * @date 3/21/22 00:59
 */
@Slf4j
public class AbstractScheduledServiceTest extends AbstractScheduledService {
    @Override
    protected void startUp() {
        log.info("start");
    }

    @Override
    protected void shutDown() {
        log.info("shutDown");
    }

    @Override
    protected void runOneIteration() {
        try {
            log.info("do work");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Scheduler scheduler() {
        return Scheduler.newFixedRateSchedule(2, 2, TimeUnit.SECONDS);
    }

    public static void main(String[] args) throws Exception {
        AbstractScheduledServiceTest service = new AbstractScheduledServiceTest();
        service.startAsync().awaitRunning();
        Thread.sleep(10000);
        service.stopAsync().awaitTerminated();
    }
}