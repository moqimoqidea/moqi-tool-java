package com.moqi.in20220320

import com.google.common.collect.HashMultiset
import com.google.common.collect.ImmutableMultiset
import com.google.common.collect.Multiset
import com.google.common.collect.Multisets
import spock.lang.Specification

/**
 * MultisetTest
 *
 * @author moqi
 * @date 3/20/22 19:34
 */
class MultisetTest extends Specification {

    def "multiset should work well"() {
        when:
        Multiset<Integer> multiset = HashMultiset.create()
        multiset.add(1)
        multiset.add(2, 200)

        then:
        multiset.count(1) == 1
        multiset.size() == 201
        multiset.count(2) == 200
    }

    def "multiset containsOccurrences should work well"() {
        when:
        Multiset<String> multiset1 = HashMultiset.create()
        multiset1.add("a", 2)

        Multiset<String> multiset2 = HashMultiset.create()
        multiset2.add("a", 5)

        then:
        multiset1.containsAll(multiset2) // returns true: all unique elements are contained,
        // even though multiset1.count("a") == 2 < multiset2.count("a") == 5
        !Multisets.containsOccurrences(multiset1, multiset2) // returns false

        when:
        Multisets.removeOccurrences(multiset2, multiset1) // multiset2 now contains 3 occurrences of "a"

        then:
        multiset2.size() == 3

        when:
        multiset2.removeAll(multiset1) // removes all occurrences of "a" from multiset2, even though multiset1.count("a") == 2

        then:
        multiset2.isEmpty() // returns true
    }

    def "multiset copyHighestCountFirst should work well"() {
        when:
        Multiset<String> multiset = HashMultiset.create()
        multiset.add("a", 3)
        multiset.add("b", 5)
        multiset.add("c", 1)

        ImmutableMultiset<String> highestCountFirst = Multisets.copyHighestCountFirst(multiset)
        // highestCountFirst, like its entrySet and elementSet, iterates over the elements in order {"b", "a", "c"}

        then:
        highestCountFirst.size() == 9
        highestCountFirst.iterator().next() == "b"
    }

}
