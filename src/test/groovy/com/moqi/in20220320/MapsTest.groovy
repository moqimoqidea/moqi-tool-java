package com.moqi.in20220320


import com.google.common.collect.ImmutableMap
import com.google.common.collect.Lists
import com.google.common.collect.MapDifference
import com.google.common.collect.Maps
import spock.lang.Specification

/**
 * Maps
 *
 * https://github.com/google/guava/wiki/CollectionUtilitiesExplained#maps
 *
 * @author moqi
 * @date 3/20/22 20:13
 */
class MapsTest extends Specification {

    def "maps uniqueIndex should work well"() {
        when:
        ArrayList<String> strings = Lists.newArrayList("tom", "jerry", "nancyyyyy")
        ImmutableMap<Integer, String> stringsByIndex = Maps.uniqueIndex(strings, String::length)

        then:
        stringsByIndex.size() == 3
        stringsByIndex.get(3) == "tom"
    }

    def "maps difference should work well"() {
        when:
        Map<String, Integer> left = ImmutableMap.of("a", 1, "b", 2, "c", 3)
        Map<String, Integer> right = ImmutableMap.of("b", 2, "c", 4, "d", 5)
        MapDifference<String, Integer> diff = Maps.difference(left, right)

        diff.entriesInCommon() // {"b" => 2}
        diff.entriesDiffering() // {"c" => (3, 4)}
        diff.entriesOnlyOnLeft() // {"a" => 1}
        diff.entriesOnlyOnRight() // {"d" => 5}

        then:
        diff.entriesInCommon().get("b") == 2
        diff.entriesDiffering().get("c").leftValue() == 3
        diff.entriesDiffering().get("c").rightValue() == 4
        diff.entriesOnlyOnLeft().get("a") == 1
        diff.entriesOnlyOnRight().get("d") == 5
    }

}
