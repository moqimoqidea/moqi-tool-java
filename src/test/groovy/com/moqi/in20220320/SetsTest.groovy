package com.moqi.in20220320

import com.google.common.collect.ImmutableSet
import com.google.common.collect.Sets
import spock.lang.Specification

/**
 * Sets
 *
 * @author moqi
 * @date 3/20/22 19:49
 */
class SetsTest extends Specification {

    /**
     * https://github.com/google/guava/wiki/CollectionUtilitiesExplained#other-set-utilities
     */
    def "sets should work well"() {
        when:
        Set<String> animals = ImmutableSet.of("gerbil", "hamster");
        Set<String> fruits = ImmutableSet.of("apple", "orange", "banana");

        Set<List<String>> product = Sets.cartesianProduct(animals, fruits);
        // {{"gerbil", "apple"}, {"gerbil", "orange"}, {"gerbil", "banana"},
        //  {"hamster", "apple"}, {"hamster", "orange"}, {"hamster", "banana"}}

        Set<Set<String>> animalSets = Sets.powerSet(animals);
        // {{}, {"gerbil"}, {"hamster"}, {"gerbil", "hamster"}}

        then:
        product.size() == 6
        animalSets.size() == 4
    }

}
