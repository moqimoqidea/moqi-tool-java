package com.moqi.in20220320;

import com.google.common.util.concurrent.AbstractExecutionThreadService;
import com.google.common.util.concurrent.Service;
import com.google.common.util.concurrent.ServiceManager;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * https://zhanyingf15.github.io/2017/11/03/java%E5%A4%9A%E7%BA%BF%E7%A8%8B%E7%AC%94%E8%AE%B0-Guava-%E4%BA%8C/
 *
 * @author moqi
 * @date 3/21/22 00:51
 */
@Slf4j
public class ServiceTest {
    private static ExecutorService executorService = Executors.newCachedThreadPool();

    public static void main(String[] args) throws Exception {
        List<Service> services = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            services.add(new TaskService("service" + i));
        }

        ServiceManager serviceManager = new ServiceManager(services);

        serviceManager.addListener(new ServiceManager.Listener() {
            @Override
            public void healthy() {
                log.info("healthy");
            }

            public void stopped() {
                log.info("stopped");
            }
        }, executorService);

        serviceManager.startAsync().awaitHealthy();
        Thread.sleep(2000);
        serviceManager.stopAsync().awaitStopped();

        executorService.shutdown();
    }

    public static class TaskService extends AbstractExecutionThreadService {
        private String name;
        private boolean isRunning = false;

        public TaskService(String name) {
            this.name = name;
        }

        @Override
        protected void run() {
            while (isRunning) {
                try {
                    log.info("{}运行中，结果：{}", name, new Random().nextInt(100));
                    Thread.sleep(1000);
                } catch (Exception e) {
                    //处理异常，这里如果抛出异常，会使服务状态变为failed同时导致任务终止
                }
            }
        }

        @Override
        protected void startUp() {
            this.isRunning = true;
            log.info("{}启动", this.name);
        }

        @Override
        protected void triggerShutdown() {
            this.isRunning = false;//修改状态值
        }

        @Override
        protected void shutDown() {
            log.info("{}关闭", this.name);
        }
    }

}
