package com.moqi.in20220320

import com.google.common.collect.BiMap
import com.google.common.collect.HashBiMap
import spock.lang.Specification

/**
 * BiMap
 *
 * @author moqi
 * @date 3/20/22 19:42
 */
class BiMapTest extends Specification {

    def "bimap should work well"() {
        when:
        BiMap<String, Integer> bimap = HashBiMap.create()
        bimap.put("tom", 28)

        then:
        bimap.get("tom") == 28
        bimap.inverse().get(28) == "tom"
    }

}
