package com.moqi.in20220320

import com.google.common.collect.ArrayListMultimap
import com.google.common.collect.HashMultimap
import com.google.common.collect.ImmutableListMultimap
import com.google.common.collect.ImmutableMap
import com.google.common.collect.ImmutableSet
import com.google.common.collect.Multimap
import com.google.common.collect.Multimaps
import com.google.common.collect.SetMultimap
import com.google.common.collect.Sets
import com.google.common.collect.TreeMultimap
import com.google.common.primitives.Ints
import spock.lang.Specification

/**
 * MultimapTest
 *
 * @author moqi
 * @date 3/20/22 19:34
 */
class MultimapTest extends Specification {

    def "multimap should work well"() {
        when:
        Multimap<String, Integer> multimap = HashMultimap.create()
        multimap.put("tom", 1)
        multimap.put("tom", 2)
        multimap.put("tom", 3)

        then:
        multimap.get("tom").size() == 3
    }

    def "multimap index should work well"() {
        when:
        Set<String> digits = Sets.newHashSet(
                "zero", "one", "two", "three", "four",
                "five", "six", "seven", "eight", "nine")
        ImmutableListMultimap<Integer, String> digitsByLength = Multimaps.index(digits, String::length)
        /*
         * digitsByLength maps:
         *  3 => {"one", "two", "six"}
         *  4 => {"zero", "four", "five", "nine"}
         *  5 => {"three", "seven", "eight"}
         */

        then:
        digitsByLength.get(3).size() == 3
        digitsByLength.get(4).size() == 4
        digitsByLength.get(5).size() == 3
    }

    def "multimap invertFrom should work well"() {
        when:
        ArrayListMultimap<String, Integer> multimap = ArrayListMultimap.create()
        multimap.putAll("b", Ints.asList(2, 4, 6))
        multimap.putAll("a", Ints.asList(4, 2, 1))
        multimap.putAll("c", Ints.asList(2, 5, 3))

        TreeMultimap<Integer, String> inverse = Multimaps.invertFrom(multimap, TreeMultimap.create())
        // note that we choose the implementation, so if we use a TreeMultimap, we get results in order
        /*
         * inverse maps:
         *  1 => {"a"}
         *  2 => {"a", "b", "c"}
         *  3 => {"c"}
         *  4 => {"a", "b"}
         *  5 => {"c"}
         *  6 => {"b"}
         */

        then:
        inverse.get(2).size() == 3
        inverse.get(4).size() == 2
    }

    def "multimap forMap should work well"() {
        when:
        Map<String, Integer> map = ImmutableMap.of("a", 1, "b", 1, "c", 2)
        SetMultimap<String, Integer> multimap = Multimaps.forMap(map)
        // multimap maps ["a" => {1}, "b" => {1}, "c" => {2}]
        Multimap<Integer, String> inverse = Multimaps.invertFrom(multimap, HashMultimap.<Integer, String>create())
        // inverse maps [1 => {"a", "b"}, 2 => {"c"}]

        then:
        multimap.size() == 3
        inverse.size() == 3
        inverse.get(1).size() == 2

        when:
        inverse.put(2, "d")
        inverse.put(3, "e")

        then:
        inverse.size() == 5
        inverse.get(3).size() == 1
    }

}
