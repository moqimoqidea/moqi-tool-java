package com.moqi.in20220320;

import com.google.common.util.concurrent.*;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

/**
 * https://www.baeldung.com/guava-futures-listenablefuture
 * https://juejin.cn/post/6844903822125432846
 *
 * @author moqi
 * @date 3/20/22 23:04
 */
@Slf4j
public class ListenableFutureTest {

    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {

        // test01();
        // test02();
        // addListenerTest();
        // addCallBackTest();
        // allAsListTest();
        // successfulAsListTest();
        // transformTest();
        // listenInPoolThreadTest();

        settleTest();

    }

    /**
     * SettableFuture可以认为是一种异步转同步工具，可以它在指定时间内获取ListenableFuture的计算结果
     */
    private static void settleTest() throws InterruptedException, ExecutionException, TimeoutException {
        ListeningExecutorService service = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());

        SettableFuture<Integer> settableFuture = SettableFuture.create();

        service.submit(() -> {
            int sum = 5 + 6;
            settableFuture.set(sum);
            return sum;
        });

        // get设置超时时间
        Integer integer = settableFuture.get(2, TimeUnit.SECONDS);
        log.info("integer:{}", integer);


        service.shutdown();
    }

    /**
     * JdkFutureAdapters: 该方法可以将JDK Future转成ListenableFuture
     */
    private static void listenInPoolThreadTest() throws InterruptedException, ExecutionException {
        ListeningExecutorService service = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());
        ExecutorService executorService = Executors.newCachedThreadPool();

        Future<String> stringFuture = executorService.submit(() -> "hello,world");
        ListenableFuture<String> future7 = JdkFutureAdapters.listenInPoolThread(stringFuture);
        String s = future7.get();
        log.info("s:{}", s);

        service.shutdown();
        executorService.shutdown();
    }

    /**
     * 如果需要对返回值做处理，可以使用Futures.transform方法，它是同步方法，另外还有一个异步方法Futures.transformAsync
     */
    private static void transformTest() throws InterruptedException, ExecutionException {
        ListeningExecutorService service = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());
        // 原Future
        ListenableFuture<String> future3 = service.submit(() -> "hello, future");
        String s1 = future3.get();
        log.info("s1:{}", s1);

        // 同步转换
        ListenableFuture<Integer> future5 = Futures.transform(future3, String::length, service);
        Integer integer = future5.get();
        log.info("integer:{}", integer);

        // 异步转换
        ListenableFuture<Integer> future6 = Futures.transformAsync(future3, input -> Futures.immediateFuture(input.length()), service);
        Integer integer1 = future6.get();
        log.info("integer1:{}", integer1);

        service.shutdown();
    }

    /**
     * 如果想获取到正常返回的Future，可以使用Futures.successfulAsList方法，该方法会将失败或取消的Future的结果用null来替代
     */
    private static void successfulAsListTest() throws InterruptedException {
        ListeningExecutorService service = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());


        ListenableFuture<Integer> future1 = service.submit(() -> 1 + 2);
        ListenableFuture<Integer> future2 = service.submit(() -> Integer.parseInt("3q"));
        ListenableFuture<List<Object>> futures = Futures.successfulAsList(future1, future2);

        Futures.addCallback(futures, new FutureCallback<>() {
            @Override
            public void onSuccess(@Nullable List<Object> result) {
                log.info("result:{}", result);
            }

            @Override
            public void onFailure(@NotNull Throwable t) {
                log.error("error message:{}", t.getMessage());
            }
        }, service);

        TimeUnit.MILLISECONDS.sleep(1000);
        service.shutdown();
    }

    /**
     * 如果需要同时获取多个Future的值，可以使用Futures.allAsList，需要注意的是如果任何一个Future在执行时出现异常，都会只执行onFailure()方法
     */
    private static void allAsListTest() throws InterruptedException {
        ListeningExecutorService service = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());


        ListenableFuture<Integer> future1 = service.submit(() -> 1 + 2);
        ListenableFuture<Integer> future2 = service.submit(() -> Integer.parseInt("3q"));
        ListenableFuture<List<Object>> futures = Futures.allAsList(future1, future2);

        Futures.addCallback(futures, new FutureCallback<>() {
            @Override
            public void onSuccess(@Nullable List<Object> result) {
                log.info("result:{}", result);
            }

            @Override
            public void onFailure(@NotNull Throwable t) {
                log.error("error message:{}", t.getMessage());
            }
        }, service);

        TimeUnit.MILLISECONDS.sleep(1000);
        service.shutdown();
    }

    /**
     * 如果需要获取返回值，可以使用Futures.addCallBack静态方法，该类是对JDK Future的拓展
     */
    private static void addCallBackTest() throws InterruptedException {
        ListeningExecutorService service = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());

        FutureCallback<String> callback = new FutureCallback<>() {
            @Override
            public void onSuccess(String result) {
                log.info("result: {}", result);
            }

            @Override
            public void onFailure(@NotNull Throwable t) {
                log.error("error message:{}", t.getMessage());
            }
        };


        Futures.addCallback(service.submit(() -> {
            TimeUnit.MILLISECONDS.sleep(500);
            return "something";
        }), callback, service);

        Futures.addCallback(service.submit(() -> {
            TimeUnit.MILLISECONDS.sleep(500);
            throw new RuntimeException("something wrong");
        }), callback, service);


        TimeUnit.MILLISECONDS.sleep(1000);
        service.shutdown();
    }

    /**
     * ListenableFuture接口扩展自Future接口，并添加了一个新方法 addListener，该方法是给异步任务添加监听
     * addListener方法不支持获取返回值
     */
    private static void addListenerTest() throws InterruptedException {
        ListeningExecutorService service = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());

        ListenableFuture<String> listenableFuture = service.submit(() -> {
            TimeUnit.MILLISECONDS.sleep(500);
            return "something";
        });

        listenableFuture.addListener(() -> log.info("listenableFuture.isDone:{}", listenableFuture.isDone()), service);

        TimeUnit.MILLISECONDS.sleep(1000);
        service.shutdown();
    }

    private static void test02() throws InterruptedException, ExecutionException {
        FutureTask<String> fetchConfigTask = fetchConfigTask("fetchConfigTask");
        fetchConfigTask.run();
        String s1 = fetchConfigTask.get();
        log.info("s1:{}", s1);

        ListenableFutureTask<String> fetchConfigListenableTask = fetchConfigListenableTask("fetchConfigListenableTask");
        fetchConfigListenableTask.run();
        String s2 = fetchConfigListenableTask.get();
        log.info("s2:{}", s2);
    }

    private static void test01() throws InterruptedException, ExecutionException {
        ListeningExecutorService service = MoreExecutors.listeningDecorator(Executors.newSingleThreadExecutor());

        ListenableFuture<Integer> asyncTask = service.submit(() -> {
            TimeUnit.MILLISECONDS.sleep(500); // long-running task
            return 5;
        });

        Integer integer = asyncTask.get();
        log.info("integer:{}", integer);
        service.shutdown();
    }

    // old api
    public static FutureTask<String> fetchConfigTask(String configKey) {
        return new FutureTask<>(() -> {
            TimeUnit.MILLISECONDS.sleep(500);
            return String.format("%s.%d", configKey, new Random().nextInt(Integer.MAX_VALUE));
        });
    }

    // new api
    public static ListenableFutureTask<String> fetchConfigListenableTask(String configKey) {
        return ListenableFutureTask.create(() -> {
            TimeUnit.MILLISECONDS.sleep(500);
            return String.format("%s.%d", configKey, new Random().nextInt(Integer.MAX_VALUE));
        });
    }

}
