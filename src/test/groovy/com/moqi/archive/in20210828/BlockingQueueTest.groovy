package com.moqi.archive.in20210828

import groovy.util.logging.Slf4j
import spock.lang.Specification

import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.BlockingQueue
import java.util.concurrent.TimeUnit
import java.util.stream.IntStream

@Slf4j
class BlockingQueueTest extends Specification {

    private static final BlockingQueue<String> QUEUE = new ArrayBlockingQueue<>(MAX_SIZE)
    private static final int MAX_SIZE = 10

    def cleanup() {
        QUEUE.clear()
    }

    def "and more element should thrown illegal state exception"() {
        when:
        IntStream.range(0, MAX_SIZE).forEach(x -> QUEUE.offer("data"))
        QUEUE.add("11 data")

        then:
        thrown(IllegalStateException.class)
    }

    def "offer more element should return false"() {
        when:
        IntStream.range(0, MAX_SIZE).forEach(x -> QUEUE.offer("data"))
        def result = QUEUE.offer("11 data")

        then:
        result == false
    }

    def "put more element should hang until poll"() {
        when:
        IntStream.range(0, MAX_SIZE).forEach(x -> QUEUE.offer("data"))
        new Thread(() -> {
            Thread.sleep(100)
            QUEUE.poll()
            log.info("quque poll success!")
        }).start()
        QUEUE.put("data")
        log.info("quque put success!")

        then:
        QUEUE.size() == MAX_SIZE
    }

    def "offer with more time should success"() {
        when:
        IntStream.range(0, MAX_SIZE).forEach(x -> QUEUE.offer("data"))
        new Thread(() -> {
            Thread.sleep(100)
            QUEUE.poll()
            log.info("quque poll success!")
        }).start()
        def result = QUEUE.offer("data", 120, TimeUnit.MILLISECONDS)
        log.info("quque put success!")

        then:
        result == true
    }

    def "offer with less time should success"() {
        when:
        IntStream.range(0, MAX_SIZE).forEach(x -> QUEUE.offer("data"))
        new Thread(() -> {
            Thread.sleep(100)
            QUEUE.poll()
            log.info("quque poll success!")
        }).start()
        def result = QUEUE.offer("data", 20, TimeUnit.MILLISECONDS)
        log.info("quque put success!")

        then:
        result == false
    }

    def "take with should hang until put one"() {
        when:
        new Thread(() -> {
            Thread.sleep(100)
            QUEUE.put("new data")
            log.info("quque put success!")
        }).start()
        def data = QUEUE.take()
        log.info("quque take success!")

        then:
        data.equals("new data")
    }

    def "poll with more time should thrown exception"() {
        when:
        new Thread(() -> {
            Thread.sleep(100)
            QUEUE.put("new data")
            log.info("quque put success!")
        }).start()
        def data = QUEUE.poll(200, TimeUnit.MILLISECONDS)
        log.info("quque poll success!")

        then:
        data.equals("new data")
    }

    def "poll with less time should return null"() {
        when:
        new Thread(() -> {
            Thread.sleep(100)
            QUEUE.put("new data")
            log.info("quque put success!")
        }).start()
        def data = QUEUE.poll(20, TimeUnit.MILLISECONDS)

        then:
        data == null
    }

    def "remainning capacity should return right size"() {
        when:
        QUEUE.add("data")

        then:
        QUEUE.remainingCapacity() == MAX_SIZE - 1

        when:
        QUEUE.add("data")

        then:
        QUEUE.remainingCapacity() == MAX_SIZE - 2
    }

    def "remove not exist data should return false"() {
        when:
        QUEUE.add("data")
        def notExistResult = QUEUE.remove("not exist")

        then:
        notExistResult == false
    }

    def "remove exist data should return true"() {
        when:
        QUEUE.add("data")
        def existResult = QUEUE.remove("data")

        then:
        existResult == true
    }

    def "contains not exist data should return false"() {
        when:
        QUEUE.add("data")
        def notExistResult = QUEUE.contains("not exist")

        then:
        notExistResult == false
    }

    def "contains exist data should return true"() {
        when:
        QUEUE.add("data")
        def existResult = QUEUE.contains("data")

        then:
        existResult == true
    }

    def "drain to with data should return right size"() {
        when:
        IntStream.range(0, MAX_SIZE).forEach(x -> QUEUE.offer("data"))
        def realSize = QUEUE.drainTo(new ArrayList<String>())

        then:
        realSize == 10
        QUEUE.size() == 0
    }

    def "drain to with data and max limit should return right size"() {
        when:
        IntStream.range(0, MAX_SIZE).forEach(x -> QUEUE.offer("data"))
        def realSize = QUEUE.drainTo(new ArrayList<String>(), 3)

        then:
        realSize == 3
        QUEUE.size() == 7
    }

    def "drain to with nothing should return right size"() {
        when:
        def realSize = QUEUE.drainTo(new ArrayList<String>())

        then:
        realSize == 0
        QUEUE.size() == 0
    }

}
