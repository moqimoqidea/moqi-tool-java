package com.moqi.archive.in20210908

import spock.lang.Specification

/**
 * @author moqi
 * On 2021/9/8 22:30
 */
class ArrayDequeTest extends Specification {

    def "test array deque"() {
        given:
        def fifo = new ArrayDeque(2)


        when:
        fifo.addFirst(1)
        fifo.addFirst(2)
        fifo.addFirst(3)

        then:
        fifo.size() == 3
        fifo.peek() == 3

        when:
        def ele = fifo.removeLast()

        then:
        ele == 1
        fifo.size() == 2
    }

}
