package com.moqi.archive.in20210908

import com.google.common.collect.EvictingQueue
import spock.lang.Specification

/**
 * @author moqi
 * On 2021/9/8 22:30
 */
class EvictingQueueTest extends Specification {

    def "test evicting queue"() {
        given:
        def fifo = EvictingQueue.create(2)


        when:
        fifo.add(1)
        fifo.add(2)
        fifo.add(3)

        then:
        fifo.size() == 2
        fifo.peek() == 2
    }

}
