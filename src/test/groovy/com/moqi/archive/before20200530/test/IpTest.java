package com.moqi.archive.before20200530.test;

import com.google.common.net.InetAddresses;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by moqi
 * On 5/17/21 10:55
 */
@Slf4j
public class IpTest {

    @SuppressWarnings("UnstableApiUsage")
    public static void main(String[] args) {

        boolean inetAddress = InetAddresses.isInetAddress("10.77.120.250");
        log.info("inetAddress = " + inetAddress);
        // boolean inetAddress1 = InetAddresses.isInetAddress(null);
        // log.info("inetAddress1 = " + inetAddress1);
        boolean inetAddress2 = InetAddresses.isInetAddress("");
        log.info("inetAddress2 = " + inetAddress2);

    }

}
