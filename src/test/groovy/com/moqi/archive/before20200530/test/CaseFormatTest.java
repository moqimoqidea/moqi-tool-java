package com.moqi.archive.before20200530.test;

import com.google.common.base.CaseFormat;
import lombok.extern.slf4j.Slf4j;

/**
 * https://www.tutorialspoint.com/guava/guava_caseformat.htm
 *
 * Created by moqi
 * On 5/11/21 11:11
 */
@Slf4j
public class CaseFormatTest {

    /**
     * testData
     * testData
     * TestData
     */
    public static void main(String[] args) {
        log.info(CaseFormat.LOWER_HYPHEN.to(CaseFormat.LOWER_CAMEL, "test-data"));
        log.info(CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, "test_data"));
        log.info(CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, "test_data"));
    }

}

