package com.moqi.archive.before20200530.test;

import com.google.common.base.Joiner;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;

import java.util.Set;

@Slf4j
public class GuavaCollectionTest {

    public static void main(String[] args) {
        Set<String> set1 = Sets.newHashSet("1", "2");
        Set<String> set2 = Sets.newHashSet("3", "2");

        Sets.SetView<String> union = Sets.union(set1, set2);
        log.info("union = " + union);

        Set<String> emptySet = Sets.newHashSet();
        String join = Joiner.on(",").join(emptySet);
        log.info("join = " + join);

        // NPE
        /*HashSet<String> set3 = new HashSet<>(Splitter.on(",")
                .trimResults()
                .splitToList(null));
        log.info("set3 = " + set3);*/

        // NPE
        /*Set<String> set4 = null;
        String join1 = Joiner.on(",").join(set4);
        log.info("join1 = " + join1);*/
    }

}
