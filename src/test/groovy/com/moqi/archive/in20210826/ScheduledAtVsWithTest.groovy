package com.moqi.archive.in20210826

import groovy.util.logging.Slf4j
import spock.lang.Ignore
import spock.lang.Shared
import spock.lang.Specification

import java.time.Instant
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

/**
 * 测试 ScheduledExecutorService 两个方法的区别
 * https://stackoverflow.com/a/24649939
 *
 * 在子线程执行时间小于间隔时间的情况下，at 按时间间隔触发，with 为间隔时间 + 任务执行时间；
 * 在子线程执行时间大于间隔时间的情况下，at 按任务执行时间为间隔触发，with 依然为间隔时间 + 任务执行时间；
 *
 * 此测试类为方便观察使用秒作为单位，跑完需要一分钟十二秒左右，默认忽略。
 */
@Ignore
@Slf4j
class ScheduledAtVsWithTest extends Specification {

    @Shared
    def gapList = new ArrayList<Long>()
    @Shared
    def turnCount = 1

    def cleanup() {
        gapList = new ArrayList<Long>()
        turnCount = 1
    }

    def "schedule at fixed rate on short time should get period gap"() {
        when:
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor()
        service.scheduleAtFixedRate(() -> {
            log.info("start run, turn count is: " + turnCount)
            try {
                Thread.sleep(1000)
                long epochSecond = Instant.now().getEpochSecond()
                log.info("sleep over, turn count is: " + turnCount + ", epoch second: " + epochSecond)
                turnCount += 1
                gapList.add(epochSecond)
            } catch (InterruptedException e) {
                log.warn("thread interrupted", e)
            }
        }, 2L, 2L, TimeUnit.SECONDS)

        and:
        Thread.sleep(12345)
        service.shutdown()
        log.info("main thread wake up, then gap list is: " + gapList)

        then:
        gapList.size() >= 3
        gapList[2] - gapList[1] == 2
    }

    def "schedule with fixed rate on short time should get period gap add execute time"() {
        when:
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor()
        service.scheduleWithFixedDelay(() -> {
            log.info("start run, turn count is: " + turnCount)
            try {
                Thread.sleep(1000)
                long epochSecond = Instant.now().getEpochSecond()
                log.info("sleep over, turn count is: " + turnCount + ", epoch second: " + epochSecond)
                turnCount += 1
                gapList.add(epochSecond)
            } catch (InterruptedException e) {
                log.warn("thread interrupted", e)
            }
        }, 2L, 2L, TimeUnit.SECONDS)

        and:
        Thread.sleep(12345)
        service.shutdown()
        log.info("main thread wake up, then gap list is: " + gapList)

        then:
        gapList.size() >= 3
        gapList[2] - gapList[1] >= 3
    }

    def "schedule at fixed rate on long time should get execute time"() {
        when:
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor()
        service.scheduleAtFixedRate(() -> {
            log.info("start run, turn count is: " + turnCount)
            try {
                Thread.sleep(5000)
                long epochSecond = Instant.now().getEpochSecond()
                log.info("sleep over, turn count is: " + turnCount + ", epoch second: " + epochSecond)
                turnCount += 1
                gapList.add(epochSecond)
            } catch (InterruptedException e) {
                log.warn("thread interrupted", e)
            }
        }, 2L, 2L, TimeUnit.SECONDS)

        and:
        Thread.sleep(23456)
        service.shutdown()
        log.info("main thread wake up, then gap list is: " + gapList)

        then:
        gapList.size() >= 3
        gapList[2] - gapList[1] == 5
    }

    def "schedule with fixed rate on long time should get period gap add execute time"() {
        when:
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor()
        service.scheduleWithFixedDelay(() -> {
            log.info("start run, turn count is: " + turnCount)
            try {
                Thread.sleep(5000)
                long epochSecond = Instant.now().getEpochSecond()
                log.info("sleep over, turn count is: " + turnCount + ", epoch second: " + epochSecond)
                turnCount += 1
                gapList.add(epochSecond)
            } catch (InterruptedException e) {
                log.warn("thread interrupted", e)
            }
        }, 2L, 2L, TimeUnit.SECONDS)

        and:
        Thread.sleep(23456)
        service.shutdown()
        log.info("main thread wake up, then gap list is: " + gapList)

        then:
        gapList.size() >= 3
        gapList[2] - gapList[1] >= 7
    }

}
