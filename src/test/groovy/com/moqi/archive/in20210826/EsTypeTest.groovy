package com.moqi.archive.in20210826

import com.alibaba.fastjson.JSON
import spock.lang.Specification

/**
 * @author moqi
 * @create 8/26/21 09:36
 */
class EsTypeTest extends Specification {

    // {"data":{"key":"key1","value":"value1"},"name":"zhangsan"}
    def "test person with object json format"() {
        when:
        def json = JSON.toJSONString(new PersonWithObject(
                name: "zhangsan",
                data: new Data(
                        key: "key1",
                        value: "value1"
                )
        ))
        println("json: " + json)

        then:
        json.length() >= 0
    }

    // {"data":{"key1":"value1"},"name":"zhangsan"}
    def "test person with map json format"() {
        when:
        def json = JSON.toJSONString(new PersonWithMap(
                name: "zhangsan",
                data: ["key1": "value1"]
        ))
        println("json: " + json)

        then:
        json.length() >= 0
    }

}


class PersonWithObject {
    String name
    Object data
}

class Data {
    String key
    String value
}

class PersonWithMap {
    String name
    Map<String, Object> data
}
