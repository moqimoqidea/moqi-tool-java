package com.moqi.in20211229;

import lombok.extern.slf4j.Slf4j;

import java.sql.*;

/**
 * @author moqi
 * @date 2021/12/29 16:14
 */
@Slf4j
public class TestH2MysqlMode {

    public static void main(String[] args) {
        // String url = "jdbc:h2:tcp://localhost:9092/~/tmp/h2dbs/testdb";
        String url = "jdbc:h2:~/test;MODE=MySQL;DATABASE_TO_LOWER=TRUE";
        String user = "sa";
        String passwd = "";


        try (Connection con = DriverManager.getConnection(url, user, passwd);
             Statement st = con.createStatement();
             ResultSet rs = st.executeQuery("show tables")) {
             log.info("rs: {}", rs);
        } catch (SQLException ex) {
            log.warn("SQLException: {}", ex.getMessage());
        }
    }

}
