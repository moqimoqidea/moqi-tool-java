package com.moqi.in20220108

import spock.lang.Specification

/**
 * @author moqi
 * @date 1/8/22 23:15
 */
class Java17UseSpockMockTest extends Specification {

    Address address = Mock()
    Person person = new Person(address)

    def "test mock work well"() {
        expect:
        person.getAddressName() == null
    }

}

class Person {

    Address address

    Person(Address address) {
        this.address = address
    }

    def getAddressName() {
        address.getName()
    }

    static void main(String[] args) {
        Person p = new Person(new Address("beijing"))

        System.out.println(p.getAddressName())
    }
}

class Address {
    String name = "default address."

    Address(String name) {
        this.name = name
    }
}