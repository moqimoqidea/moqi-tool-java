package com.moqi.in20220406


import com.google.common.math.DoubleMath
import spock.lang.Specification

import java.math.RoundingMode

/**
 * int 除法截断，使用 Guava DoubleMath.roundToInt 恢复
 *
 * @author moqi
 * @date 4/6/22 19:09
 */
class IntDivTest extends Specification {

    def "div should work well"() {
        expect:
        2 == DoubleMath.roundToInt((double) 6400 / 4000, RoundingMode.CEILING)
        1 == DoubleMath.roundToInt((double) 6400 / 4000, RoundingMode.DOWN)
    }

}
