package com.moqi.in20220411

import groovy.util.logging.Slf4j
import net.datafaker.Faker
import org.apache.commons.lang3.StringUtils
import spock.lang.Specification

/**
 * Test datafaker dependency
 *
 * @author moqi
 * @date 4/11/22 10:26
 */
@Slf4j
class FakerTest extends Specification {

    def "faker generate name should work well"() {
        when:
        def faker = new Faker()

        and:
        def fullName = faker.name().fullName()
        def firstName = faker.name().firstName()
        def lastName = faker.name().lastName()

        log.info("fullName: {}", fullName)
        log.info("firstName: {}", firstName)
        log.info("lastName: {}", lastName)

        then:
        StringUtils.isNotBlank(fullName)
        StringUtils.isNotBlank(firstName)
        StringUtils.isNotBlank(lastName)
    }

}
